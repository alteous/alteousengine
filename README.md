Alteous Engine
==============

A lightweight graphics and game engine built on OpenGL.
-------------------------------------------------------

### Instructions for building an executable from source

Windows:

* Obtain a copy of the 32bit SDL2 development library (SDL2.a) and the 32bit runtime binary (SDL2.dll)
* Place the SDL2 development library (SDL2.a) where your compiler will find it during the linking stage.
* Build using cmake and a GNU style compiler frontend (for example TGM-GCC, Cygwin, or MinGW).
* Place the SDL2 runtime binary (SDL2.dll) alongside the executable.

* When compiling shaders, make sure the line endings are Unix style (LF) or you may get compilation errors.

Linux:

* sudo apt-get cmake libgl1-mesa-dev libsdl2-2.0-dev
* cmake .

### Instructions for porting to WebGL

* Build the *portable* Emscripten SDK (https://kripken.github.io/emscripten-site/docs/getting_started/downloads.html)
* make
