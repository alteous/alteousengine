/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file entity_loader_test.cpp
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 02/01/16
 * @brief Unit test for the engine entity loader.
 */

#include <gtest/gtest.h>
#include <alteous/entity_loader.h>

const ats_char *path = "res/scenes/park.json";

static void error_fn(ats_error error_code, const ats_char *description)
{
        fprintf(stderr, "Error (%d): %s.\n", error_code, description);
        FAIL();
}

TEST(EntityLoader, LoadPark)
{
        ats_size objects_loaded = ats_ent_loader_load(path, error_fn);
        EXPECT_EQ(3, objects_loaded);
}

