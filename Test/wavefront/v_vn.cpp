/**
 * @file wavefront/v_vn.cpp
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 02/01/16
 * @brief Tests loading a .obj file with only vertex positions and vertex normals.
 */

#include <gtest/gtest.h>

#include <alteous/wavefront.h>

static const ats_float positions[] = {
        0.0f,  0.0f,  0.0f,
        0.0f,  0.0f,  1.0f,
        0.0f,  1.0f,  0.0f,
        0.0f,  1.0f,  1.0f,
        1.0f,  0.0f,  0.0f,
        1.0f,  0.0f,  1.0f,
        1.0f,  1.0f,  0.0f,
        1.0f,  1.0f,  1.0f
};

static const ats_float normals[] = {
         0.0f,  0.0f,  1.0f,
         0.0f,  0.0f, -1.0f,
         0.0f,  1.0f,  0.0f,
         0.0f, -1.0f,  0.0f,
         1.0f,  0.0f,  0.0f,
        -1.0f,  0.0f,  0.0f
};

static const struct ats_wft_face faces[] = {
        {
                { 1, 7, 5 },
                { 0, 0, 0 },
                { 2, 2, 2 }
        },
        {
                { 1, 3, 7 },
                { 0, 0, 0 },
                { 2, 2, 2 }
        },
        {
                { 1, 4, 3 },
                { 0, 0, 0 },
                { 6, 6, 6 }
        },
        {
                { 1, 2, 4 },
                { 0, 0, 0 },
                { 6, 6, 6 }
        },
        {
                { 3, 8, 7 },
                { 0, 0, 0 },
                { 3, 3, 3 }
        },
        {
                { 3, 4, 8 },
                { 0, 0, 0 },
                { 3, 3, 3 }
        },
        {
                { 5, 7, 8 },
                { 0, 0, 0 },
                { 5, 5, 5 }
        },
        {
                { 5, 8, 6 },
                { 0, 0, 0 },
                { 5, 5, 5 }
        },
        {
                { 1, 5, 6 },
                { 0, 0, 0 },
                { 4, 4, 4 }
        },
        {
                { 1, 6, 2 },
                { 0, 0, 0 },
                { 4, 4, 4 }
        },
        {
                { 2, 6, 8 },
                { 0, 0, 0 },
                { 1, 1, 1 }
        },
        {
                { 2, 8, 4 },
                { 0, 0, 0 },
                { 1, 1, 1 }
        }
};

TEST(Wavefront, v_vn)
{
        ats_error error;
        struct ats_wft *wft = NULL;

        wft = ats_wft_init("res/obj/v-vn.obj", &error);
        ASSERT_EQ(AtsNoError, error);

        for (ats_size i = 0; i < sizeof(positions) / sizeof(*positions); i++) {
                EXPECT_FLOAT_EQ(positions[i], wft->position_vectors[i]);
        }

        for (ats_size i = 0; i < sizeof(normals) / sizeof(*normals); i++) {
                EXPECT_FLOAT_EQ(normals[i], wft->normal_vectors[i]);
        }

        for (ats_size i = 0; i < sizeof(faces) / sizeof(*faces); i++) {
                for (ats_size j = 0; j < 3; j++) {
                        EXPECT_EQ(faces[i].position_index[j], wft->faces[i].position_index[j]);
                }
                for (ats_size j = 0; j < 3; j++) {
                        EXPECT_EQ(faces[i].normal_index[j], wft->faces[i].normal_index[j]);
                }
                for (ats_size j = 0; j < 2; j++) {
                        EXPECT_EQ(faces[i].texture_index[j], wft->faces[i].texture_index[j]);
                }
        }

        ats_wft_free(wft);
}
