/**
 * @file transform_stack_test.cpp
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 21/12/15
 * @brief Defines test cases for an AtsTransformStack.
 * @see AtsTransformStack
 */

#include <gtest/gtest.h>
#include <alteous/transform.h>

static const ats_mat4 identity = {
        {
                1.0f, 0.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 1.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f
        }
};

static constexpr ats_float sqrt2 = std::sqrt(2.0f);

class Transform : public testing::Test
{
public:
        struct ats_transform *transform = nullptr;
        struct ats_mat4 result, expected;

        virtual void SetUp() override
        {
                transform = ats_transform_init();
        }

        virtual void TearDown() override
        {
                ats_transform_free(transform);
                transform = nullptr;
        }
};

TEST_F(Transform, Init)
{
        ASSERT_NE(nullptr, transform);
}

TEST_F(Transform, Identity)
{
        /* The identity transformation should always be the first transformation on the stack. */
        ats_transform_compose(transform, &result);
        ASSERT_TRUE(ats_mat4_eq(&identity, &result));
}

TEST_F(Transform, SingleTranslation)
{
        struct ats_vec3f vector = { 0.0f, 5.0f, -2.0f };
        ats_transform_translate(transform, &vector);
        ats_transform_compose(transform, &result);

        expected = {
                {
                        1.0f, 0.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f, 0.0f,
                        0.0f, 0.0f, 1.0f, 0.0f,
                        0.0f, 5.0f, -2.0f, 1.0f
                }
        };
        EXPECT_TRUE(ats_mat4_eq(&result, &expected));
}

TEST_F(Transform, SingleScale)
{
        struct ats_vec3f vector = { 1.0f, 0.0f, -3.0f };
        ats_transform_scale(transform, &vector);
        ats_transform_compose(transform, &result);

        expected = {
                {
                        1.0f, 0.0f, 0.0f, 0.0f,
                        0.0f, 0.0f, 0.0f, 0.0f,
                        0.0f, 0.0f, -3.0f, 0.0f,
                        0.0f, 0.0f, 0.0f, 1.0f
                }
        };
        EXPECT_TRUE(ats_mat4_eq(&result, &expected));
}

TEST_F(Transform, SingleRotation)
{
        struct ats_angle angle = { 45.0f, AtsDegrees };
        ats_float radians = ats_angle_radians(&angle);
        ats_float cosA = cosf(radians);
        ats_float sinA = sinf(radians);

        ats_transform_rotX(transform, &angle);
        ats_transform_compose(transform, &result);
        expected = {
                {
                        1.0f, 0.0f, 0.0f, 0.0f,
                        0.0f, cosA, sinA, 0.0f,
                        0.0f, -sinA, cosA, 0.0f,
                        0.0f, 0.0f, 0.0f, 1.0f
                }
        };
        EXPECT_TRUE(ats_mat4_eq(&result, &expected));

        ats_transform_rotY(transform, &angle);
        ats_transform_compose(transform, &result);
        expected = {
                {
                        cosA, 0.0f, -sinA, 0.0f,
                        0.0f, 1.0f, 0.0f, 0.0f,
                        sinA, 0.0f, cosA, 0.0f,
                        0.0f, 0.0f, 0.0f, 1.0f
                }
        };
        EXPECT_TRUE(ats_mat4_eq(&result, &expected));

        ats_transform_rotZ(transform, &angle);
        ats_transform_compose(transform, &result);
        expected = {
                {
                        cosA, sinA, 0.0f, 0.0f,
                        -sinA, cosA, 0.0f, 0.0f,
                        0.0f, 0.0f, 1.0f, 0.0f,
                        0.0f, 0.0f, 0.0f, 1.0f
                }
        };
        EXPECT_TRUE(ats_mat4_eq(&result, &expected));
}
