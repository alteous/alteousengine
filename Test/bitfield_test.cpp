/**
 * @file bitfield_test.cpp
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 29/12/15
 */

#include <gtest/gtest.h>

#include <alteous/bitfield.h>
#include <alteous/types.h>

typedef uint8_t flag_t;

static const flag_t bit0 = 0;
static const flag_t bit1 = 1;
static const flag_t bit2 = 2;
static const flag_t bit3 = 3;
static const flag_t bit4 = 4;
static const flag_t bit5 = 5;
static const flag_t bit6 = 6;
static const flag_t bit7 = 7;

static const flag_t flag0 = (1 << bit0);
static const flag_t flag1 = (1 << bit1);
static const flag_t flag2 = (1 << bit2);
static const flag_t flag3 = (1 << bit3);
static const flag_t flag4 = (1 << bit4);
static const flag_t flag5 = (1 << bit5);
static const flag_t flag6 = (1 << bit6);
static const flag_t flag7 = (1 << bit7);

TEST(Bitfield, SetFlag)
{
        flag_t flags = 0;

        ATS_BITFIELD_SET_FLAG(flags, flag0);
        ASSERT_EQ(flag0, flags);

        ATS_BITFIELD_SET_FLAG(flags, flag0);
        ASSERT_EQ(flag0, flags);

        ATS_BITFIELD_SET_FLAG(flags, flag7);
        ASSERT_EQ(flag7 | flag0, flags);
}

TEST(Bitfield, SetBit)
{
        flag_t flags = 0;

        ATS_BITFIELD_SET_BIT(flags, bit0);
        ASSERT_EQ(flag0, flags);

        ATS_BITFIELD_SET_BIT(flags, bit0);
        ASSERT_EQ(flag0, flags);

        ATS_BITFIELD_SET_BIT(flags, bit7);
        ASSERT_EQ(flag7 | flag0, flags);
}

TEST(Bitfield, ClearFlag)
{
        flag_t flags = flag1 | flag2;

        ATS_BITFIELD_CLEAR_FLAG(flags, flag0);
        ASSERT_EQ(flag1 | flag2, flags);

        ATS_BITFIELD_CLEAR_FLAG(flags, flag1);
        ASSERT_EQ(flag2, flags);

        ATS_BITFIELD_CLEAR_FLAG(flags, flag2);
        ASSERT_EQ(0, flags);
}

TEST(Bitfield, ClearBit)
{
        flag_t flags = flag1 | flag2;

        ATS_BITFIELD_CLEAR_BIT(flags, bit0);
        ASSERT_EQ(flag1 | flag2, flags);

        ATS_BITFIELD_CLEAR_BIT(flags, bit1);
        ASSERT_EQ(flag2, flags);

        ATS_BITFIELD_CLEAR_BIT(flags, bit2);
        ASSERT_EQ(0, flags);
}

TEST(Bitfield, HasFlag)
{
        flag_t flags = flag3 | flag4 | flag6;
        ats_bool has_flag;

        has_flag = ATS_BITFIELD_HAS_FLAG(flags, flag5);
        ASSERT_FALSE(has_flag);

        has_flag = ATS_BITFIELD_HAS_FLAG(flags, flag4);
        ASSERT_TRUE(has_flag);

        has_flag = ATS_BITFIELD_HAS_FLAG(flags, flag6);
        ASSERT_TRUE(has_flag);
}

TEST(Bitfield, HasBit)
{
        flag_t flags = flag3 | flag4 | flag6;
        ats_bool has_flag;

        has_flag = ATS_BITFIELD_HAS_BIT(flags, bit5);
        ASSERT_FALSE(has_flag);

        has_flag = ATS_BITFIELD_HAS_BIT(flags, bit4);
        ASSERT_TRUE(has_flag);

        has_flag = ATS_BITFIELD_HAS_BIT(flags, bit6);
        ASSERT_TRUE(has_flag);
}