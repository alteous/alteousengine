/**
 * @file entity_test.cpp
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 27/12/15
 */

#include <gtest/gtest.h>

#include <alteous/entity.h>
#include <alteous/component.h>

TEST(Entity, Init)
{
        ats_comp_bitfield flags, returned_flags;

        flags = AtsCompPosBit | AtsCompMvtBit;
        ats_ent_uid id0 = ats_ent_init(flags);
        ASSERT_NE(-1, id0);

        flags = AtsCompPosBit;
        ats_ent_uid id1 = ats_ent_init(flags);
        ASSERT_NE(-1, id1);
        ASSERT_NE(id0, id1);

        flags = AtsCompMvtBit;
        ats_ent_uid id2 = ats_ent_init(flags);
        ASSERT_NE(-1, id2);
        ASSERT_NE(id0, id1);
        ASSERT_NE(id1, id2);
}

TEST(Entity, Position)
{
        ats_comp_bitfield flags = AtsCompPosBit;
        ats_ent_uid id = ats_ent_init(flags);

        ASSERT_NE(-1, id);

        void *position = ats_ent_get_comp(id, AtsCompPos);
        ASSERT_NE(nullptr, position);

        void *movement = ats_ent_get_comp(id, AtsCompMvt);
        ASSERT_EQ(nullptr, movement);
}

TEST(Entity, Movement)
{
        ats_comp_bitfield flags = AtsCompMvtBit;
        ats_ent_uid id = ats_ent_init(flags);

        ASSERT_NE(-1, id);

        void *position = ats_ent_get_comp(id, AtsCompPos);
        ASSERT_EQ(nullptr, position);

        void *movement = ats_ent_get_comp(id, AtsCompMvt);
        ASSERT_NE(nullptr, movement);
}

TEST(Entity, PositionMovement)
{
        ats_comp_bitfield flags = AtsCompPosBit | AtsCompMvtBit;
        ats_ent_uid id = ats_ent_init(flags);

        ASSERT_NE(-1, id);

        void *position = ats_ent_get_comp(id, AtsCompPos);
        ASSERT_NE(nullptr, position);

        void *movement = ats_ent_get_comp(id, AtsCompMvt);
        ASSERT_NE(nullptr, movement);
}
