/**
 * @file angle_test.cpp
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 21/12/15
 * @brief Defines test cases for an AtsAngle.
 * @see AtsAngle
 */

#include <gtest/gtest.h>
#include <alteous/angle.h>
#include <alteous/constants.h>

static struct ats_angle angle;

TEST(Angle, GetDegrees)
{
        angle = { 90.0f, AtsDegrees };
        EXPECT_FLOAT_EQ(90.0f, ats_angle_degrees(&angle));

        angle = { ATS_PI / 2.0f, AtsRadians };
        EXPECT_FLOAT_EQ(90.0f, ats_angle_degrees(&angle));
}

TEST(Angle, GetRadians)
{
        angle = { ATS_PI / 2.0f, AtsRadians };
        EXPECT_FLOAT_EQ(ATS_PI / 2.0f, ats_angle_radians(&angle));

        angle = { 90.0f, AtsDegrees };
        EXPECT_FLOAT_EQ(ATS_PI / 2.0f, ats_angle_radians(&angle));
}

TEST(Angle, Add)
{
        struct ats_angle expected_degrees = { 180.0f, AtsDegrees };
        struct ats_angle expected_radians = { ATS_PI, AtsRadians };
        struct ats_angle arg;

        angle = { ATS_PI / 2.0f, AtsRadians };
        arg = { 90.0f, AtsDegrees };
        ats_angle_add(&angle, &arg);
        EXPECT_FLOAT_EQ(ats_angle_degrees(&expected_degrees), ats_angle_degrees(&angle));
        EXPECT_FLOAT_EQ(ats_angle_radians(&expected_radians), ats_angle_radians(&angle));

        angle = { 90.0f, AtsDegrees };
        arg = { ATS_PI / 2.0f, AtsRadians };
        ats_angle_add(&angle, &arg);
        EXPECT_FLOAT_EQ(ats_angle_degrees(&expected_degrees), ats_angle_degrees(&angle));
        EXPECT_FLOAT_EQ(ats_angle_radians(&expected_radians), ats_angle_radians(&angle));

        angle = { ATS_PI / 2.0f, AtsRadians };
        arg = { ATS_PI / 2.0f, AtsRadians };
        ats_angle_add(&angle, &arg);
        EXPECT_FLOAT_EQ(ats_angle_degrees(&expected_degrees), ats_angle_degrees(&angle));
        EXPECT_FLOAT_EQ(ats_angle_radians(&expected_radians), ats_angle_radians(&angle));

        angle = { 90.0f, AtsDegrees };
        arg = { 90.0f, AtsDegrees };
        ats_angle_add(&angle, &arg);
        EXPECT_FLOAT_EQ(ats_angle_degrees(&expected_degrees), ats_angle_degrees(&angle));
        EXPECT_FLOAT_EQ(ats_angle_radians(&expected_radians), ats_angle_radians(&angle));
}

TEST(Angle, Copy)
{
        struct ats_angle copy;

        angle = { 90.0f, AtsDegrees };
        ats_angle_copy(&angle, &copy);
        EXPECT_FLOAT_EQ(ats_angle_degrees(&angle), ats_angle_degrees(&copy));
        EXPECT_FLOAT_EQ(ats_angle_radians(&angle), ats_angle_radians(&copy));
}
