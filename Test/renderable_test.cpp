/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file renderable_test.cpp
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 03/01/16
 * @brief Unit test for the renderable component generator.
 */

#include <gtest/gtest.h>

#include <alteous/renderable.h>
#include <alteous/mesh.h>
#include <alteous/alteous.h>

static ats_float positions[] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f
};

static ats_float normals[] = {
        0.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 0.0f
};

static ats_float textures[] = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f
};

static ats_ushort indices[] = {
        0, 1, 2
};

static ats_int vertex_count = 3;
static ats_int index_count = 3;

static struct ats_mesh mesh = {
        positions,
        normals,
        textures,
        vertex_count,
        indices,
        index_count
};

TEST(Renderable, Init)
{
        /* We need an active OpenGL context to generate buffers. */
        ats_init(0);

        ats_renderable_uid id = -1;
        ats_error error = ats_renderable_init(&id, &mesh);

        ASSERT_EQ(AtsNoError, error);
        ASSERT_TRUE(id > -1);

        ats_renderable_free(id);

        ats_terminate();
}
