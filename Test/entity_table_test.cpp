/**
 * @file entity_table_test.cpp
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 28/12/15
 */

#include <gtest/gtest.h>

#include <alteous/entity_table.h>
#include <alteous/bitfield.h>

static const ats_size init_row_limit = 32;

class EntityTable : public testing::Test
{
public:
        struct ats_ent_tbl *table = nullptr;

        virtual void SetUp() override
        {
                table = ats_ent_tbl_init();
        }

        virtual void TearDown() override
        {
                ats_ent_tbl_free(table);
                table = nullptr;
        }
};

TEST_F(EntityTable, Init)
{
        ASSERT_NE(nullptr, table);
}

TEST_F(EntityTable, InsertEmpty)
{
        ats_size row, col;
        EXPECT_DEATH(ats_ent_tbl_insert(table, 0, &row, &col), "");
}

TEST_F(EntityTable, InsertWithoutRc)
{
        ats_error error = ats_ent_tbl_insert(table, AtsCompPosBit, 0, 0);
        ASSERT_EQ(AtsNoError, error);
}

TEST_F(EntityTable, InsertIntoSingleRow)
{
        ats_size row, col;
        ats_error error;

        error = ats_ent_tbl_insert(table, AtsCompPosBit, &row, &col);
        ASSERT_EQ(AtsNoError, error);
        EXPECT_EQ(0, row);
        EXPECT_EQ(0, col);

        error = ats_ent_tbl_insert(table, AtsCompPosBit, &row, &col);
        ASSERT_EQ(AtsNoError, error);
        EXPECT_EQ(0, row);
        EXPECT_EQ(1, col);
}

TEST_F(EntityTable, InsertIntoMultipleRows)
{
        ats_size row, col;
        ats_error error;

        error = ats_ent_tbl_insert(table, AtsCompPosBit, &row, &col);
        ASSERT_EQ(AtsNoError, error);
        EXPECT_EQ(0, row);
        EXPECT_EQ(0, col);

        error = ats_ent_tbl_insert(table, AtsCompPosBit, &row, &col);
        ASSERT_EQ(AtsNoError, error);
        EXPECT_EQ(0, row);
        EXPECT_EQ(1, col);

        error = ats_ent_tbl_insert(table, AtsCompMvtBit, &row, &col);
        ASSERT_EQ(AtsNoError, error);
        EXPECT_EQ(1, row);
        EXPECT_EQ(0, col);

        error = ats_ent_tbl_insert(table, AtsCompPosBit | AtsCompMvtBit, &row, &col);
        ASSERT_EQ(AtsNoError, error);
        EXPECT_EQ(2, row);
        EXPECT_EQ(0, col);
}

TEST_F(EntityTable, OutOfBounds)
{
        EXPECT_DEATH(ats_ent_tbl_get_comp(table, AtsCompPos, 0, 0), "");
}

TEST_F(EntityTable, GetComp)
{
        ats_size row, col;
        void *position;
        void *movement;

        ats_ent_tbl_insert(table, AtsCompPosBit, &row, &col);
        position = ats_ent_tbl_get_comp(table, AtsCompPos, row, col);
        movement = ats_ent_tbl_get_comp(table, AtsCompMvt, row, col);
        EXPECT_NE(nullptr, position);
        EXPECT_EQ(nullptr, movement);

        ats_ent_tbl_insert(table, AtsCompMvtBit, &row, &col);
        position = ats_ent_tbl_get_comp(table, AtsCompPos, row, col);
        movement = ats_ent_tbl_get_comp(table, AtsCompMvt, row, col);
        EXPECT_EQ(nullptr, position);
        EXPECT_NE(nullptr, movement);

        ats_ent_tbl_insert(table, AtsCompPosBit | AtsCompMvtBit, &row, &col);
        position = ats_ent_tbl_get_comp(table, AtsCompPos, row, col);
        movement = ats_ent_tbl_get_comp(table, AtsCompMvt, row, col);
        EXPECT_NE(nullptr, position);
        EXPECT_NE(nullptr, movement);
}

TEST_F(EntityTable, IterateOverEmptyTable)
{
        struct ats_ent_tbl_itr *itr;

        itr = ats_ent_tbl_itr_init(table, AtsCompPosBit);
        ASSERT_EQ(nullptr, itr);
}

TEST_F(EntityTable, IterateOverSingleColumn)
{
        struct ats_ent_tbl_itr *itr;
        ats_comp_bitfield comp_flags = AtsCompPosBit;
        void *pos_ptr;
        void *mvt_ptr;

        ats_ent_tbl_insert(table, comp_flags, NULL, NULL);
        ats_ent_tbl_insert(table, comp_flags, NULL, NULL);

        itr = ats_ent_tbl_itr_init(table, AtsCompPosBit);
        ASSERT_NE(nullptr, itr);
        while (ats_ent_tbl_itr_next(itr)) {
                pos_ptr = ats_ent_tbl_itr_get_comp(itr, AtsCompPos);
                mvt_ptr = ats_ent_tbl_itr_get_comp(itr, AtsCompMvt);

                /* We should only recieve the entities that only have position data. */
                ASSERT_NE(nullptr, pos_ptr);
                ASSERT_EQ(nullptr, mvt_ptr);
        }
        ats_ent_tbl_itr_free(itr);
}

TEST_F(EntityTable, IterateOverMultipleColumns)
{
        struct ats_ent_tbl_itr *itr;
        void *pos_ptr;
        void *mvt_ptr;

        ats_ent_tbl_insert(table, AtsCompPosBit, NULL, NULL);
        ats_ent_tbl_insert(table, AtsCompPosBit, NULL, NULL);

        ats_ent_tbl_insert(table, AtsCompPosBit | AtsCompMvtBit, NULL, NULL);
        ats_ent_tbl_insert(table, AtsCompPosBit | AtsCompMvtBit, NULL, NULL);

        itr = ats_ent_tbl_itr_init(table, AtsCompPosBit | AtsCompMvtBit);
        ASSERT_NE(nullptr, itr);
        while (ats_ent_tbl_itr_next(itr)) {
                pos_ptr = ats_ent_tbl_itr_get_comp(itr, AtsCompPos);
                mvt_ptr = ats_ent_tbl_itr_get_comp(itr, AtsCompMvt);

                /* We should only receive the entities that have both position and movement data. */
                ASSERT_NE(nullptr, pos_ptr);
                ASSERT_NE(nullptr, mvt_ptr);
        }
        ats_ent_tbl_itr_free(itr);
}
