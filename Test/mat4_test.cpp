/**
 * @file transform_stack_test.cpp
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 21/12/15
 * @brief Defines test cases for an AtsMat4.
 * @see AtsMat4
 */

#include <gtest/gtest.h>
#include <alteous/mat4.h>

static const ats_mat4 identity = {
        {
                1.0f, 0.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 1.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f
        }
};

static const ats_mat4 zero = {
        {
                0.0f, 0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 0.0f
        }
};

TEST(Mat4, Equals)
{
        struct ats_mat4 matrix = {
                {
                        1.0f, 2.0f, 3.0f, 4.0f,
                        5.0f, 6.0f, 7.0f, 8.0f,
                        9.0f, 2.0f, 4.0f, 2.0f,
                        1.0f, 5.0f, 2.0f, 1.0f
                }
        };

        struct ats_mat4 arg = {
                {
                        1.0f, 2.0f, 3.0f, 4.0f,
                        5.0f, 6.0f, 7.0f, 8.0f,
                        9.0f, 2.0f, 4.0f, 2.0f,
                        1.0f, 5.0f, 2.0f, 1.0f
                }
        };

        ASSERT_EQ(0, memcmp(matrix.m, arg.m, 16 * sizeof(ats_float)));
        ASSERT_TRUE(ats_mat4_eq(&matrix, &arg));
}

TEST(Mat4, Identity)
{
        struct ats_mat4 matrix;
        ats_mat4_identity(&matrix);
        ASSERT_TRUE(ats_mat4_eq(&matrix, &identity));
}

/* N.B. this tests only the multiplication semantics, not the accuracy of the computed results. */
TEST(Mat4, MatrixProduct)
{
        struct ats_mat4 product;

        /* A completely filled matrix. */
        struct ats_mat4 left = {
                {
                        1.0f, 2.0f, 3.0f, 4.0f,
                        5.0f, 6.0f, 7.0f, 8.0f,
                        9.0f, 2.0f, 4.0f, 2.0f,
                        1.0f, 5.0f, 2.0f, 1.0f
                }
        };

        /* A sparse matrix. */
        struct ats_mat4 right = {
                {
                        1.0f, 0.0f, 7.0f, 0.0f,
                        2.0f, 5.0f, 0.0f, 0.0f,
                        9.0f, 2.0f, 4.0f, 2.0f,
                        1.0f, 5.0f, 2.0f, 1.0f
                }
        };

        /* Ensure the identity matrix does not alter the matrix. */
        ats_mat4_mat_prod(&product, &right, &identity);
        ASSERT_TRUE(ats_mat4_eq(&product, &right));

        ASSERT_TRUE(ats_mat4_eq(&product, &right));

        struct ats_mat4 expected_lr = {
                {
                        64.0f, 16.0f, 31.0f, 18.0f,
                        27.0f, 34.0f, 41.0f, 48.0f,
                        57.0f, 48.0f, 61.0f, 62.0f,
                        45.0f, 41.0f, 48.0f, 49.0f
                }
        };
        ats_mat4_mat_prod(&product, &left, &right);
        ASSERT_TRUE(ats_mat4_eq(&product, &expected_lr));

        struct ats_mat4 expected_rl = {
                {
                        36.0f, 36.0f, 27.0f, 10.0f,
                        88.0f, 84.0f, 79.0f, 22.0f,
                        51.0f, 28.0f, 83.0f, 10.0f,
                        30.0f, 34.0f, 17.0f, 5.0f
                }
        };
        ats_mat4_mat_prod(&product, &right, &left);
        ASSERT_TRUE(ats_mat4_eq(&product, &expected_rl));

        struct ats_mat4 expected_ll = {
                {
                        42.0f, 40.0f, 37.0f, 30.0f,
                        106.0f, 100.0f, 101.0f, 90.0f,
                        57.0f, 48.0f, 61.0f, 62.0f,
                        45.0f, 41.0f, 48.0f, 49.0f
                }
        };
        ats_mat4_mat_prod(&product, &left, &left);
        ASSERT_TRUE(ats_mat4_eq(&product, &expected_ll));

        struct ats_mat4 expected_rr = {
                {
                        64.0f, 14.0f, 35.0f, 14.0f,
                        12.0f, 25.0f, 14.0f, 0.0f,
                        51.0f, 28.0f, 83.0f, 10.0f,
                        30.0f, 34.0f, 17.0f, 5.0f
                }
        };
        ats_mat4_mat_prod(&product, &right, &right);
        ASSERT_TRUE(ats_mat4_eq(&product, &expected_rr));
}

TEST(Mat4, ScalarProduct)
{
        struct ats_mat4 matrix = {
                {
                        1.0f, 1.5f, 2.0f, 2.5f,
                        3.0f, 3.5f, 4.0f, 4.5f,
                        5.0f, 5.5f, 6.0f, 6.5f,
                        7.0f, 7.5f, 8.0f, 8.5f
                }
        };

        struct ats_mat4 expected = {
                {
                        2.0f, 3.0f, 4.0f, 5.0f,
                        6.0f, 7.0f, 8.0f, 9.0f,
                        10.0f, 11.0f, 12.0f, 13.0f,
                        14.0f, 15.0f, 16.0f, 17.0f,
                }
        };
        ats_mat4_scl_prod(&matrix, 2.0f);
        ASSERT_TRUE(ats_mat4_eq(&matrix, &expected));

        ats_mat4_scl_prod(&matrix, 0.0f);
        ASSERT_TRUE(ats_mat4_eq(&matrix, &zero));
}
