/**
 * @file stack_test.cpp
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 21/12/15
 * @brief Defines test cases for an AtsStack.
 * @see AtsStack
 */

#include <gtest/gtest.h>
#include <alteous/stack.h>

class Stack : public testing::Test
{
public:
        struct ats_stk *stack = nullptr;
        static const ats_size init_stack_capacity = 2;

        virtual void SetUp() override
        {
                stack = ats_stk_init(init_stack_capacity);
        }

        virtual void TearDown() override
        {
                ats_stk_free(stack);
                stack = nullptr;
        }
};

TEST_F(Stack, Init)
{
        ASSERT_NE(nullptr, stack);
        ASSERT_NE(nullptr, stack->elements);
        ASSERT_EQ(0, stack->height);
        ASSERT_EQ(2, stack->capacity);
        EXPECT_EQ(nullptr, stack->top);
}

TEST_F(Stack, Push)
{
        ats_int *m0 = new ats_int(0);
        ats_int *m1 = new ats_int(1);
        ats_int *m2 = new ats_int(2);
        ats_error error;

        /* Check that the stack updates correctly when not resizing. */
        error = ats_stk_push(stack, m0);
        ASSERT_EQ(AtsNoError, error);
        ASSERT_EQ(1, stack->height);
        EXPECT_EQ(2, stack->capacity);
        EXPECT_EQ(m0, stack->top);

        error = ats_stk_push(stack, m1);
        ASSERT_EQ(AtsNoError, error);
        ASSERT_EQ(2, stack->height);
        EXPECT_EQ(2, stack->capacity);
        EXPECT_EQ(m1, stack->top);

        /* Check that the stack resizes itself properly (i.e. doubling its size). */
        error = ats_stk_push(stack, m2);
        if (AtsAllocFailed == error) {
                /* This situation is very unlikely, but it's best to check anyway. */
                ASSERT_NE(nullptr, stack->elements);
                ASSERT_EQ(m1, stack->top);
                ASSERT_EQ(2, stack->capacity);
                ASSERT_EQ(2, stack->height);
        } else {
                ASSERT_EQ(3, stack->height);
                EXPECT_EQ(4, stack->capacity);
                EXPECT_EQ(m2, stack->top);
        }

        delete m0;
        delete m1;
        delete m2;
}

TEST_F(Stack, Peek)
{
        ats_int *m0 = new ats_int(0);
        ats_int *m1 = new ats_int(1);
        ats_int *m2 = new ats_int(2);

        /* Check that the stack updates correctly when not resizing. */
        ats_stk_push(stack, m0);
        EXPECT_EQ(m0, ats_stk_peek(stack));

        ats_stk_push(stack, m1);
        EXPECT_EQ(m1, ats_stk_peek(stack));

        ats_stk_push(stack, m2);
        EXPECT_EQ(m2, ats_stk_peek(stack));

        ats_stk_pop(stack);
        EXPECT_EQ(m1, ats_stk_peek(stack));

        ats_stk_pop(stack);
        EXPECT_EQ(m0, ats_stk_peek(stack));

        ats_stk_pop(stack);
        EXPECT_EQ(nullptr, ats_stk_peek(stack));

        delete m0;
        delete m1;
        delete m2;
}

TEST_F(Stack, Height)
{
        ats_int *m = new ats_int(0);

        /* Check that the stack updates correctly when not resizing. */
        EXPECT_EQ(0, ats_stk_height(stack));

        ats_stk_push(stack, m);
        EXPECT_EQ(1, ats_stk_height(stack));

        ats_stk_pop(stack);
        EXPECT_EQ(0, ats_stk_height(stack));

        ats_stk_pop(stack);
        EXPECT_EQ(0, ats_stk_height(stack));

        delete m;
}

TEST_F(Stack, IsEmpty)
{
        ats_int *m = new ats_int(0);

        /* Check that the stack updates correctly when not resizing. */
        EXPECT_EQ(ATS_TRUE, ats_stk_is_empty(stack));

        ats_stk_push(stack, m);
        EXPECT_EQ(ATS_FALSE, ats_stk_is_empty(stack));

        ats_stk_pop(stack);;
        EXPECT_EQ(ATS_TRUE, ats_stk_is_empty(stack));

        ats_stk_pop(stack);
        EXPECT_EQ(ATS_TRUE, ats_stk_is_empty(stack));

        delete m;
}

TEST_F(Stack, Pop)
{
        ats_int *m0 = new ats_int(0);
        ats_int *m1 = new ats_int(1);
        ats_int_ptr ptr;

        /* Check that the stack updates correctly when not resizing. */
        EXPECT_EQ(nullptr, ats_stk_pop(stack));

        ats_stk_push(stack, m0);
        ats_stk_push(stack, m1);

        ptr = static_cast<ats_int_ptr>(ats_stk_pop(stack));
        EXPECT_EQ(m1, ptr);
        EXPECT_EQ(*m1, *ptr);

        ptr = static_cast<ats_int_ptr>(ats_stk_pop(stack));
        EXPECT_EQ(m0, ptr);
        EXPECT_EQ(*m0, *ptr);

        delete m0;
        delete m1;
}
