/**
 * @file map_test.cpp
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 19/12/15
 * @brief Defines test cases for an AtsMap/
 * @see AtsMap
 */

#include <gtest/gtest.h>
#include <alteous/map.h>

class Map : public testing::Test
{
public:
        struct ats_map *map = nullptr;

        static ats_int ComparatorFn(void *left_ptr, void *right_ptr)
        {
                ats_int x = *static_cast<ats_int*>(left_ptr);
                ats_int y = *static_cast<ats_int*>(right_ptr);

                return (x < y) - (y < x);
        }

        static ats_int HashFn(void *key_ptr)
        {
                return *static_cast<ats_int_ptr>(key_ptr);
        }

        virtual void SetUp() override
        {
                /* Map with integer keys mapped to integer values. */
                map = ats_map_make(ats_int, ats_int, HashFn, ComparatorFn);
        }

        virtual void TearDown() override
        {
                ats_map_free(map);
                map = nullptr;
        }
};

TEST_F(Map, Init)
{
        ASSERT_NE(nullptr, map);
        ASSERT_NE(nullptr, map->buckets);

        EXPECT_EQ(16, map->capacity);
        EXPECT_EQ(0, map->size);
        EXPECT_EQ(sizeof(int), map->key_size);
        EXPECT_EQ(sizeof(int), map->key_size);
}

TEST_F(Map, Put)
{
        ats_int *key, *value;
        ats_error error;
        struct ats_map_pair pair;

        key = new ats_int;
        *key = 1;

        value = new ats_int;
        *value = 2;

        pair = { key, value };
        error = ats_map_put(map, pair);
        ASSERT_EQ(AtsNoError, error);
        EXPECT_EQ(1, map->size);

        pair = { key, value };
        error = ats_map_put(map, pair);
        EXPECT_EQ(AtsKeyAlreadyExists, error);
        ASSERT_EQ(1, map->size);

        *key = 2;
        pair = { key, value };
        error = ats_map_put(map, pair);
        EXPECT_EQ(AtsNoError, error);
        ASSERT_EQ(2, map->size);

        delete key;
        delete value;
}

TEST_F(Map, ContainsKey)
{
        ats_int *key, *value;
        struct ats_map_pair pair;

        key = new ats_int;
        value = new ats_int;

        EXPECT_EQ(ATS_FALSE, ats_map_contains_key(map, key));

        pair = { key, value };
        ats_map_put(map, pair);
        EXPECT_EQ(ATS_TRUE, ats_map_contains_key(map, key));

        ats_map_rm(map, key);
        EXPECT_EQ(ATS_FALSE, ats_map_contains_key(map, key));

        delete key;
        delete value;
}

TEST_F(Map, Get)
{
        ats_int *key, *value;
        struct ats_map_pair pair;

        key = new ats_int;
        *key = 1;
        value = new ats_int;
        *value = 2;

        pair = ats_map_get(map, key);
        ASSERT_EQ(nullptr, pair.key_ptr);
        EXPECT_EQ(nullptr, pair.value_ptr);

        pair = { key, value };
        ats_map_put(map, pair);
        pair = ats_map_get(map, key);
        EXPECT_EQ(*key, *static_cast<ats_int *>(pair.key_ptr));
        EXPECT_EQ(*value, *static_cast<ats_int *>(pair.value_ptr));

        delete key;
        delete value;
}

TEST_F(Map, Remove)
{
        ats_int *key, *value;
        struct ats_map_pair pair;

        key = new ats_int;
        value = new ats_int;

        *key = 1;
        *value = 2;

        pair = ats_map_rm(map, key);
        ASSERT_EQ(nullptr, pair.key_ptr);
        EXPECT_EQ(nullptr, pair.value_ptr);
        EXPECT_EQ(0, map->size);

        pair = { key, value };
        ats_map_put(map, pair);
        EXPECT_EQ(1, map->size);

        pair = ats_map_rm(map, key);
        EXPECT_EQ(*key, *static_cast<ats_int *>(pair.key_ptr));
        EXPECT_EQ(*value, *static_cast<ats_int *>(pair.value_ptr));
        EXPECT_EQ(0, map->size);
}

TEST_F(Map, Replace)
{
        ats_int *key_ptr, *value_ptr, *replacement_key_ptr, *replacement_value_ptr;
        struct ats_map_pair pair, replacement_pair, replaced_pair;

        key_ptr = new ats_int;
        value_ptr = new ats_int;
        replacement_key_ptr = new ats_int;
        replacement_value_ptr = new ats_int;

        *key_ptr = 1;
        *value_ptr = 2;

        pair = { key_ptr, value_ptr };
        ats_map_put(map, pair);

        *value_ptr = 3;
        replaced_pair = ats_map_replace(map, pair);
        ASSERT_NE(nullptr, pair.key_ptr);
        ASSERT_NE(nullptr, pair.value_ptr);
        EXPECT_EQ(pair.key_ptr, replaced_pair.key_ptr);
        EXPECT_EQ(pair.value_ptr, replaced_pair.value_ptr);
        EXPECT_EQ(*static_cast<ats_int_ptr>(replaced_pair.key_ptr),
                  *static_cast<ats_int_ptr>(pair.key_ptr));
        EXPECT_EQ(*static_cast<ats_int_ptr>(replaced_pair.value_ptr),
                  *static_cast<ats_int_ptr>(pair.value_ptr));

        *replacement_key_ptr = *key_ptr;
        *replacement_value_ptr = 4;
        replacement_pair = { replacement_key_ptr, replacement_value_ptr };
        replaced_pair = ats_map_replace(map, replacement_pair);
        ASSERT_NE(nullptr, pair.key_ptr);
        ASSERT_NE(nullptr, pair.value_ptr);
        EXPECT_EQ(key_ptr, replaced_pair.key_ptr);
        EXPECT_EQ(value_ptr, replaced_pair.value_ptr);
        EXPECT_EQ(*static_cast<ats_int_ptr>(key_ptr),
                  *static_cast<ats_int_ptr>(replaced_pair.key_ptr));
        EXPECT_EQ(*static_cast<ats_int_ptr>(value_ptr),
                  *static_cast<ats_int_ptr>(replaced_pair.value_ptr));

        delete key_ptr;
        delete value_ptr;
        delete replacement_key_ptr;
        delete replacement_value_ptr;
}
