/**
 * Created by David Harvey-Macaulay (alteous@outlook.com) on 22/12/15.
 */

#include <gtest/gtest.h>
#include <alteous/vec3.h>

TEST(Vec3f, Copy)
{
        struct ats_vec3f v = { 1.23f, 45.6f, 78.9f };
        struct ats_vec3f copy;
        ats_vec3f_copy(&copy, &v);

        EXPECT_EQ(0, memcmp(&v, &copy, sizeof(struct ats_vec3f)));
}

TEST(Vec3f, EqualsExact)
{
        struct ats_vec3f v0 = { 1.0f, 2.5f, -3.0f };
        struct ats_vec3f v1 = { 1.0f, 2.5f, -3.0f };

        EXPECT_TRUE(ats_vec3f_equals(&v0, &v1));
}

TEST(Vec3f, EqualsApproximate)
{
        struct ats_vec3f v0 = { 1.0f, 2.5f, -3.0f };
        struct ats_vec3f v1 = { 1.0f + 1e-8f, 2.5f - 1e-7f, -3.0f + 1e-10f };

        EXPECT_TRUE(ats_vec3f_equals(&v0, &v1));
}

TEST(Vec3f, NotEqual)
{
        struct ats_vec3f v0 = { 1.0f, 2.5f, -3.0f };
        struct ats_vec3f v1 = { 1.0f, 2.5f - 1e-5f, -3.0f };

        EXPECT_FALSE(ats_vec3f_equals(&v0, &v1));
}

TEST(Vec3f, Add)
{
        struct ats_vec3f v0 = { 1.0f, 2.0f, -3.0f };
        struct ats_vec3f v1 = { 1.5f, 2.0f, 2.5f };
        struct ats_vec3f expected = { 2.5f, 4.0f, -0.5f };

        ats_vec3f_add(&v0, &v1);
        EXPECT_TRUE(ats_vec3f_equals(&expected, &v0));
}

TEST(Vec3f, Direction)
{
        struct ats_vec3f p = { 1.0f, 2.0f, -3.0f };
        struct ats_vec3f q = { -0.4f, 0.6f, 2.2f };

        struct ats_vec3f expected = { -1.4f, -1.4f, 5.2f };
        struct ats_vec3f pq;
        ats_vec3f_direction(&pq, &p, &q);

        EXPECT_TRUE(ats_vec3f_equals(&pq, &expected));
}

TEST(Vec3f, Length)
{
        struct ats_vec3f v0 = { -0.5f, 4.0f, 3.2f };

        ats_float expected_length = 5.146843693f;
        ats_float length = ats_vec3f_length(&v0);
        EXPECT_FLOAT_EQ(expected_length, length);
}

TEST(Vec3f, CrossProduct)
{
        struct ats_vec3f p = { 1.5f, -2.8f, 0.2f };
        struct ats_vec3f q = { -3.1f, 0.0f, 1.7f };

        struct ats_vec3f expected_pq = { -4.76f, -3.17f, -8.68f };
        struct ats_vec3f pq;
        ats_vec3f_cross(&pq, &p, &q);
        EXPECT_TRUE(ats_vec3f_equals(&expected_pq, &pq));

        struct ats_vec3f expected_qp = { 4.76f, 3.17f, 8.68f };
        struct ats_vec3f qp;
        ats_vec3f_cross(&qp, &q, &p);
        EXPECT_TRUE(ats_vec3f_equals(&expected_qp, &qp));
}

TEST(Vec3f, Normalise)
{
        ats_bool normalised;

        struct ats_vec3f non_zero = { -0.5f, 4.0f, 3.2f };
        struct ats_vec3f expected = { -0.0971469176f, 0.7771753406f, 0.6217402725f };
        normalised = ats_vec3f_normalise(&non_zero);
        EXPECT_TRUE(normalised);
        EXPECT_TRUE(ats_vec3f_equals(&expected, &non_zero));

        struct ats_vec3f zero = { 1e-7f, -1e-8f, 0.0f };
        struct ats_vec3f zero_copy = zero;
        normalised = ats_vec3f_normalise(&zero);
        EXPECT_FALSE(normalised);
        EXPECT_TRUE(ats_vec3f_equals(&zero, &zero_copy));
}

TEST(Vec3f, Scale)
{
        ats_float scalar = 2.0f;
        struct ats_vec3f v = { -0.5f, 0.0f, 2.2f };
        struct ats_vec3f expected = { -1.0f, 0.0f, 4.4f };
        ats_vec3f_scale(&v, scalar);
        EXPECT_TRUE(ats_vec3f_equals(&expected, &v));
}

TEST(Vec3f, SurfaceNormal)
{
        ats_bool success;
        struct ats_vec3f v0 = { 1.0f, -1.0f, 1.0f };
        struct ats_vec3f v1 = { 2.0f, 2.5f, 0.0f };
        struct ats_vec3f v2 = { -1.0f, 2.0f, -0.5f };

        struct ats_vec3f surface_normal;
        struct ats_vec3f expected = { -0.2077353343f, 0.3231438533f, 0.9232681523f };
        success = ats_vec3f_surf_norm(&surface_normal, &v0, &v1, &v2);
        EXPECT_TRUE(success);
        EXPECT_TRUE(ats_vec3f_equals(&expected, &surface_normal));

        struct ats_vec3f w0 = { 1.0f, 1.0f, 1.0f };
        struct ats_vec3f w1 = { -0.5f, 2.5f, 0.0f };
        struct ats_vec3f w2 = w0;
        struct ats_vec3f surface_normal_copy = surface_normal;
        success = ats_vec3f_surf_norm(&surface_normal, &w0, &w1, &w2);
        EXPECT_FALSE(success);
}