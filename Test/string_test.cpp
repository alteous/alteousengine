/**
 * Created by David Harvey-Macaulay (alteous@outlook.com) on 21/12/15.
 */

#include <gtest/gtest.h>
#include <alteous/string.h>

#define NULL_CHAR '\0'

class String : public testing::Test
{
public:
        struct ats_string *string = nullptr;

        virtual void TearDown() override
        {
                ats_string_free(string);
                string = nullptr;
        }
};

static ats_bool is_null_terminated(const ats_char *expected, const struct ats_string *actual)
{
        return NULL_CHAR == actual->chars[strlen(expected)];
}

static const ats_char *hello_world = "Hello world!";
TEST_F(String, Init)
{
        string = ats_string_init(hello_world);
        ASSERT_NE(nullptr, string);
        ASSERT_NE(nullptr, string->chars);
        ASSERT_TRUE(is_null_terminated(hello_world, string));
        EXPECT_EQ(0, strcmp(hello_world, string->chars));
}

TEST_F(String, CopyLiteral)
{
        string = ats_string_init_empty();
        ats_string_copy_lit(hello_world, string);
        ASSERT_NE(nullptr, string->chars);
        ASSERT_TRUE(is_null_terminated(hello_world, string));
        EXPECT_EQ(0, strcmp(hello_world, string->chars));
}