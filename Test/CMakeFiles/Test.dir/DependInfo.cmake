# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/alteous/AlteousEngine/Test/angle_test.cpp" "/home/alteous/AlteousEngine/Test/CMakeFiles/Test.dir/angle_test.cpp.o"
  "/home/alteous/AlteousEngine/Test/bitfield_test.cpp" "/home/alteous/AlteousEngine/Test/CMakeFiles/Test.dir/bitfield_test.cpp.o"
  "/home/alteous/AlteousEngine/Test/entity_table_test.cpp" "/home/alteous/AlteousEngine/Test/CMakeFiles/Test.dir/entity_table_test.cpp.o"
  "/home/alteous/AlteousEngine/Test/entity_test.cpp" "/home/alteous/AlteousEngine/Test/CMakeFiles/Test.dir/entity_test.cpp.o"
  "/home/alteous/AlteousEngine/Test/map_test.cpp" "/home/alteous/AlteousEngine/Test/CMakeFiles/Test.dir/map_test.cpp.o"
  "/home/alteous/AlteousEngine/Test/mat4_test.cpp" "/home/alteous/AlteousEngine/Test/CMakeFiles/Test.dir/mat4_test.cpp.o"
  "/home/alteous/AlteousEngine/Test/stack_test.cpp" "/home/alteous/AlteousEngine/Test/CMakeFiles/Test.dir/stack_test.cpp.o"
  "/home/alteous/AlteousEngine/Test/string_test.cpp" "/home/alteous/AlteousEngine/Test/CMakeFiles/Test.dir/string_test.cpp.o"
  "/home/alteous/AlteousEngine/Test/transform_test.cpp" "/home/alteous/AlteousEngine/Test/CMakeFiles/Test.dir/transform_test.cpp.o"
  "/home/alteous/AlteousEngine/Test/vec3_test.cpp" "/home/alteous/AlteousEngine/Test/CMakeFiles/Test.dir/vec3_test.cpp.o"
  "/home/alteous/AlteousEngine/Test/wavefront_test.cpp" "/home/alteous/AlteousEngine/Test/CMakeFiles/Test.dir/wavefront_test.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "Alteous/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
