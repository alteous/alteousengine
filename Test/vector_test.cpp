/**
 * Created by David Harvey-Macaulay (alteous@outlook.com) on 22/12/15.
 */

#include <gtest/gtest.h>
#include <alteous/vector.h>

static const ats_size initial_capacity = 2;

class Vector : public testing::Test
{
public:
        struct ats_vtr *vector = nullptr;

        virtual void SetUp() override
        {
                vector = ats_vtr_init(initial_capacity);
        }

        virtual void TearDown() override
        {
                ats_vtr_free(vector);
                vector = nullptr;
        }
};

TEST_F(Vector, Init)
{
        ASSERT_NE(nullptr, vector);
        ASSERT_NE(nullptr, vector->elements);
        EXPECT_EQ(initial_capacity, vector->capacity);
        EXPECT_EQ(0, vector->length);
}

TEST_F(Vector, Push)
{
        ats_error error;
        ats_int *m0 = new ats_int(0);
        ats_int *m1 = new ats_int(1);
        ats_int *m2 = new ats_int(2);

        error = ats_vtr_push(vector, m0);
        ASSERT_EQ(AtsNoError, error);
        EXPECT_EQ(1, vector->length);
        EXPECT_EQ(2, vector->capacity);

        error = ats_vtr_push(vector, m0);
        ASSERT_EQ(AtsNoError, error);
        EXPECT_EQ(2, vector->length);
        EXPECT_EQ(2, vector->capacity);

        error = ats_vtr_push(vector, m2);
        if (AtsNoError != error) {
                ASSERT_NE(nullptr, vector);
                ASSERT_NE(nullptr, vector->elements);
                EXPECT_EQ(2, vector->length);
                EXPECT_EQ(2, vector->capacity);
        } else {
                EXPECT_EQ(3, vector->length);
                EXPECT_EQ(4, vector->capacity);
        }

        delete m0;
        delete m1;
        delete m2;
}

TEST_F(Vector, Get)
{
        ats_int *m0 = new ats_int(0);
        ats_int *m1 = new ats_int(1);
        ats_int *m2 = new ats_int(2);

        ats_vtr_push(vector, m0);
        ats_vtr_push(vector, m1);
        ats_vtr_push(vector, m2);
        EXPECT_EQ(3, vector->length);

        EXPECT_EQ(m0, ats_vtr_get(vector, 0));
        EXPECT_EQ(m1, ats_vtr_get(vector, 1));
        EXPECT_EQ(m2, ats_vtr_get(vector, 2));
        EXPECT_EQ(3, vector->length);

        delete m0;
        delete m1;
        delete m2;
}

TEST_F(Vector, Remove)
{
        void *element;
        ats_int *m0 = new ats_int(0);
        ats_int *m1 = new ats_int(1);
        ats_int *m2 = new ats_int(2);

        ats_vtr_push(vector, m0);
        ats_vtr_push(vector, m1);
        ats_vtr_push(vector, m2);
        EXPECT_EQ(3, vector->length);

        element = ats_vtr_rm(vector, 0);
        EXPECT_EQ(m0, element);
        EXPECT_EQ(2, vector->length);
        EXPECT_EQ(m1, ats_vtr_get(vector, 0));
        EXPECT_EQ(m2, ats_vtr_get(vector, 1));

        ats_vtr_push(vector, m0);
        element = ats_vtr_rm(vector, 1);
        EXPECT_EQ(m2, element);
        EXPECT_EQ(2, vector->length);
        EXPECT_EQ(m1, ats_vtr_get(vector, 0));
        EXPECT_EQ(m0, ats_vtr_get(vector, 1));

        ats_vtr_push(vector, m1);
        element = ats_vtr_rm(vector, 2);
        EXPECT_EQ(m1, element);
        EXPECT_EQ(2, vector->length);
        EXPECT_EQ(m1, ats_vtr_get(vector, 0));
        EXPECT_EQ(m0, ats_vtr_get(vector, 1));

        delete m0;
        delete m1;
        delete m2;
}
