/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file texture.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 29/11/15
 * @brief Contains an implementation of a texture loader.
 */

#include <stb/stb_image.h>
#include <stdlib.h>

#include "alteous/gl.h"
#include "alteous/texture.h"

ATSAPI struct ats_tex* ATSCALL ats_tex_init(const char *file_name, ats_tex_err_fn error_fn)
{
        struct ats_tex *texture = malloc(sizeof(struct ats_tex));

        glGenTextures(1, &(texture->id));

        int width, height, components;
        ats_ubyte* image = stbi_load(file_name, &width, &height, &components, STBI_rgb);

        if (image) {
                glBindTexture(GL_TEXTURE_2D, texture->id);
                glTexImage2D(GL_TEXTURE_2D,
                             0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
                glGenerateMipmap(GL_TEXTURE_2D);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        } else {
                error_fn(stbi_failure_reason());
                free(texture);
                texture = NULL;
        }

        return texture;
}

ATSAPI void ATSCALL ats_tex_free(struct ats_tex *texture)
{
        glDeleteTextures(1, &texture->id);
}
