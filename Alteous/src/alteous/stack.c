/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file stack.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 06/12/15
 * @brief Implementation of a generic stack data structure.
 */

#include <stdlib.h>
#include <stdio.h>

#include "alteous/memory.h"
#include "alteous/stack.h"
#include "alteous/assertion.h"

static const ats_size ats_stack_grow_factor = 2;

ATSAPI struct ats_stk* ATSCALL ats_stk_init(ats_size initial_capacity)
{
        struct ats_stk *stack = malloc(sizeof(*stack));

        stack->elements = 0;
        stack->height   = 0;
        stack->top      = 0;
        stack->capacity = initial_capacity;
        stack->elements = ATS_MALLOC(stack->capacity, void*);

        if (0 == stack->elements) {
                free(stack);
                stack = 0;
        }

        return stack;
}

ATSAPI void ATSCALL ats_stk_free(struct ats_stk *stack)
{
        ATS_DEBUG_ASSERT(NULL != stack);

        free(stack->elements);
        free(stack);
}

ATSAPI ats_size ATSCALL ats_stk_height(const struct ats_stk *stack)
{
        ATS_DEBUG_ASSERT(NULL != stack);
        
        return stack->height;
}

ATSAPI ats_bool ATSCALL ats_stk_is_empty(const struct ats_stk *stack)
{             
        ATS_DEBUG_ASSERT(NULL != stack);
        
        return 0 == ats_stk_height(stack);
}

ATSAPI const void* ATSCALL ats_stk_peek(const struct ats_stk *stack)
{
        ATS_DEBUG_ASSERT(NULL != stack);

        return stack->top;
}

ATSAPI void* ATSCALL ats_stk_pop(struct ats_stk *stack)
{
        ATS_DEBUG_ASSERT(NULL != stack);
        
        void *element = NULL;
        if (stack->height > 0) {
                element = stack->top;
                stack->top = 0;
                stack->height--;
                stack->top = stack->height > 0 ? stack->elements[stack->height - 1] : 0;
        }

        return element;
}

static ats_error AtsStack_grow(struct ats_stk *stack)
{
        ATS_DEBUG_ASSERT(NULL != stack);
        
        void **array = ATS_REALLOC(stack->elements, ats_stack_grow_factor * stack->height, void**);
        if (NULL == array) {
                return AtsAllocFailed;
        }

        stack->elements = array;
        stack->capacity *= ats_stack_grow_factor;
        stack->top = stack->elements[stack->height];
        return AtsNoError;
}

ATSAPI ats_error ATSCALL ats_stk_push(struct ats_stk *stack, void *element)
{
        ATS_DEBUG_ASSERT(NULL != stack);
        ATS_DEBUG_ASSERT(NULL != element);

        if (stack->height == stack->capacity) {
                if (AtsStack_grow(stack)) {
                        return AtsAllocFailed;
                }
        }

        stack->elements[stack->height] = stack->top = element;
        stack->height++;

        return AtsNoError;
}
