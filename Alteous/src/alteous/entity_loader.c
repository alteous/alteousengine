/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file entity_loader.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 02/01/16
 * @brief Implementation of the engine JSON entity data parser and loader.
 *
 * This file makes heavy use of the jansson library (http://www.digip.org/jansson/) for handling
 * JSON data input.
 */

#include "alteous/entity_loader.h"

#include <string.h>
#include <jansson/jansson.h>

#include "alteous/file.h"
#include "alteous/entity.h"
#include "alteous/macros.h"

struct ats_ent_obj {
        ats_comp_bitfield flags;
        struct ats_comp_pos position;
        struct ats_comp_mvt movement;
        struct ats_comp_rdr render;
};

static void load_position_component(json_t *values, struct ats_ent_obj *obj)
{
        json_t *position, *x_pair, *y_pair, *z_pair;
        obj->flags |= AtsCompPosBit;

        position = json_object_get(values, "coordinate");
        x_pair = json_object_get(position, "x");
        y_pair = json_object_get(position, "y");
        z_pair = json_object_get(position, "z");

        obj->position.coordinate.x = (ats_float) json_number_value(x_pair);
        obj->position.coordinate.y = (ats_float) json_number_value(y_pair);
        obj->position.coordinate.z = (ats_float) json_number_value(z_pair);
}

static void load_movement_component(json_t *values, struct ats_ent_obj *obj)
{
        json_t *velocity, *acceleration, *x_pair, *y_pair, *z_pair;

        obj->flags |= AtsCompMvtBit;

        velocity = json_object_get(values, "velocity");
        x_pair = json_object_get(velocity, "x");
        y_pair = json_object_get(velocity, "y");
        z_pair = json_object_get(velocity, "z");

        obj->movement.velocity.x = (ats_float) json_real_value(x_pair);
        obj->movement.velocity.y = (ats_float) json_real_value(y_pair);
        obj->movement.velocity.z = (ats_float) json_real_value(z_pair);

        acceleration = json_object_get(values, "acceleration");
        x_pair = json_object_get(acceleration, "x");
        y_pair = json_object_get(acceleration, "y");
        z_pair = json_object_get(acceleration, "z");

        obj->movement.acceleration.x = (ats_float) json_real_value(x_pair);
        obj->movement.acceleration.y = (ats_float) json_real_value(y_pair);
        obj->movement.acceleration.z = (ats_float) json_real_value(z_pair);
}

static void load_render_component(json_t *values, struct ats_ent_obj *obj)
{
        json_t *mesh;
        const ats_char *mesh_name;

        obj->flags |= AtsCompRdrBit;

        mesh = json_object_get(values, "mesh");
        mesh_name = json_string_value(mesh);

        obj->render.id = ats_renderable_get_id(mesh_name);
}

static void load_component(json_t *comp, struct ats_ent_obj *obj)
{
        json_t *comp_type = json_object_get(comp, "type");
        json_t *value_obj = json_object_get(comp, "values");
        if (NULL == comp_type || NULL == value_obj) {
                return;
        }

        const ats_char *type_value = json_string_value(comp_type);
        if (ATS_STR_EQ(type_value, "position")) {
                load_position_component(value_obj, obj);
        } else if (ATS_STR_EQ(type_value, "movement")) {
                load_movement_component(value_obj, obj);
        } else if (ATS_STR_EQ(type_value, "render")) {
                load_render_component(value_obj, obj);
        }
}

static void load_component_array(json_t *comp_array, struct ats_ent_obj *obj)
{
        ats_size index;
        json_t *comp;
        json_array_foreach(comp_array, index, comp) {
                load_component(comp, obj);
        }
}

static ats_size load_object_array(json_t *object_array)
{
        ats_size objects_loaded = 0;
        ats_size index = 0;
        json_t *object = NULL;
        json_t *comp_array = NULL;
        struct ats_ent_obj entity_object;
        ats_ent_uid id;
        json_array_foreach(object_array, index, object) {
                entity_object.flags = 0;
                comp_array = json_object_get(object, "components");
                load_component_array(comp_array, &entity_object);
                if (0 != entity_object.flags) {
                        id = ats_ent_init(entity_object.flags);

                        struct ats_comp_pos *pos = ats_ent_get_comp(id, AtsCompPos);
                        pos->coordinate = entity_object.position.coordinate;

                        struct ats_comp_rdr *rdr = ats_ent_get_comp(id, AtsCompRdr);
                        rdr->id = entity_object.render.id;

                        objects_loaded++;
                }
        }

        return objects_loaded;
}

ATSAPI ats_size ATSCALL ats_ent_loader_load(const ats_char *path, ats_error_fn error_fn)
{
        ats_size objects_loaded = 0;
        ats_file file = fopen(path, "rb");
        if (NULL == file) {
                error_fn(AtsFileCouldNotOpen, "Could not open JSON file.");
                return objects_loaded;
        }

        json_error_t error;
        json_t *json = json_loadf(file, JSON_DECODE_ANY, &error);
        if (NULL == json) {
                error_fn(AtsParseError, error.text);
                return objects_loaded;
        }

        json_t *object_array = json_object_get(json, "objects");
        objects_loaded = load_object_array(object_array);

        json_decref(json);
        fclose(file);

        return objects_loaded;
}

