/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file detail/context.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 15/12/15
 * @brief Implementation of the engine's OpenGL context creation.
 */

#include "alteous/detail/context.h"

#include <SDL2/SDL.h>
#include <stdio.h>

#ifdef EMSCRIPTEN
#include <stdlib.h>
#endif

#include "alteous/detail/cursor.h"

static SDL_GLContext context      = NULL;
static SDL_Window *window         = NULL;
static ats_int context_width      = 0;
static ats_int context_height     = 0;
static ats_bool context_is_active = ATS_FALSE;

static void init_sdl()
{
        if (0 != SDL_Init(SDL_INIT_VIDEO)) {
                fprintf(stderr, "Fatal SDL error: %s\n", SDL_GetError());
                exit(EXIT_FAILURE);
        }

        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
        SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

        window = SDL_CreateWindow(ATS_INIT_CONTEXT_TITLE,
                                  SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED,
                                  ATS_INIT_CONTEXT_WIDTH,
                                  ATS_INIT_CONTEXT_HEIGHT,
                                  SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
        if (0 == window) {
                fprintf(stderr, "Fatal SDL error: %s\n", SDL_GetError());
                exit(EXIT_FAILURE);
        }

        context = SDL_GL_CreateContext(window);
        if (0 == context) {
                fprintf(stderr, "Fatal SDL error: %s\n", SDL_GetError());
                exit(EXIT_FAILURE);
        }

        if (0 != SDL_GL_MakeCurrent(window, context)) {
                fprintf(stderr, "Fatal SDL error: %s\n", SDL_GetError());
                exit(EXIT_FAILURE);
        }
}

ATSAPI void ATSCALL _ats_context_init()
{
        context_width = ATS_INIT_CONTEXT_WIDTH;
        context_height = ATS_INIT_CONTEXT_HEIGHT;
        init_sdl();
        context_is_active = ATS_TRUE;
}

ATSAPI void ATSCALL _ats_context_free()
{
        SDL_DestroyWindow(window);
        SDL_GL_DeleteContext(context);
}

ATSAPI void ATSCALL _ats_context_set_size(ats_int width, ats_int height)
{
        context_width = width;
        context_height = height;
}

ATSAPI void ATSCALL _ats_context_get_size(ats_int *width, ats_int *height)
{
        *width = context_width;
        *height = context_height;
}


ATSAPI ats_bool ATSCALL _ats_context_is_active()
{
        return context_is_active;
}

ATSAPI void ATSCALL _ats_context_close()
{
        context_is_active = ATS_FALSE;
}

ATSAPI void ATSCALL _ats_context_swap_buffers()
{
        SDL_GL_SwapWindow(window);
        _ats_cursor_reset_delta_pos();
}
