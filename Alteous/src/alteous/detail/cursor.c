/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file detail/cursor.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 16/12/15
 * @brief Implementation of the engine's mouse cursor listener.
 */

#include "alteous/detail/cursor.h"

#include <SDL2/SDL_mouse.h>

static struct ats_vec2i current_position;
static struct ats_vec2i last_position;
static struct ats_vec2i delta_position;

static const struct ats_vec2i zero_vector = { 0, 0 };

ATSAPI void ATSCALL _ats_cursor_init()
{
        SDL_SetRelativeMouseMode(SDL_TRUE);
        SDL_ShowCursor(0);

        ats_vec2i_copy(&current_position, &zero_vector);
        ats_vec2i_copy(&last_position, &zero_vector);
        ats_vec2i_copy(&delta_position, &zero_vector);
}

ATSAPI void ATSCALL _ats_cursor_get_delta_pos(struct ats_vec2i *delta)
{
        ats_vec2i_copy(delta, &delta_position);
}

ATSAPI void ATSCALL _ats_cursor_reset_delta_pos()
{
        ats_vec2i_copy(&delta_position, &zero_vector);
}

ATSAPI void ATSCALL _ats_cursor_get_pos(struct ats_vec2i *position)
{
        ats_vec2i_copy(position, &current_position);
}

ATSAPI void ATSCALL _ats_cursor_set_pos(const struct ats_vec2i *new_position)
{
        ats_vec2i_copy(&last_position, &current_position);

        ats_vec2i_copy(&current_position, new_position);

        delta_position.x = current_position.x - last_position.x;
        delta_position.y = current_position.y - last_position.y;
}
