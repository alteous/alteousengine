/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file detail/fp_camera.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 19/12/15
 * @brief Implementation of the engine's OpenGL context creation.
 */

#include "alteous/fp_camera.h"

#include <tgmath.h>

#include "alteous/cursor.h"
#include "alteous/camera.h"
#include "alteous/keyboard.h"

static struct ats_cam camera;

static const ats_float move_speed = 1.0f / 20.0f;
static const ats_float look_speed = 1.0f / 30.0f;

/**
 * @brief Moves the camera forwards and horizontally in the current look direction.
 */
static void _ats_fp_cam_move_forward()
{
        camera.position.x -= move_speed * sinf(ats_angle_radians(&camera.yaw));
        camera.position.z -= move_speed * cosf(ats_angle_radians(&camera.yaw));
}

/**
 * @brief Moves the camera backward and horizontally in the current look direction.
 */
static void _ats_fp_cam_move_backward()
{
        camera.position.x += move_speed * sinf(ats_angle_radians(&camera.yaw));
        camera.position.z += move_speed * cosf(ats_angle_radians(&camera.yaw));
}

/**
 * @brief Moves the camera horizontally 90 degrees right of the current look direction.
 */
static void _ats_fp_cam_move_left()
{
        camera.position.x -= move_speed * cosf(ats_angle_radians(&camera.yaw));
        camera.position.z += move_speed * sinf(ats_angle_radians(&camera.yaw));
}

/**
 * @brief Moves the camera horizontally 90 degrees right of the current look direction.
 */
static void _ats_fp_cam_move_right()
{
        camera.position.x += move_speed * cosf(ats_angle_radians(&camera.yaw));
        camera.position.z -= move_speed * sinf(ats_angle_radians(&camera.yaw));
}

/**
 * @brief Moves the camera directly up in the y axis.
 */
static void _ats_fp_cam_move_up()
{
        camera.position.y += move_speed;
}

/**
 * @brief Moves the camera directly down in the z axis.
 */
static void _ats_fp_cam_move_down()
{
        camera.position.y -= move_speed;
}

/**
 * @brief Rotates the camera up in the x axis.
 */
static void _ats_fp_cam_pitch_up()
{
        struct ats_angle angle = { -look_speed, AtsRadians };
        ats_angle_add(&camera.pitch, &angle);
}

/**
 * @brief Rotates the camera down in the x axis.
 */
static void _ats_fp_cam_pitch_down()
{
        struct ats_angle angle = { look_speed, AtsRadians };
        ats_angle_add(&camera.pitch, &angle);
}

/**
 * @brief Rotates the camera in the y axis.
 */
static void _ats_fp_cam_yaw_clockwise()
{
        struct ats_angle angle = { -look_speed, AtsRadians };
        ats_angle_add(&camera.yaw, &angle);
}

/**
 * @brief Rotates the camera in the y axis.
 */
static void _ats_fp_cam_yaw_anticlockwise()
{
        struct ats_angle angle = { look_speed, AtsRadians };
        ats_angle_add(&camera.yaw, &angle);
}

/**
 * @brief Rotates the camera in the z axis.
 */
static void _ats_fp_cam_roll_clockwise()
{
        struct ats_angle angle = { -look_speed, AtsRadians };
        ats_angle_add(&camera.roll, &angle);
}

/**
 * @brief Rotates the camera in the z axis.
 */
static void _ats_fp_cam_roll_anticlockwise()
{
        struct ats_angle angle = { look_speed, AtsRadians };
        ats_angle_add(&camera.roll, &angle);
}

ATSAPI void ATSCALL _ats_fp_cam_init()
{
        camera = ats_cam_make_default();
}

ATSAPI void ATSCALL _ats_fp_cam_update()
{
        if (ats_keyboard_is_key_down(SDLK_w)) {
                _ats_fp_cam_move_forward();
        }

        if (ats_keyboard_is_key_down(SDLK_s)) {
                _ats_fp_cam_move_backward();
        }

        if (ats_keyboard_is_key_down(SDLK_a)) {
                _ats_fp_cam_move_left();
        }

        if (ats_keyboard_is_key_down(SDLK_d)) {
                _ats_fp_cam_move_right();
        }

        if (ats_keyboard_is_key_down(SDLK_e)) {
                _ats_fp_cam_roll_clockwise();
        }

        if (ats_keyboard_is_key_down(SDLK_q)) {
                _ats_fp_cam_roll_anticlockwise();
        }

        if (ats_keyboard_is_key_down(SDLK_z)) {
                _ats_fp_cam_move_down();
        }

        if (ats_keyboard_is_key_down(SDLK_x)) {
                _ats_fp_cam_move_up();
        }

        struct ats_vec2i cursor_movement;
        ats_cursor_get_delta_pos(&cursor_movement);

        if (cursor_movement.x) {
                if (cursor_movement.x > 0)
                        _ats_fp_cam_yaw_clockwise();
                else
                        _ats_fp_cam_yaw_anticlockwise();
        }
        if (cursor_movement.y) {
                if (cursor_movement.y > 0)
                        _ats_fp_cam_pitch_up();
                else
                        _ats_fp_cam_pitch_down();
        }
}

ATSAPI void ATSCALL _ats_fp_cam_compute_view_matrix(struct ats_mat4 *matrix)
{
        ats_cam_compute_view_matrix(matrix, &camera);
}

ATSAPI void ATSCALL _ats_fp_cam_get_position(struct ats_vec3f *position)
{
        *position = camera.position;
}
