/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file camera.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 13/12/15
 * @brief Implementation of an abstract camera.
 */

#include "alteous/camera.h"

#include <tgmath.h>
#include <assert.h>

#include "alteous/transform.h"

#define ATS_NON_ZERO(x) (fabsf((x)) > 1e-6)

ATSAPI ats_error ATSCALL ats_cam_compute_view_matrix(struct ats_mat4 *output,
                                                     const struct ats_cam *camera)
{
#ifdef DEBUG
        assert(NULL != output);
        assert(NULL != camera);
#endif
        struct ats_transform *transform = ats_transform_init();
        if (NULL == transform) {
                return AtsAllocFailed;
        }

        ats_transform_untranslate(transform, &camera->position);

        if (ATS_NON_ZERO(camera->yaw.value)) {
                ats_transform_unrotY(transform, &camera->yaw);
        }

        if (ATS_NON_ZERO(camera->pitch.value)) {
                ats_transform_unrotX(transform, &camera->pitch);
        }

        if (ATS_NON_ZERO(camera->roll.value)) {
                ats_transform_unrotZ(transform, &camera->roll);
        }

        ats_transform_compose(transform, output);
        ats_transform_free(transform);

        return AtsNoError;
}

ATSAPI struct ats_cam ATSCALL ats_cam_make_default()
{
        struct ats_cam camera;
        camera.position.x = camera.position.y  = camera.position.z = 0.0f;
        camera.position.z = 5.0f;
        camera.yaw.value  = camera.pitch.value = camera.roll.value = 0.0f;
        camera.yaw.type   = camera.pitch.type  = camera.roll.type  = AtsRadians;

        return camera;
}

#undef ATS_NON_ZERO
