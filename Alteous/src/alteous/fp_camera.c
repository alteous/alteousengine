/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file fp_camera.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 19/12/15
 * @brief Implementation of the engine's first person camera interface.
 */

#include "alteous/detail/fp_camera.h"

ATSAPI void ATSCALL ats_fp_cam_get_position(struct ats_vec3f *position)
{
        _ats_fp_cam_get_position(position);
}

ATSAPI void ATSCALL ats_fp_cam_get_view_matrix(struct ats_mat4 *matrix)
{
        _ats_fp_cam_compute_view_matrix(matrix);
}

ATSAPI void ATSCALL ats_fp_cam_update()
{
        _ats_fp_cam_update();
}
