/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file angle.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 06/12/15
 * @brief Implementation of the angle data structure.
 */

#include <stdio.h>
#include <assert.h>

#include "alteous/angle.h"
#include "alteous/constants.h"

ATSAPI void ATSCALL ats_angle_add(struct ats_angle *angle, const struct ats_angle *arg)
{
#ifdef DEBUG
        assert(NULL != angle);
        assert(NULL != arg);
#endif
        switch (angle->type) {
                case AtsRadians:
                        angle->value += ats_angle_radians(arg);
                        break;
                case AtsDegrees:
                        angle->value += ats_angle_degrees(arg);
                        break;
                default:
                        fprintf(stderr, "ats_angle error: Unknown angle type %d.\n", angle->type);
                        break;
        }
}

ATSAPI void ATSCALL ats_angle_copy(const struct ats_angle *angle, struct ats_angle *destination)
{
#ifdef DEBUG
        assert(NULL != angle);
        assert(NULL != destination);
#endif
        destination->type = angle->type;
        destination->value = angle->value;
}

ATSAPI ats_float ATSCALL ats_angle_degrees(const struct ats_angle *angle)
{
#ifdef DEBUG
        assert(NULL != angle);
#endif
        switch (angle->type) {
                case AtsRadians:
                        return 180.0f * angle->value / ATS_PI;
                case AtsDegrees:
                        return angle->value;
                default:
                        fprintf(stderr, "ats_angle error: Unknown angle type %d.\n", angle->type);
                        return angle->value;
        }
}

ATSAPI ats_float ATSCALL ats_angle_radians(const struct ats_angle *angle)
{
#ifdef DEBUG
        assert(NULL != angle);
#endif
        switch (angle->type) {
                case AtsRadians:
                        return angle->value;
                case AtsDegrees:
                        return ATS_PI * angle->value / 180.0f;
                default:
                        fprintf(stderr, "ats_angle error: Unknown angle type %d.\n", angle->type);
                        return angle->value;
        }
}
