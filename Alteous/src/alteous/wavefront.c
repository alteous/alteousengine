/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file wavefront.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 22/11/15
 * @brief Implementation of the engine's Wavefront .obj loader.
 */

#include "alteous/wavefront.h"

#include <stdio.h>
#include <ctype.h>
#include <alteous/mesh.h>
#include <string.h>

#include "alteous/assertion.h"
#include "alteous/file.h"

#define NULL_CHAR '\0'

/* The maximum permitted indices - note the +1 since Wavefront indices start at 1. */
static const ats_size maximum_index_count = 1 + ATS_USHORT_MAX;

/* The initial number of faces. */
static const ats_size initial_face_limit = 512;

/* Vertex position_vectors are 3 component vectors. */
static const ats_size position_vector_size = 3;

/* Vertex normals are 3 component vectors. */
static const ats_size normal_vector_size = 3;

/* Texture co-ordinates are 2 component vectors. */
static const ats_size texture_coordinate_size = 2;

static ats_error parse_position(const ats_char *line, struct ats_wft *wft)
{
        if (wft->position_count == maximum_index_count) {
                return AtsTooManyVertices;
        }

        ats_float x, y, z;
        ats_int scan_count = sscanf(line, "v %f %f %f", &x, &y, &z);
        if (position_vector_size != scan_count) {
                return AtsParseError;
        }

        wft->position_vectors[position_vector_size * wft->position_count + 0] = x;
        wft->position_vectors[position_vector_size * wft->position_count + 1] = y;
        wft->position_vectors[position_vector_size * wft->position_count + 2] = z;
        wft->position_count++;

        return AtsNoError;
}

static ats_error parse_normal(const ats_char *line, struct ats_wft *wft)
{
        if (wft->normal_count == maximum_index_count) {
                return AtsTooManyVertices;
        }

        ats_float x, y, z;
        ats_int scan_count = sscanf(line, "vn %f %f %f", &x, &y, &z);
        if (normal_vector_size != scan_count) {
                return AtsParseError;
        }

        wft->normal_vectors[normal_vector_size * wft->normal_count + 0] = x;
        wft->normal_vectors[normal_vector_size * wft->normal_count + 1] = y;
        wft->normal_vectors[normal_vector_size * wft->normal_count + 2] = z;
        wft->normal_count++;

        return AtsNoError;
}

static ats_error parse_texture_coordinate(const ats_char *line, struct ats_wft *wft)
{
        if (wft->texture_count == maximum_index_count) {
                return AtsTooManyVertices;
        }

        ats_float s, t;
        ats_int scan_count = sscanf(line, "vt %f %f", &s, &t);
        if (texture_coordinate_size != scan_count) {
                return AtsParseError;
        }

        wft->texture_coordinates[texture_coordinate_size * wft->texture_count + 0] = s;
        wft->texture_coordinates[texture_coordinate_size * wft->texture_count + 1] = t;
        wft->texture_count++;

        return AtsNoError;
}

static ats_error parse_vertex_attribute(const ats_char *line, struct ats_wft *wft)
{
        /* First character in the line was 'v'. */
        switch (line[1]) {
                case ' ':
                        /* "v " == vertex coordinate vector. */
                        return parse_position(line, wft);
                case 'n':
                        /* "vn" == vertex normal vector. */
                        return parse_normal(line, wft);
                case 't':
                        /* "vt" == vertex texture co-ordinate. */
                        return parse_texture_coordinate(line, wft);
                default:
                        /* skip this line. */
                        return AtsNoError;
        }
}

typedef enum {
        AtsWftVertexPosition = 0, AtsWftVertexTexture = 1, AtsWftVertexNormal = 2
} ats_wft_idx_type;

static ats_error parse_face(const ats_char *line, struct ats_wft *wft)
{
        struct ats_wft_face *face = wft->faces + wft->face_count;

        ats_wft_idx_type idx_type = AtsWftVertexPosition;
        ats_int idx;
        ats_int positions_scanned = 0;
        ats_int normals_scanned = 0;
        ats_int textures_scanned = 0;
        ats_bool parse_was_successful;

        for (ats_int i = 1; NULL_CHAR != line[i]; i++) {
                if (isdigit(line[i])) {
                        /* Get the number at line[i]. */
                        parse_was_successful = 1 == sscanf(line + i, "%d", &idx);
                        if (ATS_FALSE == parse_was_successful) {
                                return AtsParseError;
                        }
                        if (idx > 1 + ATS_USHORT_MAX) {
                                /* GLES2 can only handle indices from 0 to 2^16 - 1. */
                                return AtsIndexOutOfBounds;
                        }
                        /* Put the parsed index into the correct array. */
                        switch (idx_type) {
                                case AtsWftVertexPosition:
                                        if (positions_scanned < 3) {
                                                face->position_index[positions_scanned] = (ats_ushort) idx;
                                                positions_scanned++;
                                                break;
                                        } else {
                                                return AtsTooManyVertices;
                                        }
                                case AtsWftVertexTexture:
                                        if (textures_scanned < 3) {
                                                face->texture_index[textures_scanned] = (ats_ushort) idx;
                                                textures_scanned++;
                                                break;
                                        } else {
                                                return AtsTooManyVertices;
                                        }
                                case AtsWftVertexNormal:
                                        if (normals_scanned < 3) {
                                                face->normal_index[normals_scanned] = (ats_ushort) idx;
                                                normals_scanned++;
                                                idx_type = AtsWftVertexPosition;
                                                break;
                                        } else {
                                                return AtsTooManyVertices;
                                        }
                        }
                } else if ('/' == line[i]) {
                        /* Cycle through the index types. */
                        idx_type = (ats_wft_idx_type) (1 + idx_type) % 3;
                }
        }

        wft->face_count++;

        return AtsNoError;
}

static ats_error parse_raw_line(const ats_char *line, struct ats_wft *wft)
{
        switch (line[0]) {
                case 'v':
                        return parse_vertex_attribute(line, wft);
                case 'f':
                        return parse_face(line, wft);
                default:
                        return AtsNoError;
        }
}

static struct ats_wft* _ats_wft_init()
{
        struct ats_wft *wft = malloc(sizeof(*wft));
        if (NULL == wft) {
                return NULL;
        }

        /*
         * Position vector: 3 components per vertex.
         * Do not resize.
         */
        wft->position_count = 0;
        wft->position_vectors = malloc(ATS_USHORT_MAX * position_vector_size * sizeof(*wft->position_vectors));
        if (NULL == wft->position_vectors) {
                goto free_wft;
        }

        /*
         * Normal vector: 3 components per vertex.
         * Do not resize.
         */
        wft->normal_count = 0;
        wft->normal_vectors = malloc(ATS_USHORT_MAX * normal_vector_size * sizeof(*wft->normal_vectors));
        if (NULL == wft->normal_vectors) {
                goto free_posn;
        }

        /*
         * Texture co-ordinate: 2 components per vertex.
         * Do not resize.
         */
        wft->texture_count = 0;
        wft->texture_coordinates = malloc(ATS_USHORT_MAX * texture_coordinate_size * sizeof(*wft->texture_coordinates));
        if (NULL == wft->texture_coordinates) {
                goto free_norm;
        }

        /*
         * Position vector: 3 components per vertex.
         * Resize if necessary.
         */
        wft->face_count = 0;
        wft->face_limit = initial_face_limit; /* Resizable. */
        wft->faces = calloc(wft->face_limit, sizeof(*wft->faces));

        if (NULL == wft->faces) {
                goto free_text;
        }

        return wft;

        free_text:
                free(wft->texture_coordinates);
        free_norm:
                free(wft->normal_vectors);
        free_posn:
                free(wft->position_vectors);
        free_wft:
                free(wft);

        return NULL;
}

ATSAPI struct ats_wft* ATSCALL ats_wft_init(const ats_char *file_path, ats_error *error)
{
        ats_file file = fopen(file_path, "r");
        if (NULL == file) {
                *error = AtsFileCouldNotOpen;
                return NULL;
        }

        struct ats_wft *wft = _ats_wft_init();
        if (NULL == wft) {
                *error = AtsAllocFailed;
                fclose(file);
                return NULL;
        }

        ats_char line_buffer[512];
        while (fgets(line_buffer, 512, file)) {
                *error = parse_raw_line(line_buffer, wft);
                if (AtsNoError != *error) {
                        return NULL;
                }
        }

        fclose(file);

        return wft;
}

ATSAPI void ATSCALL ats_wft_free(struct ats_wft *wft)
{
        ATS_DEBUG_ASSERT(NULL != wft);

        free(wft->position_vectors);
        free(wft->normal_vectors);
        free(wft->texture_coordinates);
        free(wft->faces);
        free(wft);
}

ATSAPI ats_renderable_uid ATSCALL ats_wft_load(const ats_char *path, const ats_char *name)
{
        ats_error error;
        struct ats_wft *wft = ats_wft_init(path, &error);
        if (AtsNoError != error) {
                return -1;
        }

        struct ats_mesh *mesh = ats_wft_to_mesh(wft);
        if (NULL == mesh) {
                ats_wft_free(wft);
                return -1;
        }

        mesh->name = name;
        ats_renderable_uid id = -1;
        error = ats_renderable_init(&id, mesh);
        if (AtsNoError != error) {
                ats_wft_free(wft);
                free(mesh->positions);
                free(mesh->textures);
                free(mesh->normals);
                free(mesh);
                return -1;
        }

        ats_wft_free(wft);
        free(mesh->positions);
        free(mesh->textures);
        free(mesh->normals);
        free(mesh);

        return id;
}

ATSAPI struct ats_mesh* ATSCALL ats_wft_to_mesh(const struct ats_wft *wft)
{
        struct ats_mesh *mesh = malloc(sizeof(*mesh));
        mesh->positions = malloc(3 * wft->face_count * position_vector_size * sizeof(ats_float));
        mesh->normals = malloc(3 * wft->face_count * normal_vector_size * sizeof(ats_float));
        mesh->textures = malloc(3 * wft->face_count * texture_coordinate_size * sizeof(ats_float));
        mesh->vertex_count = 3 * (ats_int) wft->face_count;
        mesh->indices = NULL;
        mesh->indices = NULL;
        mesh->index_count = 0;

        struct ats_wft_face *face = NULL;
        ats_float *v0, *v1, *v2;

        if (wft->position_count > 0) {
                ats_size j = 0;
                for (ats_size i = 0; i < wft->face_count; i++) {
                        face = wft->faces + i;
                        v0 = wft->position_vectors + (face->position_index[0] - 1);
                        v1 = wft->position_vectors + (face->position_index[1] - 1);
                        v2 = wft->position_vectors + (face->position_index[2] - 1);
                        memcpy(mesh->positions + j, v0, 3 * sizeof(ats_float));
                        j++;
                        memcpy(mesh->positions + j, v1, 3 * sizeof(ats_float));
                        j++;
                        memcpy(mesh->positions + j, v2, 3 * sizeof(ats_float));
                        j++;
                }
        }

        if (wft->normal_count > 0) {
                ats_size j = 0;
                for (ats_size i = 0; i < wft->face_count; i++) {
                        face = wft->faces + i;
                        v0 = wft->normal_vectors + (face->normal_index[0] - 1);
                        v1 = wft->normal_vectors + (face->normal_index[1] - 1);
                        v2 = wft->normal_vectors + (face->normal_index[2] - 1);
                        memcpy(mesh->normals + j, v0, 3 * sizeof(ats_float));
                        j++;
                        memcpy(mesh->normals + j, v1, 3 * sizeof(ats_float));
                        j++;
                        memcpy(mesh->normals + j, v2, 3 * sizeof(ats_float));
                        j++;
                }
        }

        if (wft->texture_count > 0) {
                ats_size j = 0;
                for (ats_size i = 0; i < wft->face_count; i++) {
                        face = wft->faces + i;
                        v0 = wft->texture_coordinates + (face->texture_index[0] - 1);
                        v1 = wft->texture_coordinates + (face->texture_index[1] - 1);
                        v2 = wft->texture_coordinates + (face->texture_index[2] - 1);
                        memcpy(mesh->textures + j, v0, 2 * sizeof(ats_float));
                        j++;
                        memcpy(mesh->textures + j, v1, 2 * sizeof(ats_float));
                        j++;
                        memcpy(mesh->textures + j, v2, 2 * sizeof(ats_float));
                        j++;
                }
        }

        return mesh;
}
