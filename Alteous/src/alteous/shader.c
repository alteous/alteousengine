/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file shader.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 29/11/15
 * @brief Implementation of an abstract GLSL program within OpenGL.
 */

#include "alteous/shader.h"

#include <stdlib.h>
#include <string.h>

ATSAPI struct ats_shader *ATSCALL ats_shader_init(const ats_char *vertex_shader_source,
                                           const ats_char *fragment_shader_source,
                                           struct ats_attrib *attributes,
                                           ats_size attribute_count,
                                           ats_shader_err_fn error_fn)
{
        struct ats_shader *program = malloc(sizeof(struct ats_shader));

        program->id = glCreateProgram();
        program->components = 0;
        program->component_count = 0;

        ats_shader_comp_uid vertex_component_id = ats_shader_compile_sources(
                vertex_shader_source, GL_VERTEX_SHADER, error_fn);
        ats_shader_comp_uid fragment_component_id = ats_shader_compile_sources(
                fragment_shader_source, GL_FRAGMENT_SHADER, error_fn);

        if (vertex_component_id != 0 && fragment_component_id != 0) {
                program->components = malloc(2 * sizeof(ats_shader_comp_uid));
                program->components[0] = vertex_component_id;
                program->components[1] = fragment_component_id;
                program->component_count = 2;

                glAttachShader(program->id, vertex_component_id);
                glAttachShader(program->id, fragment_component_id);
                ats_shader_bind_attribs(program->id, attributes, attribute_count);
                glLinkProgram(program->id);

                ats_int link_status;
                glGetProgramiv(program->id, GL_LINK_STATUS, &link_status);
                if (!link_status) {
                        ats_int error_log_length;
                        glGetProgramiv(program->id, GL_INFO_LOG_LENGTH, &error_log_length);
                        ats_char error_log[error_log_length];
                        glGetProgramInfoLog(program->id, error_log_length, &error_log_length,
                                            error_log);
                        error_fn(error_log);
                        ats_shader_free(program);
                        program = NULL;
                }
        } else {
                ats_shader_free(program);
                program = NULL;
        }

        return program;
}

ATSAPI ats_shader_comp_uid ATSCALL ats_shader_compile_sources(const ats_char *source, GLenum type,
                                                              ats_shader_err_fn error_fn)
{
        ats_shader_comp_uid component_id = glCreateShader(type);

        ats_int source_length = (ats_int) strlen(source);
        glShaderSource(component_id, 1, &source, &source_length);
        glCompileShader(component_id);

        ats_int compile_status;
        glGetShaderiv(component_id, GL_COMPILE_STATUS, &compile_status);
        if (!compile_status) {
                ats_int error_log_length;
                glGetShaderiv(component_id, GL_INFO_LOG_LENGTH, &error_log_length);
                ats_char error_log[error_log_length];
                glGetShaderInfoLog(component_id, error_log_length, &error_log_length, error_log);
                error_fn(error_log);
                glDeleteShader(component_id);
                component_id = 0;
        }

        return component_id;
}

ATSAPI void ATSCALL ats_shader_bind_attribs(ats_shader_uid id,
                                            struct ats_attrib *attribs,
                                            ats_size attrib_count)
{
        for (ats_size i = 0; i < attrib_count; i++) {
                glBindAttribLocation(id, attribs[i].location, attribs[i].name);
        }
}

ATSAPI void ATSCALL ats_shader_clear(struct ats_shader *shader)
{
        for (ats_size i = 0; i < shader->component_count; i++) {
                glDetachShader(shader->id, shader->components[i]);
                glDeleteShader(shader->components[i]);
        }
        glDeleteProgram(shader->id);

        free(shader->components);
        shader->component_count = 0;
        shader->id = 0;
}

ATSAPI void ATSCALL ats_shader_free(struct ats_shader *shader)
{
        ats_shader_clear(shader);
        free(shader);
}

