/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file map.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 15/12/15
 * @brief Implementation of a generic map data structure.
 */

#include "alteous/map.h"

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <alteous/assertion.h>

#include "alteous/memory.h"

static const ats_size initial_capacity = 16;
static const struct ats_map_pair null_pair = { NULL, NULL };

static ats_bool map_key_eq(struct ats_map_pair pair, void *key_ptr, ats_compare_fn compare)
{
        if (NULL == pair.key_ptr) {
                return ATS_FALSE;
        } else {
                return 0 == compare(pair.key_ptr, key_ptr);
        }
}

static ats_size map_bucket_index(struct ats_map *map, void *key_ptr)
{
        ats_int hash = map->hash_fn(key_ptr);
        return hash % map->capacity;
}

ATSAPI struct ats_map * ATSCALL ats_map_init(ats_size key_size, ats_size value_size,
                                             ats_hash_fn hash_fn, ats_compare_fn comparator_fn)
{
        struct ats_map *map = ATS_ALLOC(struct ats_map);
        if (0 == map) {
                return 0;
        }

        map->buckets = ATS_MALLOC(initial_capacity, struct ats_map_entry);
        if (0 == map->buckets) {
                free(map);
                return 0;
        }
        map->capacity = initial_capacity;
        map->size = 0;
        map->key_size = key_size;
        map->value_size = value_size;
        map->hash_fn = hash_fn;
        map->key_compare_fn = comparator_fn;

        /* Zero initialise the attributes (i.e. key, value, next) of all map entries. */
        memset(map->buckets, 0, map->capacity * sizeof(struct ats_map_entry));

        return map;
}

static void map_free(struct ats_map_entry *entry)
{
        if (NULL != entry) {
                if (entry->next) {
                        map_free(entry);
                }
                free(entry);
        }
}

ATSAPI void ATSCALL ats_map_free(struct ats_map *map)
{
        ATS_DEBUG_ASSERT(0 != map);

        /* Free any chained entries. */
        for (ats_size i = 0; i < map->capacity; i++) {
                map_free(map->buckets[i].next);
        }

        /* Free the root entries. */
        free(map->buckets);
        free(map);
}

static ats_bool entry_contains_key(struct ats_map *map, struct ats_map_entry *entry, void *key)
{
        if (NULL == entry) {
                return ATS_FALSE;
        } else if (map_key_eq(entry->pair, key, map->key_compare_fn)) {
                return ATS_TRUE;
        } else {
                return entry_contains_key(map, entry->next, key);
        }
}

ATSAPI ats_bool ATSCALL ats_map_contains_key(struct ats_map *map, void *key_ptr)
{
        ATS_DEBUG_ASSERT(0 != map);

        ats_size bucket_index = map_bucket_index(map, key_ptr);

        return entry_contains_key(map, map->buckets + bucket_index, key_ptr);
}

static struct ats_map_pair get(struct ats_map *map, struct ats_map_entry *entry, void *key_ptr)
{
        if (NULL != entry) {
                if (map_key_eq(entry->pair, key_ptr, map->key_compare_fn)) {
                        return entry->pair;
                } else {
                        return get(map, entry->next, key_ptr);
                }
        } else {
                return null_pair;
        }
}

ATSAPI struct ats_map_pair ATSCALL ats_map_get(struct ats_map *map, void *key_ptr)
{
        ATS_DEBUG_ASSERT(0 != key_ptr);

        ats_size bucket_index = map_bucket_index(map, key_ptr);

        return get(map, map->buckets + bucket_index, key_ptr);
}

static ats_error entry_append(struct ats_map *map, struct ats_map_entry *entry,
                              void *key_ptr, void *value_ptr)
{
        /* If this is the end of linked list then make a new tail key-value pair. */
        if (NULL == entry->next) {
                entry->next = ATS_ALLOC(struct ats_map_entry);
                if (NULL == entry->next) {
                        return AtsAllocFailed;
                }
                entry->next->pair.key_ptr = key_ptr;
                entry->next->pair.value_ptr = value_ptr;
                entry->next->next = NULL;
                map->size++;
                return AtsNoError;
        } else if (map_key_eq(entry->pair, key_ptr, map->key_compare_fn)) {
                return AtsKeyAlreadyExists;
        } else {
                return entry_append(map, entry->next, key_ptr, value_ptr);
        }
}

ATSAPI ats_error ATSCALL ats_map_put(struct ats_map *map, struct ats_map_pair pair)
{
        ATS_DEBUG_ASSERT(0 != map);

        if (NULL == pair.key_ptr) {
                return AtsKeyIsNull;
        }
        ats_size bucket_index = map_bucket_index(map, pair.key_ptr);
        struct ats_map_entry *entry = map->buckets + bucket_index;
        if (NULL == entry->pair.key_ptr) {
                entry->pair = pair;
                entry->next = NULL;
                map->size++;
                return AtsNoError;
        } else if (map_key_eq(entry->pair, pair.key_ptr, map->key_compare_fn)) {
                return AtsKeyAlreadyExists;
        }

        return entry_append(map, map->buckets + bucket_index, pair.key_ptr, pair.value_ptr);
}

static struct ats_map_pair remove_entry(struct ats_map *map, struct ats_map_entry *entry,
                                        void *key_ptr)
{
        if (NULL != entry) {
                if (map_key_eq(entry->pair, key_ptr, map->key_compare_fn)) {
                        struct ats_map_pair removed_pair = entry->pair;
                        entry->pair = null_pair;
                        map->size--;
                        return removed_pair;
                } else {
                        return remove_entry(map, entry->next, key_ptr);
                }
        } else {
                return null_pair;
        }
}

ATSAPI struct ats_map_pair ATSCALL ats_map_rm(struct ats_map *map, void *key_ptr)
{
#ifdef DEBUG
        assert(0 != map);
        assert(0 != key_ptr);
#endif
        ats_size bucket_index = map_bucket_index(map, key_ptr);

        return remove_entry(map, map->buckets + bucket_index, key_ptr);
}

static struct ats_map_pair entry_replace(struct ats_map *map, struct ats_map_entry *entry,
                                         struct ats_map_pair new_pair)
{
        if (NULL != entry) {
                if (map_key_eq(entry->pair, new_pair.key_ptr, map->key_compare_fn)) {
                        /* Return a copy of the replaced pair before updating the pointer. */
                        struct ats_map_pair replaced_pair = entry->pair;
                        entry->pair = new_pair;
                        return replaced_pair;
                } else {
                        return entry_replace(map, entry->next, new_pair);
                }
        } else {
                return null_pair;
        }
}

ATSAPI struct ats_map_pair ATSCALL ats_map_replace(struct ats_map *map, struct ats_map_pair pair)
{
#ifdef DEBUG
        assert(0 != map);
        assert(0 != pair.key_ptr);
#endif
        ats_size bucket_index = map_bucket_index(map, pair.key_ptr);

        return entry_replace(map, map->buckets + bucket_index, pair);
}
