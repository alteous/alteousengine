/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file heightmap.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 31/12/15
 * @brief Implementation of the height map loader.
 */

#include "alteous/heightmap.h"

#include <stdlib.h>

#include "stb/stb_image.h"
#include "alteous/assertion.h"

struct ats_hgtmap {
        ats_float *heights;
        ats_int width;
        ats_int height;
};

static struct ats_hgtmap* _ats_hgtmap_init(ats_int w, ats_int h, ats_ubyte *data)
{
        ATS_DEBUG_ASSERT(w > 0);
        ATS_DEBUG_ASSERT(h > 0);

        struct ats_hgtmap *map = malloc(sizeof(struct ats_hgtmap));
        if (NULL == map) {
                return NULL;
        }

        ats_size total_points = (ats_size) (w * h);
        map->heights =  malloc(total_points * sizeof(ats_float));
        if (NULL == map->heights) {
                return NULL;
        }

        map->width = w;
        map->height = h;
        for (ats_size i = 0; i < total_points; i++) {
                map->heights[i] = data[i] / ((ats_float) ATS_UBYTE_MAX);
        }

        return map;
}

ATSAPI struct ats_hgtmap* ATSCALL ats_hgtmap_init(const ats_char *path, enum ats_img_fmt fmt)
{
        ATS_DEBUG_ASSERT(AtsPng == fmt);

        ats_int width, height, components;
        ats_ubyte *image = stbi_load(path, &width, &height, &components, STBI_grey);
        if (NULL == image) {
                return NULL;
        }

        struct ats_hgtmap *map = _ats_hgtmap_init(width, height, image);
        if (NULL == map) {
                return NULL;
        }

        stbi_image_free(image);

        return map;
}

ATSAPI void ATSCALL ats_hgtmap_free(struct ats_hgtmap *map)
{
        free(map->heights);
        free(map);
}
