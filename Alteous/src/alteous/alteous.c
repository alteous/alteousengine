/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file alteous.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 13/12/15
 * @brief Implementation of the engine's initialisation and termination functions.
 */

#include "alteous/alteous.h"

#include <stdlib.h>
#include <stdio.h>
#include <SDL2/SDL.h>

#include "alteous/detail/context.h"
#include "alteous/detail/keyboard.h"
#include "alteous/detail/cursor.h"
#include "alteous/detail/fp_camera.h"

#define IS_FLAGGED(bits, flag) (((bits) & (flag)) == (flag))

static void init_glew()
{
#ifndef EMSCRIPTEN
        glewExperimental = ATS_TRUE;
        GLenum glew_error = glewInit();
        if (glew_error != GLEW_OK) {
                fprintf(stderr, "Fatal GLEW error: %s\n", glewGetErrorString(glew_error));
                exit(EXIT_FAILURE);
        }
        if (!GLEW_VERSION_3_2) {
                fprintf(stderr, "Fatal GLEW error: OpenGL version 3.2 not available.\n");
                exit(EXIT_FAILURE);
        }
#endif
}

static void _alteous_set_cursor_pos(Sint32 x, Sint32 y)
{
        struct ats_vec2i new_position = { x, y };
        _ats_cursor_set_pos(&new_position);
}

ATSAPI void ATSCALL ats_init(uint32_t flags)
{
        _ats_context_init();
        init_glew();

        /* Keyboard input handler. */
        if (IS_FLAGGED(flags, ATS_INIT_KEYBOARD)) {
                _ats_keyboard_init();
        }

        /* Cursor input handler. */
        if (IS_FLAGGED(flags, ATS_INIT_CURSOR)) {
                _ats_cursor_init();
        }

        /* Timer function handler. */
        if (IS_FLAGGED(flags, ATS_INIT_TIMER)) {
                /* TODO: Initialise timer here. */
        }

        /* First person camera mode. */
        if (IS_FLAGGED(flags, ATS_INIT_FP_CAMERA)) {
                _ats_fp_cam_init();
        }
}

ATSAPI void ATSCALL ats_poll_events()
{
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
                switch (event.type) {
                        case SDL_QUIT:
                                _ats_context_close();
                                ats_exit();
                        case SDL_KEYDOWN:
                                if (SDLK_ESCAPE == event.key.keysym.sym) {
                                        _ats_context_close();
                                        ats_exit();
                                }
                                _ats_keyboard_set_key_down(event.key.keysym.sym);
                                break;
                        case SDL_KEYUP:
                                _ats_keyboard_set_key_up(event.key.keysym.sym);
                                break;
                        case SDL_WINDOWEVENT:
                                if (SDL_WINDOWEVENT_RESIZED == event.window.event) {
                                        ats_int new_width = event.window.data1;
                                        ats_int new_height = event.window.data2;
                                        _ats_context_set_size(new_width, new_height);
                                }
                                break;
                        case SDL_MOUSEMOTION:
                                _alteous_set_cursor_pos(event.motion.x, event.motion.y);
                                break;
                        default:
                                break;
                }
        }
}

ATSAPI void ATSCALL ats_exit()
{
        exit(ats_terminate());
}

ATSAPI int ATSCALL ats_terminate()
{
        _ats_context_free();
        SDL_Quit();

        return EXIT_SUCCESS;
}

#undef IS_FLAGGED
