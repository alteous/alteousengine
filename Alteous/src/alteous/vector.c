/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file vector.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 13/11/15
 * @brief Implementation of a generic and dynamically sized array.
 */

#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "alteous/memory.h"
#include "alteous/vector.h"

static const ats_size grow_factor = 2;
static ats_error AtsVector_grow(struct ats_vtr *vector)
{
#ifdef DEBUG
        assert(NULL != vector);
        assert(NULL != vector->elements);
#endif
        void **new_ptr = ATS_REALLOC(vector->elements, grow_factor * vector->capacity, void**);
        if (NULL == new_ptr) {
                return AtsAllocFailed;
        }

        vector->elements = new_ptr;
        vector->capacity *= grow_factor;

        return AtsNoError;
}

ATSAPI struct ats_vtr * ATSCALL ats_vtr_init(ats_size initial_capacity)
{
        struct ats_vtr *vector = ATS_ALLOC(struct ats_vtr);
        if (NULL == vector) {
                return NULL;
        }

        vector->elements = ATS_MALLOC(initial_capacity, void*);
        if (NULL == vector->elements) {
                free(vector);
                return NULL;
        }

        vector->length = 0;
        vector->capacity = initial_capacity;

        return vector;
}

ATSAPI void ATSCALL ats_vtr_free(struct ats_vtr *vector)
{
#ifdef DEBUG
        assert(NULL != vector);
#endif
        free(vector->elements);
        free(vector);
}

ATSAPI const void* ATSCALL ats_vtr_get(struct ats_vtr *vector, ats_size index)
{
#ifdef DEBUG
        assert(NULL != vector);
        assert(index >= 0 && index < vector->length);
#endif
        return vector->elements[index];
}

ATSAPI ats_error ATSCALL ats_vtr_push(struct ats_vtr *vector, void *value_ptr)
{
#ifdef DEBUG
        assert(NULL != vector);
        assert(NULL != vector->elements);
#endif
        if (vector->length == vector->capacity) {
                ats_error error = AtsVector_grow(vector);
                if (error) {
                        return error;
                }
        }
        vector->elements[vector->length] = value_ptr;
        vector->length++;

        return AtsNoError;
}

ATSAPI void* ATSCALL ats_vtr_rm(struct ats_vtr *vector, ats_size index)
{
#ifdef DEBUG
        assert(NULL != vector);
        assert(index >= 0 && index < vector->length);
#endif
        void* iterator = vector->elements[index];
        for (ats_size i = index; i < vector->length - 1; i++) {
                vector->elements[i] = vector->elements[1 + i];
        }
        vector->length--;

        return iterator;
}

ATSAPI void ATSCALL ats_vtr_for_each(struct ats_vtr *vector, ats_vtr_itr_fn iterator_fn)
{
#ifdef DEBUG
        assert(NULL != vector);
        assert(NULL != iterator_fn);
#endif
        const void *value_ptr = NULL;
        for (ats_size i = 0; i < vector->length; i++) {
                value_ptr = vector->elements[i];
                iterator_fn(value_ptr);
        }
}
