/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file transform.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 06/12/15
 * @brief Implementation of a transformation stack.
 */

#include <stdio.h>

#include "alteous/memory.h"
#include "alteous/transform.h"

enum ats_transform_type {
        AtsRotateX, AtsRotateY, AtsRotateZ, AtsTranslate, AtsScale
};

union ats_transform_value {
        struct ats_vec3f scale;
        struct ats_vec3f translation;
        struct ats_angle rotation;
};

struct ats_transform_ins {
        enum ats_transform_type type;
        union ats_transform_value value;
};

static const ats_size initial_transform_capacity = 10;

static void _ats_transform_translate(struct ats_mat4 *target, const struct ats_vec3f *vector)
{
        struct ats_mat4 transformation_matrix, product;
        ats_mat4_make_translation(&transformation_matrix, vector);
        ats_mat4_mat_prod(&product, target, &transformation_matrix);
        ats_mat4_copy(target, &product);
}

static void _ats_transform_rotX(struct ats_mat4 *target, const struct ats_angle *angle)
{
        struct ats_mat4 rotation_matrix, product;
        ats_mat4_make_rotX(&rotation_matrix, angle);
        ats_mat4_mat_prod(&product, target, &rotation_matrix);
        ats_mat4_copy(target, &product);
}

static void _ats_transform_rotY(struct ats_mat4 *target, const struct ats_angle *angle)
{
        struct ats_mat4 rotation_matrix, product;
        ats_mat4_make_rotY(&rotation_matrix, angle);
        ats_mat4_mat_prod(&product, target, &rotation_matrix);
        ats_mat4_copy(target, &product);
}

static void _ats_transform_rotZ(struct ats_mat4 *target, const struct ats_angle *angle)
{
        struct ats_mat4 rotation_matrix, product;
        ats_mat4_make_rotZ(&rotation_matrix, angle);
        ats_mat4_mat_prod(&product, target, &rotation_matrix);
        ats_mat4_copy(target, &product);
}

static void _ats_transform_scale(struct ats_mat4 *target, const struct ats_vec3f *vector)
{
        struct ats_mat4 scale_matrix, product;
        ats_mat4_make_scale(&scale_matrix, vector);
        ats_mat4_mat_prod(&product, target, &scale_matrix);
        ats_mat4_copy(target, &product);
}

ATSAPI void ATSCALL ats_transform_compose(struct ats_transform *transform_stack,
                                          struct ats_mat4 *output)
{
        ats_mat4_identity(output);

        struct ats_transform_ins *transform;
        while (transform_stack->stack->height > 0) {
                transform = (struct ats_transform_ins *) ats_stk_pop(transform_stack->stack);
                switch (transform->type) {
                        case AtsRotateX:
                                _ats_transform_rotX(output, &(transform->value.rotation));
                                break;
                        case AtsRotateY:
                                _ats_transform_rotY(output, &(transform->value.rotation));
                                break;
                        case AtsRotateZ:
                                _ats_transform_rotZ(output, &(transform->value.rotation));
                                break;
                        case AtsScale:
                                _ats_transform_scale(output, &(transform->value.scale));
                                break;
                        case AtsTranslate:
                                _ats_transform_translate(output, &(transform->value.translation));
                                break;
                        default:
                                return;
                }
                free(transform);
        }
}

ATSAPI void ATSCALL ats_transform_free(struct ats_transform *transform_stack)
{
        struct ats_transform_ins *transform;
        while (transform_stack->stack->height > 0) {
                transform = ats_stk_pop(transform_stack->stack);
                free(transform);
        }
        ats_stk_free(transform_stack->stack);
        free(transform_stack);
}

ATSAPI struct ats_transform * ATSCALL ats_transform_init()
{
        struct ats_transform *transform_stack = ATS_ALLOC(struct ats_transform);
        if (transform_stack) {
                transform_stack->stack = ats_stk_init(initial_transform_capacity);
                if (!transform_stack->stack) {
                        free(transform_stack);
                        transform_stack = NULL;
                }
        }

        return transform_stack;
}

ATSAPI ats_error ATSCALL ats_transform_rotX(struct ats_transform *parent,
                                            const struct ats_angle *angle)
{
        struct ats_transform_ins *transform = ATS_ALLOC(struct ats_transform_ins);
        if (NULL == transform) {
                return AtsAllocFailed;
        }

        transform->type = AtsRotateX;
        ats_angle_copy(angle, &transform->value.rotation);

        return ats_stk_push(parent->stack, transform);
}

ATSAPI ats_error ATSCALL ats_transform_unrotX(struct ats_transform *parent,
                                              const struct ats_angle *angle)
{
        struct ats_angle reverse = { -1.0f * angle->value, angle->type };

        return ats_transform_rotX(parent, &reverse);
}

ATSAPI ats_error ATSCALL ats_transform_rotY(struct ats_transform *parent,
                                            const struct ats_angle *angle)
{
        struct ats_transform_ins *transform = ATS_ALLOC(struct ats_transform_ins);
        if (NULL == transform) {
                return AtsAllocFailed;
        }

        transform->type = AtsRotateY;
        ats_angle_copy(angle, &transform->value.rotation);

        return ats_stk_push(parent->stack, transform);
}

ATSAPI ats_error ATSCALL ats_transform_unrotY(struct ats_transform *parent,
                                              const struct ats_angle *angle)
{
        struct ats_angle reverse = { -1.0f * angle->value, angle->type };

        return ats_transform_rotY(parent, &reverse);
}

ATSAPI ats_error ATSCALL ats_transform_rotZ(struct ats_transform *parent,
                                            const struct ats_angle *angle)
{
        struct ats_transform_ins *transform = ATS_ALLOC(struct ats_transform_ins);
        if (NULL == transform) {
                return AtsAllocFailed;
        }

        transform->type = AtsRotateZ;
        ats_angle_copy(angle, &transform->value.rotation);

        return ats_stk_push(parent->stack, transform);
}

ATSAPI ats_error ATSCALL ats_transform_unrotZ(struct ats_transform *parent,
                                              const struct ats_angle *angle)
{
        struct ats_angle reverse = { -1.0f * angle->value, angle->type };

        return ats_transform_rotZ(parent, &reverse);
}

ATSAPI ats_error ATSCALL ats_transform_scale(struct ats_transform *parent,
                                             const struct ats_vec3f *vector)
{
        struct ats_transform_ins *transform = ATS_ALLOC(struct ats_transform_ins);
        if (NULL == transform) {
                return AtsAllocFailed;
        }

        transform->type = AtsScale;
        transform->value.scale = *vector;

        return ats_stk_push(parent->stack, transform);
}

ATSAPI ats_error ATSCALL ats_transform_translate(struct ats_transform *parent,
                                                 const struct ats_vec3f *vector)
{
        struct ats_transform_ins *transform = malloc(sizeof(struct ats_transform_ins));
        if (NULL == transform) {
                return AtsAllocFailed;
        }

        transform->type = AtsTranslate;
        transform->value.translation = *vector;

        return ats_stk_push(parent->stack, transform);
}

ATSAPI ats_error ATSCALL ats_transform_untranslate(struct ats_transform *parent,
                                                   const struct ats_vec3f *vector)
{
        struct ats_vec3f reverse = { -1.0f * vector->x, -1.0f * vector->y, -1.0f * vector->z };

        return ats_transform_translate(parent, &reverse);
}
