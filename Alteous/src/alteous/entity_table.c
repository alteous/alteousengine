/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file entity_table.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 29/12/15
 * @brief Implementation of the engine's entity soa data structure.
 */

#include "alteous/entity_table.h"

#include <stdlib.h>
#include <assert.h>

#include "alteous/bitfield.h"

/**
 * @brief Defines the columns of a soa row representing entity component data.
 */
struct ats_ent_tbl_row {
        ats_comp_bitfield flags;            /* Component configuration for each row.           */
        ats_size col_count;                 /* Number of concrete entities mapped to this row. */
        ats_size col_limit;                 /* Column capacity before resize is necessary.     */
        void *comp[_AtsCompCount];          /* The component array.                            */
};

/**
 * @brief An SOA soa composed of rows of entity component data.
 */
struct ats_ent_tbl {
        struct ats_ent_tbl_row *rows;       /* Row entries, one for each comp configuration.   */
        ats_size row_count;                 /* The number of rows in the soa.                */
        ats_size row_limit;                 /* Row capacity before resize is necessary.        */
};

struct ats_ent_tbl_itr {
        struct ats_ent_tbl *table;
        ats_size row;
        ats_size col;
        struct ats_ent_tbl_row *row_ptr;
        ats_comp_bitfield flags;
};

/** Initial number of rows in a soa. */
static const ats_size _ats_entity_table_init_row_limit = 32;

/** Initial number of cols in a soa rows. */
static const ats_size _ats_entity_table_init_col_limit = 256;

/** Doubles the number of row pointers in a soa. */
static ats_error _ats_entity_table_grow(struct ats_ent_tbl *table)
{
        void *new_rows = realloc(table->rows,
                                 2 * table->row_limit * sizeof(struct ats_ent_tbl_row));
        if (NULL == new_rows) {
                return AtsAllocFailed;
        }

        table->rows = new_rows;
        table->row_limit *= 2;

        return AtsNoError;
}

/** Returns a pointer to the row with the given flags, if it exists, and NULL otherwise. */
static struct ats_ent_tbl_row * _ats_entity_table_find(struct ats_ent_tbl *table,
                                                       ats_comp_bitfield flags,
                                                       ats_size *row_index)
{
        for (ats_size i = 0; i < table->row_count; i++) {
                if (flags == table->rows[i].flags) {
                        if (NULL != row_index) {
                                *row_index = i;
                        }
                        return table->rows + i;
                }
        }

        return NULL;
}

/** Appends a new row to a soa. Returns a pointer to the row if successful and NULL otherwise. */
static struct ats_ent_tbl_row * _ats_entity_table_push_row(struct ats_ent_tbl *table,
                                                           ats_comp_bitfield flags,
                                                           ats_size *row_index)
{
        /* Is resize required. */
        if (table->row_count == table->row_limit) {
                ats_error error = _ats_entity_table_grow(table);
                if (error) {
                        return NULL;
                }
        }

        struct ats_ent_tbl_row *row = table->rows + table->row_count;
        if (NULL != row_index) {
                *row_index = table->row_count;
        }
        row->flags = flags;
        row->col_count = 0;
        row->col_limit = _ats_entity_table_init_col_limit;

        enum ats_comp_type type;
        void *comp_ptr;
        for(uint8_t i = 0; i < _AtsCompCount; i++) {
                type = ats_comp_types[i];

                /* If this component if required. */
                if (ATS_BITFIELD_HAS_BIT(flags, type)) {

                        comp_ptr = ats_comp_init(type, row->col_limit);

                        /* If allocation failed, free all the previous rows and exit. */
                        if (NULL == comp_ptr) {
                                for (uint8_t j = 0; j < i; j++) {
                                        ats_comp_free(row->comp[j]);
                                }
                                return NULL;
                        }

                        row->comp[i] = comp_ptr;

                } else {
                        row->comp[i] = NULL;
                }
        }

        table->row_count++;

        return row;
}

ATSAPI struct ats_ent_tbl * ATSCALL ats_ent_tbl_init()
{
        struct ats_ent_tbl *table_ptr = malloc(sizeof(struct ats_ent_tbl));
        if (NULL == table_ptr) {
                return NULL;
        }

        table_ptr->row_count = 0;
        table_ptr->row_limit = _ats_entity_table_init_row_limit;
        table_ptr->rows = calloc(table_ptr->row_limit, sizeof(struct ats_ent_tbl_row));
        if (NULL == table_ptr->rows) {
                free(table_ptr);
                return NULL;
        }

        return table_ptr;
}

ATSAPI ats_error ATSCALL ats_ent_tbl_insert(struct ats_ent_tbl *table, ats_comp_bitfield flags,
                                            ats_size *row_index, ats_size *col_index)
{
#ifdef DEBUG
        assert(0 != flags);
#endif
        /* Try to locate a row which already has the component configuration we require. */
        struct ats_ent_tbl_row *row_ptr;
        row_ptr = _ats_entity_table_find(table, flags, row_index);

        /* If the given config does not exist then will need to add the entity to a new row. */
        if (NULL == row_ptr) {
                row_ptr = _ats_entity_table_push_row(table, flags, row_index);
                if (NULL == row_ptr) {
                        return AtsAllocFailed;
                }
        }

        if (NULL != col_index) {
                *col_index = row_ptr->col_count;
        }
        row_ptr->col_count++;

        return AtsNoError;
}

ATSAPI void ATSCALL ats_ent_tbl_free(struct ats_ent_tbl *table)
{
        /* N.B: We need only free the rows that have entity data stored in them. */
        struct ats_ent_tbl_row *row_ptr;
        for (ats_size i = 0; i < table->row_count /* ! */; i++) {
                row_ptr = table->rows + i;
                for (uint8_t j = 0; j < _AtsCompCount; j++) {
                        ats_comp_free(row_ptr->comp[j]);
                }
        }

        free(table->rows);
        free(table);
}

static void* _ats_ent_tbl_get_comp(struct ats_ent_tbl_row *row_ptr, ats_size col,
                                   enum ats_comp_type type)
{
        void *comp_ptr = row_ptr->comp[type];

        return comp_ptr == NULL ? NULL : comp_ptr + col * ats_comp_sizeof(type);
}

ATSAPI void* ATSCALL ats_ent_tbl_get_comp(struct ats_ent_tbl *table, enum ats_comp_type id,
                                          ats_size row, ats_size col)
{
#ifdef DEBUG
        assert(NULL != table);
        assert(id >= 0);
        assert(id < _AtsCompCount);
#endif
        struct ats_ent_tbl_row *row_ptr = table->rows + row;
#ifdef DEBUG
        assert(col < row_ptr->col_count);
#endif
        return _ats_ent_tbl_get_comp(row_ptr, col, id);
}

ATSAPI struct ats_ent_tbl_itr* ATSCALL ats_ent_tbl_itr_init(struct ats_ent_tbl *table,
                                                            ats_comp_bitfield flags)
{
        for (ats_size i = 0; i < table->row_count; i++) {
                if (table->rows[i].flags == flags) {
                        struct ats_ent_tbl_itr *it = malloc(sizeof(struct ats_ent_tbl_itr));
                        if (NULL == it) {
                                return NULL;
                        }
                        it->table = table;
                        it->row_ptr = table->rows + i;
                        it->flags = flags;
                        it->row = i;
                        it->col = 0;
                        return it;
                }
        }

        return NULL;
}

ATSAPI void ATSCALL ats_ent_tbl_itr_free(struct ats_ent_tbl_itr *it)
{
        free(it);
}

ATSAPI ats_bool ATSCALL ats_ent_tbl_itr_next(struct ats_ent_tbl_itr *it)
{
#ifdef DEBUG
        assert(NULL != it);
#endif
        struct ats_ent_tbl_row *row = it->row_ptr;
        if (row->col_count != it->col) {
                /* Move to the next column in this row. */
                it->col++;
                return ATS_TRUE;
        }

        /* Find the next row that contains the desired component flags. */
        for (ats_size i = 1 + it->row; i < it->table->row_count; i++) {
                if (ATS_BITFIELD_HAS_FLAG(it->table->rows[i].flags, it->flags)) {
                        it->row_ptr = it->table->rows + i;
                        it->row = i;
                        it->col = 0;
                        return ATS_TRUE;
                }
        }

        /* No such row exists. */
        return ATS_FALSE;
}

ATSAPI void* ATSCALL ats_ent_tbl_itr_get_comp(struct ats_ent_tbl_itr *it, enum ats_comp_type type)
{
#ifdef DEBUG
        assert(NULL != it);
#endif
        return _ats_ent_tbl_get_comp(it->row_ptr, it->col, type);
}
