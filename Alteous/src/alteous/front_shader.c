/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file front_shader.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 29/11/15
 * @brief Implementation of the default Alteous renderer.
 */

#include <stdlib.h>

#include "alteous/file.h"
#include "alteous/front_shader.h"
#include "alteous/shader.h"

static const char *vertex_shader_file_path = "res/shaders/front/vertex.glsl";
static const char *fragment_shader_file_path = "res/shaders/front/fragment.glsl";

static void ats_fshader_err_fn(const ats_char *error_log)
{
        fprintf(stderr, "Fatal engine error loading front shader:\n");
        fprintf(stderr, "%s\n", error_log);
}

ATSAPI ats_fshader* ATSCALL ats_fshader_init(file_err_fn file_error_fn)
{
        ats_fshader *shader;
        char *vertex_shader_source = ats_file_read_utf8(vertex_shader_file_path, file_error_fn);
        char *fragment_shader_source = ats_file_read_utf8(fragment_shader_file_path, file_error_fn);

        struct ats_attrib attributes[] = {
                { "v_Position",     AtsPositionAttrib },
                { "v_Colour",       AtsColourAttrib },
                { "v_TextureCoord", AtsTextureAttrib },
                { "v_Normal",       AtsNormalAttrib }
        };

        shader = ats_shader_init(vertex_shader_source,
                                 fragment_shader_source,
                                 attributes,
                                 sizeof(attributes) / sizeof(*attributes),
                                 ats_fshader_err_fn);

        free(vertex_shader_source);
        free(fragment_shader_source);

        return shader;
}


ATSAPI void ATSCALL ats_fshader_free(ats_fshader *shader)
{
        ats_shader_free(shader);
}

ATSAPI void ATSCALL ats_fshader_load_camera_position(ats_fshader *shader, ats_float x, ats_float y,
                                                     ats_float z)
{
        GLint location = glGetUniformLocation(shader->id, "v_CameraPosition");
        glUniform3f(location, x, y, z);
}

ATSAPI void ATSCALL ats_fshader_load_projection_matrix(ats_fshader *shader, ats_float *matrix)
{
        GLint location = glGetUniformLocation(shader->id, "v_ProjectionMatrix");
        glUniformMatrix4fv(location, 1, ATS_FALSE, matrix);
}

ATSAPI void ATSCALL ats_fshader_load_modelview_matrix(ats_fshader *shader, ats_float *matrix)
{
        GLint location = glGetUniformLocation(shader->id, "v_ModelViewMatrix");
        glUniformMatrix4fv(location, 1, ATS_FALSE, matrix);
}

ATSAPI void ATSCALL ats_fshader_load_normal_matrix(ats_fshader *shader, ats_float *matrix)
{
        GLint location = glGetUniformLocation(shader->id, "v_NormalMatrix");
        glUniformMatrix3fv(location, 1, ATS_FALSE, matrix);
}

ATSAPI void ATSCALL ats_fshader_load_global_light(ats_fshader *shader,
                                                  const struct ats_olight *light)
{
        GLint intensity_loc = glGetUniformLocation(shader->id, "v_GlobalLight.intensity");
        GLint position_loc = glGetUniformLocation(shader->id, "v_GlobalLight.coordinate");
        GLint color_loc = glGetUniformLocation(shader->id, "v_GlobalLight.colour");

        glUniform1f(intensity_loc, light->intensity);
        glUniform3f(position_loc, light->position.x, light->position.y, light->position.z);
        glUniform3f(color_loc, light->colour.r, light->colour.g, light->colour.b);
}

ATSAPI void ATSCALL ats_fshader_load_mtl(ats_fshader *shader, const struct ats_mtl *material)
{
        GLint ambient_loc = glGetUniformLocation(shader->id, "v_Material.Ka");
        GLint diffuse_loc = glGetUniformLocation(shader->id, "v_Material.Kd");
        GLint specular_loc = glGetUniformLocation(shader->id, "v_Material.Ks");
        GLint shininess_loc = glGetUniformLocation(shader->id, "v_Material.shininess");

        glUniform1f(shininess_loc, material->shininess);
        glUniform3f(ambient_loc, material->ambient.r, material->ambient.g, material->ambient.b);
        glUniform3f(diffuse_loc, material->diffuse.r, material->diffuse.g, material->diffuse.b);
        glUniform3f(specular_loc, material->specular.r, material->specular.g, material->specular.b);
}

ATSAPI void ATSCALL ats_fshader_load_gamma(ats_fshader *shader, ats_float gamma)
{
        GLint gamma_loc = glGetUniformLocation(shader->id, "v_Gamma");
        glUniform1f(gamma_loc, gamma);
}
