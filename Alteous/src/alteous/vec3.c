/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file vec3.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 22/11/15
 * @brief Implementation of functions for 3 dimensional vector types.
 */

#include <assert.h>
#include <string.h>
#include <tgmath.h>
#include <stdio.h>

#include "alteous/vec3.h"

static const ats_float equals_tolerance = 1e-6;

ATSAPI void ATSCALL ats_vec3f_add(struct ats_vec3f *target, const struct ats_vec3f *arg)
{
#ifdef DEBUG
        assert(NULL != target);
        assert(NULL != arg);
#endif
        target->x += arg->x;
        target->y += arg->y;
        target->z += arg->z;
}

ATSAPI void ATSCALL ats_vec3f_copy(struct ats_vec3f *dest, const struct ats_vec3f *src)
{
#ifdef DEBUG
        assert(NULL != dest);
        assert(NULL != src);
#endif
        memcpy(dest, src, sizeof(struct ats_vec3f));
}

ATSAPI void ATSCALL ats_vec3f_direction(struct ats_vec3f *dir, const struct ats_vec3f *start,
                                        const struct ats_vec3f *end)
{
#ifdef DEBUG
        assert(NULL != dir);
        assert(NULL != start);
        assert(NULL != end);
#endif
        dir->x = end->x - start->x;
        dir->y = end->y - start->y;
        dir->z = end->z - start->z;
}

ATSAPI void ATSCALL ats_vec3f_cross(struct ats_vec3f *product, const struct ats_vec3f *left,
                                    const struct ats_vec3f *right)
{
#ifdef DEBUG
        assert(NULL != product);
        assert(NULL != left);
        assert(NULL != right);
#endif
        product->x = right->z * left->y - right->y * left->z;
        product->y = right->x * left->z - right->z * left->x;
        product->z = right->y * left->x - right->x * left->y;
}

ATSAPI ats_bool ATSCALL ats_vec3f_equals(const struct ats_vec3f *left,
                                         const struct ats_vec3f *right)
{
#ifdef DEBUG
        assert(NULL != left);
        assert(NULL != right);
#endif
        if (fabsf(left->x - right->x) > equals_tolerance) {
                return ATS_FALSE;
        }

        if (fabsf(left->y - right->y) > equals_tolerance) {
                return ATS_FALSE;
        }

        if (fabsf(left->z - right->z) > equals_tolerance) {
                return ATS_FALSE;
        }

        return ATS_TRUE;
}

ATSAPI ats_float ATSCALL ats_vec3f_length(const struct ats_vec3f *vector)
{
#ifdef DEBUG
        assert(NULL != vector);
#endif
        return sqrtf((vector->x * vector->x) + (vector->y * vector->y) + (vector->z * vector->z));
}

ATSAPI ats_bool ATSCALL ats_vec3f_normalise(struct ats_vec3f *vector)
{
        ats_float length = ats_vec3f_length(vector);
        if (length < 1e-6) {
                return ATS_FALSE;
        }

        ats_vec3f_scale(vector, 1.0f / length);

        return ATS_TRUE;
}

ATSAPI void ATSCALL ats_vec3f_scale(struct ats_vec3f *vector, ats_float scalar)
{
#ifdef DEBUG
        assert(NULL != vector);
#endif
        vector->x *= scalar;
        vector->y *= scalar;
        vector->z *= scalar;
}

ATSAPI ats_bool ATSCALL ats_vec3f_surf_norm(struct ats_vec3f *normal, const struct ats_vec3f *v0,
                                            const struct ats_vec3f *v1, const struct ats_vec3f *v2)
{
#ifdef DEBUG
        assert(NULL != normal);
        assert(NULL != v0);
        assert(NULL != v1);
        assert(NULL != v2);
#endif
        /* The direction vector from v0 to v1. */
        struct ats_vec3f v01;
        ats_vec3f_direction(&v01, v0, v1);

        /* The direction vector from v0 to v2. */
        struct ats_vec3f v02;
        ats_vec3f_direction(&v02, v0, v2);

        ats_vec3f_cross(normal, &v01, &v02);

        return ats_vec3f_normalise(normal);
}
