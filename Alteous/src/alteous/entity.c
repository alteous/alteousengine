/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file entity.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 27/12/15
 * @brief Implementation of the engine's entity component system.
 */

#include "alteous/entity.h"

#include <stdlib.h>
#include <setjmp.h>
#include <assert.h>

#include "alteous/entity_table.h"

/** Maps entity ids to a row / column pair in the soa-> */
struct ats_ent_metatable {
        ats_size *row;
        ats_size *col;
        ats_size id_count;
        ats_size id_limit;
};

/** Contains all entities in the engine. */
static struct ats_ent_tbl *table;

static struct ats_ent_metatable metatable;

/** True if both the entity metatable and the entity data soa are initialised. */
static ats_bool locals_initialised = false;
static ats_size init_metatable_id_limit = 1024;

static ats_error _ats_entity_init_metatable()
{
        metatable.id_count = 0;
        metatable.id_limit = init_metatable_id_limit;

        metatable.row = malloc(metatable.id_limit * sizeof(ats_size));
        if (NULL == metatable.row) {
                return AtsAllocFailed;
        }

        metatable.col = malloc(metatable.id_limit * sizeof(ats_size));
        if (NULL == metatable.col) {
                free(metatable.col);
                return AtsAllocFailed;
        }

        return AtsNoError;
}

static ats_error _ats_entity_grow_metatable()
{
        void *row_ptr = realloc(metatable.row, 2 * metatable.id_limit * sizeof(ats_size));
        if (NULL == row_ptr) {
                return AtsAllocFailed;
        }

        metatable.row = row_ptr;

        void *col_ptr = realloc(metatable.col, 2 * metatable.id_limit * sizeof(ats_size));
        if (NULL == col_ptr) {
                return AtsAllocFailed;
        }
        metatable.col = col_ptr;

        return AtsNoError;
}

static ats_error _ats_entity_init_locals()
{
#ifdef DEBUG
        assert(ATS_FALSE == locals_initialised);
#endif
        ats_error error = _ats_entity_init_metatable();
        if (AtsNoError != error) {
                return error;
        }

        table = ats_ent_tbl_init();
        if (NULL == table) {
                return AtsAllocFailed;
        }

        return AtsNoError;
}

static ats_ent_uid _ats_entity_make_id(ats_comp_bitfield flags)
{
        /* Resize metatable. */
        if (metatable.id_limit == metatable.id_count) {
                if (AtsNoError != _ats_entity_grow_metatable()) {
                        return -1;
                }
        }

        ats_size row, col;
        ats_error error = ats_ent_tbl_insert(table, flags, &row, &col);
        if (AtsNoError != error) {
                return -1;
        }

        ats_ent_uid id = metatable.id_count;
        metatable.row[id] = row;
        metatable.col[id] = col;

        metatable.id_count++;

        return id;
}

ATSAPI ats_ent_uid ATSCALL ats_ent_init(ats_comp_bitfield flags)
{
#ifdef DEBUG
        assert(0 != flags);
#endif
        if (ATS_FALSE == locals_initialised) {
                if (AtsNoError == _ats_entity_init_locals()) {
                        locals_initialised = true;
                }
        }

        return _ats_entity_make_id(flags);
}

ATSAPI void ATSCALL ats_ent_free(ats_ent_uid id)
{
        /*
         * TODO: Add free functionality.
         */
}

ATSAPI void* ATSCALL ats_ent_get_comp(ats_ent_uid id, enum ats_comp_type type)
{
        return ats_ent_tbl_get_comp(table, type, metatable.row[id], metatable.col[id]);
}

ATSAPI ats_ent_itr* ATSCALL ats_ent_itr_init(ats_comp_bitfield flags)
{
        return ats_ent_tbl_itr_init(table, flags);
}

ATSAPI void ATSCALL ats_ent_itr_free(ats_ent_itr *it)
{
        ats_ent_tbl_itr_free(it);
}

ATSAPI ats_bool ATSCALL ats_ent_itr_next(ats_ent_itr *it)
{
        return ats_ent_tbl_itr_next(it);
}

ATSAPI void* ATSCALL ats_ent_itr_get_comp(ats_ent_itr *it, enum ats_comp_type type)
{
        return ats_ent_tbl_itr_get_comp(it, type);
}
