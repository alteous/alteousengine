/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file file.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 19/12/15
 * @brief Implementation of the engine's file utilities.
 */

#include <stdlib.h>
#include <string.h>

#include "alteous/file.h"

ATSAPI ats_char ATSCALL *ats_file_read_utf8(const ats_char *path, file_err_fn error_fn)
{
        ats_char *file_contents = 0;
        ats_file file = fopen(path, "r");

        if (!ferror(file)) {
                ats_long file_length = ats_file_length(file);
                file_contents = malloc((1 + file_length) * sizeof(ats_char));
                fread(file_contents, sizeof(ats_char), (size_t) file_length, file);
                fclose(file);
                file_contents[file_length] = '\0';
        } else {
                const ats_char *error_prefix = "ats_file error: Could not read file \"%s\"";
                size_t error_prefix_length = strlen(error_prefix);
                size_t file_path_length = strlen(path);
                size_t total_length = error_prefix_length + file_path_length;
                ats_char error_log[1 + total_length];
                printf(error_log, total_length, error_prefix, path);
                error_log[total_length] = '\0';
                error_fn(error_log);
        }

        return file_contents;
}

ATSAPI ats_long ATSCALL ats_file_length(ats_file file)
{
        ats_long file_length = 0;

        fseek(file, 0L, SEEK_END);
        file_length = ftell(file);
        fseek(file, 0L, SEEK_SET);

        return file_length;
}
