/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file render_system.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 02/01/16
 * @brief Implementation of the engine renderer.
 */

#include "alteous/render_system.h"

#include <stdlib.h>

#include "alteous/mat4.h"
#include "alteous/transform.h"
#include "alteous/fp_camera.h"
#include "alteous/mat3.h"
#include "alteous/shader.h"
#include "alteous/alteous.h"
#include "alteous/assertion.h"
#include "alteous/context.h"
#include "alteous/cube.h"
#include "alteous/entity.h"
#include "alteous/front_shader.h"

static void file_fatal_error_fn(const ats_char *error_log)
{
        if (error_log) {
                fprintf(stderr, "Fatal file error: %s.\n", error_log);
        }
        ats_exit();
}

static struct ats_mtl material = {
        20.0f,                       /* shininess */
        { 0.4f, 0.4f, 0.4f },        /* ambient   */
        { 0.6f, 0.6f, 0.6f },        /* diffuse   */
        { 0.8f, 0.8f, 0.8f }         /* specular  */
};

static struct ats_olight light = {
        1.0f,                        /* intensity */
        { 4.0f, 4.0f, 4.0f },        /* position  */
        { 1.0f, 1.0f, 1.0f }         /* colour    */
};

static struct ats_mat4 projection_matrix;
static struct ats_mat4 model_matrix;
static struct ats_mat4 view_matrix;
static struct ats_mat3 normal_matrix;
static struct ats_vec3f camera_position;
static struct ats_mat4 modelview_matrix;
static struct ats_transform *transform = NULL;
static ats_fshader *shader = NULL;
static const ats_float near_plane = 0.1f;
static const ats_float far_plane = 10.0f;
static const struct ats_angle fov = { 90.0f, AtsDegrees };

static void draw_scene()
{
        glUseProgram(shader->id);

        /* Render all objects that have a position component and a renderable component. */
        struct ats_comp_pos *pos;
        struct ats_comp_rdr *rdr;
        ats_ent_itr *iterator = ats_ent_itr_init(AtsCompPosBit | AtsCompRdrBit);
        while (ats_ent_itr_next(iterator)) {

                /* Retrieve object components. */
                pos = ats_ent_itr_get_comp(iterator, AtsCompPos); /* Position component. */
                rdr = ats_ent_itr_get_comp(iterator, AtsCompRdr); /* Renderable component. */

                /* Compute model matrix. */
                ats_mat4_identity(&model_matrix);
                transform = ats_transform_init();
                ats_transform_translate(transform, &pos->coordinate);
                ats_transform_compose(transform, &model_matrix);
                ats_transform_free(transform);
                transform = NULL;

                /* Compute modelview matrix. */
                ats_mat4_mat_prod(&modelview_matrix, &view_matrix, &model_matrix);
                ats_mat3_normal(&normal_matrix, &modelview_matrix);

                /* Load per-object shader uniform variables. */
                ats_fshader_load_camera_position(shader, camera_position.x, camera_position.y,
                                                 camera_position.z);
                ats_fshader_load_modelview_matrix(shader, modelview_matrix.m);
                ats_fshader_load_normal_matrix(shader, normal_matrix.m);
                ats_fshader_load_projection_matrix(shader, projection_matrix.m);
                ats_fshader_load_global_light(shader, &light);
                ats_fshader_load_mtl(shader, &material);
                ats_fshader_load_gamma(shader, 2.4);

                /* Render the object. */
                ats_renderable_render(rdr->id);
        }
        ats_ent_itr_free(iterator);

        glUseProgram(0);
}

ATSAPI void ATSCALL ats_render_entities()
{
        if (NULL == shader) {
                shader = ats_fshader_init(file_fatal_error_fn);
        }

        /* Set the viewport region. */
        ats_int width, height;
        ats_context_get_size(&width, &height);
        glViewport(0, 0, width, height);

        /* Clear the colour and depth buffers. */
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        /* Compute projection matrix. */
        ats_float asp = ats_context_compute_asp();
        ats_mat4_make_projection(&projection_matrix, &fov, asp, near_plane, far_plane);

        /* Update camera location and orientation. */
        ats_fp_cam_update();
        ats_fp_cam_get_position(&camera_position);
        ats_fp_cam_get_view_matrix(&view_matrix);

        draw_scene();

        ats_context_swap_buffers();
}
