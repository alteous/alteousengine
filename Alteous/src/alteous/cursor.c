/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file cursor.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 16/12/15
 * @brief Implementation of the engine's mouse cursor interface.
 */

#include "alteous/detail/cursor.h"

ATSAPI void ATSCALL ats_cursor_get_delta_pos(struct ats_vec2i *delta_position)
{
        _ats_cursor_get_delta_pos(delta_position);
}

ATSAPI void ATSCALL ats_cursor_get_pos(struct ats_vec2i *position)
{
        _ats_cursor_get_pos(position);
}
