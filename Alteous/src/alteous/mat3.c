/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file mat3.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 02/12/15
 * @brief Implementation of operations on a 3x3 matrix.
 */

#include <stdio.h>
#include <tgmath.h>
#include <string.h>

#include "alteous/mat3.h"
#include "alteous/assertion.h"

ATSAPI void ATSCALL ats_mat3_identity(struct ats_mat3 *matrix)
{
        ATS_DEBUG_ASSERT(NULL != matrix);

        matrix->m[0] = 1.0f;
        matrix->m[1] = 0.0f;
        matrix->m[2] = 0.0f;
        matrix->m[3] = 0.0f;
        matrix->m[4] = 1.0f;
        matrix->m[5] = 0.0f;
        matrix->m[6] = 0.0f;
        matrix->m[7] = 0.0f;
        matrix->m[8] = 1.0f;
}

ATSAPI void ATSCALL ats_mat3_adj(struct ats_mat3 *output,
                                 const struct ats_mat3 *input)
{
        ATS_DEBUG_ASSERT(NULL != output);
        ATS_DEBUG_ASSERT(NULL != input);

        output->m[0] = input->m[4] * input->m[8] - input->m[5] * input->m[7];
        output->m[1] = input->m[2] * input->m[7] - input->m[1] * input->m[8];
        output->m[2] = input->m[1] * input->m[5] - input->m[2] * input->m[4];

        output->m[3] = input->m[5] * input->m[6] - input->m[3] * input->m[8];
        output->m[4] = input->m[0] * input->m[8] - input->m[2] * input->m[6];
        output->m[5] = input->m[2] * input->m[3] - input->m[0] * input->m[5];

        output->m[6] = input->m[3] * input->m[7] - input->m[4] * input->m[6];
        output->m[7] = input->m[1] * input->m[6] - input->m[0] * input->m[7];
        output->m[8] = input->m[0] * input->m[4] - input->m[1] * input->m[3];
}

ATSAPI ats_float ATSCALL ats_mat3_det(const struct ats_mat3 *input)
{
        ATS_DEBUG_ASSERT(NULL != input);

        return input->m[0] * input->m[4] * input->m[8] +
               input->m[1] * input->m[5] * input->m[6] +
               input->m[2] * input->m[3] * input->m[7] -
               input->m[2] * input->m[4] * input->m[6] -
               input->m[0] * input->m[5] * input->m[7] -
               input->m[1] * input->m[3] * input->m[8];
}

ATSAPI ats_bool ATSCALL ats_mat3_inv(struct ats_mat3 *output,
                                     const struct ats_mat3 *input)
{
        ATS_DEBUG_ASSERT(NULL != output);
        ATS_DEBUG_ASSERT(NULL != input);
        
        ats_float determinant = ats_mat3_det(input);
        if (fabsf(determinant) < 1e-6) {
                return ATS_FALSE;
        }

        ats_mat3_adj(output, input);
        ats_mat3_scl_prod(output, 1.0f / determinant);

        return ATS_TRUE;
}

ATSAPI void ATSCALL ats_mat3_mat_prod(struct ats_mat3 *output, const struct ats_mat3 *left,
                                      const struct ats_mat3 *right)
{
        ATS_DEBUG_ASSERT(NULL != output);
        ATS_DEBUG_ASSERT(NULL != left);
        ATS_DEBUG_ASSERT(NULL != right);
        
        output->m[0] = left->m[0] * right->m[0] +
                       left->m[3] * right->m[1] +
                       left->m[6] * right->m[2];

        output->m[1] = left->m[1] * right->m[0] +
                       left->m[4] * right->m[1] +
                       left->m[7] * right->m[2];

        output->m[2] = left->m[2] * right->m[0] +
                       left->m[5] * right->m[1] +
                       left->m[8] * right->m[2];

        output->m[3] = left->m[0] * right->m[3] +
                       left->m[3] * right->m[4] +
                       left->m[6] * right->m[5];

        output->m[4] = left->m[1] * right->m[3] +
                       left->m[4] * right->m[4] +
                       left->m[7] * right->m[5];

        output->m[5] = left->m[2] * right->m[3] +
                       left->m[5] * right->m[4] +
                       left->m[8] * right->m[5];

        output->m[6] = left->m[0] * right->m[6] +
                       left->m[3] * right->m[7] +
                       left->m[6] * right->m[8];

        output->m[7] = left->m[1] * right->m[6] +
                       left->m[4] * right->m[7] +
                       left->m[7] * right->m[8];

        output->m[8] = left->m[2] * right->m[6] +
                       left->m[5] * right->m[7] +
                       left->m[8] * right->m[8];
}

ATSAPI ats_bool ATSCALL ats_mat3_normal(struct ats_mat3 *output, const struct ats_mat4 *modelview)
{
        ATS_DEBUG_ASSERT(NULL != output);
        ATS_DEBUG_ASSERT(NULL != modelview);
        
        output->m[0] = modelview->m[0];
        output->m[1] = modelview->m[1];
        output->m[2] = modelview->m[2];

        output->m[3] = modelview->m[4];
        output->m[4] = modelview->m[5];
        output->m[5] = modelview->m[6];

        output->m[6] = modelview->m[8];
        output->m[7] = modelview->m[9];
        output->m[8] = modelview->m[10];

        struct ats_mat3 inverse;
        ats_bool invertible = ats_mat3_inv(&inverse, output);
        if (!invertible) {
                return ATS_FALSE;
        }

        ats_mat3_transpose(&inverse);

        memcpy(output->m, inverse.m, 9 * sizeof(ats_float));

        return ATS_TRUE;
}

ATSAPI void ATSCALL ats_mat3_print(const struct ats_mat3 *matrix)
{
        ATS_DEBUG_ASSERT(NULL != matrix);
        
        for (size_t i = 0; i < 3; i++) {
                printf("[%c%4f  %c%4f  %c%4f ]\n",
                       matrix->m[0 + i] < 0.0f ? '-' : ' ', fabsf(matrix->m[0 + i]),
                       matrix->m[3 + i] < 0.0f ? '-' : ' ', fabsf(matrix->m[3 + i]),
                       matrix->m[6 + i] < 0.0f ? '-' : ' ', fabsf(matrix->m[6 + i])
                );
        }
}

ATSAPI void ATSCALL ats_mat3_scl_prod(struct ats_mat3 *matrix, ats_float scalar)
{
        ATS_DEBUG_ASSERT(NULL != matrix);
        
        matrix->m[0] *= scalar;
        matrix->m[1] *= scalar;
        matrix->m[2] *= scalar;

        matrix->m[3] *= scalar;
        matrix->m[4] *= scalar;
        matrix->m[5] *= scalar;

        matrix->m[6] *= scalar;
        matrix->m[7] *= scalar;
        matrix->m[8] *= scalar;
}

ATSAPI void ATSCALL ats_mat3_transpose(struct ats_mat3 *matrix)
{
        ATS_DEBUG_ASSERT(NULL != matrix);

        struct ats_mat3 transpose;

        transpose.m[0] = matrix->m[0];
        transpose.m[1] = matrix->m[3];
        transpose.m[2] = matrix->m[6];

        transpose.m[3] = matrix->m[1];
        transpose.m[4] = matrix->m[4];
        transpose.m[5] = matrix->m[7];

        transpose.m[6] = matrix->m[2];
        transpose.m[7] = matrix->m[5];
        transpose.m[8] = matrix->m[8];

        memcpy(matrix->m, transpose.m, 9 * sizeof(ats_float));
}
