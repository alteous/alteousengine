/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file component.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 30/12/15
 * @brief Implementation of component creation functions.
 */

#include "alteous/component.h"

#include <stdlib.h>

#include "alteous/assertion.h"

ATSAPI void* ATSCALL ats_comp_init(enum ats_comp_type type, ats_size count)
{
        ATS_DEBUG_ASSERT(0 != count);

        ats_size size = ats_comp_sizeof(type);

        /* We have to check that size > 0 because malloc(0) is implementation defined. */

        return size > 0 ? malloc(count * ats_comp_sizeof(type)) : NULL;
}

ATSAPI void ATSCALL ats_comp_free(void *comp_ptr)
{
        free(comp_ptr);
}

ATSAPI ats_size ATSCALL ats_comp_sizeof(enum ats_comp_type type)
{
        switch (type) {
                case AtsCompPos:
                        return sizeof(struct ats_comp_pos);
                case AtsCompMvt:
                        return sizeof(struct ats_comp_mvt);
                case AtsCompRdr:
                        return sizeof(struct ats_comp_rdr);
                default:
                        return 0;
        }
}
