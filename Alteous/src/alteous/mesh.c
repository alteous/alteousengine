/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file mesh.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 31/12/15
 * @brief Implementation of mesh functions.
 */

#include "alteous/shader.h"
#include "alteous/mesh.h"

ATSAPI void ATSCALL ats_mesh_render(const struct ats_mesh *mesh)
{
        if (mesh->positions) {
                glEnableVertexAttribArray(AtsPositionAttrib);
                glVertexAttribPointer(AtsPositionAttrib, 3, GL_FLOAT, GL_FALSE, 0,
                                      mesh->positions);
        }

        if (mesh->textures) {
                glEnableVertexAttribArray(AtsTextureAttrib);
                glVertexAttribPointer(AtsTextureAttrib, 2, GL_FLOAT, GL_FALSE, 0,
                                      mesh->textures);
        }

        if (mesh->normals) {
                glEnableVertexAttribArray(AtsNormalAttrib);
                glVertexAttribPointer(AtsNormalAttrib, 3, GL_FLOAT, GL_TRUE, 0, mesh->normals);
        }

        if (mesh->indices) {
                glDrawElements(GL_TRIANGLES, mesh->index_count, GL_UNSIGNED_SHORT, mesh->indices);

        } else {
                glDrawArrays(GL_TRIANGLES, 0, mesh->vertex_count);
        }

        if (mesh->normals) {
                glDisableVertexAttribArray(AtsNormalAttrib);
        }

        if (mesh->textures) {
                glDisableVertexAttribArray(AtsTextureAttrib);
        }

        if (mesh->positions) {
                glDisableVertexAttribArray(AtsPositionAttrib);
        }
}
