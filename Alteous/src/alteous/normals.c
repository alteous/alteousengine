/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file normals.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 22/12/15
 * @brief Implementation of vertex normal computations.
 */

#include "alteous/normals.h"

#include <string.h>

#include "alteous/assertion.h"

ATSAPI void ATSCALL ats_normals_compute(const struct ats_vec3f *vertices, ats_size vertex_count,
                                        const ats_ushort *indices, ats_size index_count,
                                        struct ats_vec3f *normals)
{
        ATS_DEBUG_ASSERT(NULL != vertices);
        ATS_DEBUG_ASSERT(NULL != indices);
        ATS_DEBUG_ASSERT(NULL != normals);
        ATS_DEBUG_ASSERT(0 == index_count % 3);

        /* Zero initialise the normal vectors. */
        memset(normals, 0, vertex_count * sizeof(struct ats_vec3f));

        const struct ats_vec3f *v0, *v1, *v2;
        struct ats_vec3f surface_normal;
        for (ats_size i = 0; i < index_count; i += 3) {

                /* Determine the three vertices that form the plane of each face. */
                v0 = vertices + indices[i + 0];
                v1 = vertices + indices[i + 1];
                v2 = vertices + indices[i + 2];

                /* Compute the surface normal of the plane passing through v0, v1, v2. */
                ats_vec3f_surf_norm(&surface_normal, v0, v1, v2);

                /* Add the surface normal to each of the indexed normal vectors. */
                ats_vec3f_add(normals + indices[i + 0], &surface_normal);
                ats_vec3f_add(normals + indices[i + 1], &surface_normal);
                ats_vec3f_add(normals + indices[i + 2], &surface_normal);
        }

        /* Average the sums of the surface normals to yield "smooth" normal vectors. */
        for (ats_size i = 0; i < vertex_count; i ++) {
                ats_vec3f_normalise(normals + i);
        }
}
