/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file comparator.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 19/12/15
 * @brief Implementation of basic comparators.
 */

#include "alteous/comparator.h"

#include <tgmath.h>
#include <assert.h>

ATSAPI ats_int ATSCALL ats_compare_int(void *left_ptr, void *right_ptr)
{
#ifdef DEBUG
        assert(NULL != left_ptr);
        assert(NULL != right_ptr);
#endif
        ats_int left = *(ats_int *) left_ptr;
        ats_int right = *(ats_int *) right_ptr;

        return (left < right) - (right < left);
}

ATSAPI ats_int ATSCALL ats_compare_float(void *left_ptr, void *right_ptr)
{
#ifdef DEBUG
        assert(NULL != left_ptr);
        assert(NULL != right_ptr);
#endif
        ats_float left = *(ats_float *) left_ptr;
        ats_float right = *(ats_float *) right_ptr;

        return (fabsf(left - right) < 1e-6) - (fabsf(right - left) < 1e-6);
}

ATSAPI ats_int ATSCALL ats_compare_char(void *left_ptr, void *right_ptr)
{
#ifdef DEBUG
        assert(NULL != left_ptr);
        assert(NULL != right_ptr);
#endif
        ats_char left = *(ats_char *) left_ptr;
        ats_char right = *(ats_char *) right_ptr;

        return (left < right) - (right < left);
}

ATSAPI ats_int ATSCALL ats_compare_ptr(void *left_ptr, void *right_ptr)
{
#ifdef DEBUG
        assert(NULL != left_ptr);
        assert(NULL != right_ptr);
#endif
        return (left_ptr < right_ptr) - (right_ptr < left_ptr);
}

ATSAPI ats_int ATSCALL ats_compare_size(void *left_ptr, void *right_ptr)
{
#ifdef DEBUG
        assert(NULL != left_ptr);
        assert(NULL != right_ptr);
#endif
        ats_size left = *(ats_size *) left_ptr;
        ats_size right = *(ats_size *) right_ptr;

        return (left < right) - (right < left);
}

ATSAPI ats_int ATSCALL ats_compare_bool(void *left_ptr, void *right_ptr)
{
#ifdef DEBUG
        assert(NULL != left_ptr);
        assert(NULL != right_ptr);
#endif
        return *(ats_bool *) left_ptr == *(ats_bool *) right_ptr;
}
