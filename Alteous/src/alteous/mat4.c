/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file mat4.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 02/12/15
 * @brief Implementation of operations on a 4x4 matrix.
 */

#include "alteous/mat4.h"

#include <stdio.h>
#include <tgmath.h>
#include <string.h>

#include "alteous/assertion.h"

static const ats_float equals_tolerance = 1e-8;

ATSAPI void ATSCALL ats_mat4_copy(struct ats_mat4 *target, const struct ats_mat4 *arg)
{
        memcpy(target->m, arg->m, 16 * sizeof(ats_float));
}

ATSAPI void ATSCALL ats_mat4_identity(struct ats_mat4 *matrix)
{
        matrix->m[ 0] = 1.0f;
        matrix->m[ 1] = 0.0f;
        matrix->m[ 2] = 0.0f;
        matrix->m[ 3] = 0.0f;
        matrix->m[ 4] = 0.0f;
        matrix->m[ 5] = 1.0f;
        matrix->m[ 6] = 0.0f;
        matrix->m[ 7] = 0.0f;
        matrix->m[ 8] = 0.0f;
        matrix->m[ 9] = 0.0f;
        matrix->m[10] = 1.0f;
        matrix->m[11] = 0.0f;
        matrix->m[12] = 0.0f;
        matrix->m[13] = 0.0f;
        matrix->m[14] = 0.0f;
        matrix->m[15] = 1.0f;
}

ATSAPI ats_bool ATSCALL ats_mat4_eq(const struct ats_mat4 *left, const struct ats_mat4 *right)
{
        ATS_DEBUG_ASSERT(NULL != left);
        ATS_DEBUG_ASSERT(NULL != right);

        for (ats_size i = 0; i < 16; i++) {
                if (fabsf(left->m[i] - right->m[i]) > equals_tolerance) {
                        return ATS_FALSE;
                }
        }

        return ATS_TRUE;
}

ATSAPI void ATSCALL ats_mat4_mat_prod(struct ats_mat4 *result, const struct ats_mat4 *left,
                                      const struct ats_mat4 *right)
{
        ATS_DEBUG_ASSERT(NULL != result);
        ATS_DEBUG_ASSERT(NULL != left);
        ATS_DEBUG_ASSERT(NULL != right);

        result->m[0]  = left->m[0]  * right->m[0]  + left->m[4]  * right->m[1] +
                        left->m[8]  * right->m[2]  + left->m[12] * right->m[3];
        result->m[1]  = left->m[1]  * right->m[0]  + left->m[5]  * right->m[1] +
                        left->m[9]  * right->m[2]  + left->m[13] * right->m[3];
        result->m[2]  = left->m[2]  * right->m[0]  + left->m[6]  * right->m[1] +
                        left->m[10] * right->m[2]  + left->m[14] * right->m[3];
        result->m[3]  = left->m[3]  * right->m[0]  + left->m[7]  * right->m[1] +
                        left->m[11] * right->m[2]  + left->m[15] * right->m[3];
        result->m[4]  = left->m[0]  * right->m[4]  + left->m[4]  * right->m[5] +
                        left->m[8]  * right->m[6]  + left->m[12] * right->m[7];
        result->m[5]  = left->m[1]  * right->m[4]  + left->m[5]  * right->m[5] +
                        left->m[9]  * right->m[6]  + left->m[13] * right->m[7];
        result->m[6]  = left->m[2]  * right->m[4]  + left->m[6]  * right->m[5] +
                        left->m[10] * right->m[6]  + left->m[14] * right->m[7];
        result->m[7]  = left->m[3]  * right->m[4]  + left->m[7]  * right->m[5] +
                        left->m[11] * right->m[6]  + left->m[15] * right->m[7];
        result->m[8]  = left->m[0]  * right->m[8]  + left->m[4]  * right->m[9] +
                        left->m[8]  * right->m[10] + left->m[12] * right->m[11];
        result->m[9]  = left->m[1]  * right->m[8]  + left->m[5]  * right->m[9] +
                        left->m[9]  * right->m[10] + left->m[13] * right->m[11];
        result->m[10] = left->m[2]  * right->m[8]  + left->m[6]  * right->m[9] +
                        left->m[10] * right->m[10] + left->m[14] * right->m[11];
        result->m[11] = left->m[3]  * right->m[8]  + left->m[7]  * right->m[9] +
                        left->m[11] * right->m[10] + left->m[15] * right->m[11];
        result->m[12] = left->m[0]  * right->m[12] + left->m[4]  * right->m[13] +
                        left->m[8]  * right->m[14] + left->m[12] * right->m[15];
        result->m[13] = left->m[1]  * right->m[12] + left->m[5]  * right->m[13] +
                        left->m[9]  * right->m[14] + left->m[13] * right->m[15];
        result->m[14] = left->m[2]  * right->m[12] + left->m[6]  * right->m[13] +
                        left->m[10] * right->m[14] + left->m[14] * right->m[15];
        result->m[15] = left->m[3]  * right->m[12] + left->m[7]  * right->m[13] +
                        left->m[11] * right->m[14] + left->m[15] * right->m[15];
}

ATSAPI void ATSCALL ats_mat4_scl_prod(struct ats_mat4 *target, ats_float scalar)
{
        ATS_DEBUG_ASSERT(NULL != target);

        for (int i = 0; i < 16; i++) {
                target->m[i] *= scalar;
        }
}

ATSAPI void ATSCALL ats_mat4_make_projection(struct ats_mat4 *matrix,
                                             const struct ats_angle *field_of_view,
                                             ats_float aspect_ratio, ats_float near_z_plane,
                                             ats_float far_z_plane)
{
        ATS_DEBUG_ASSERT(NULL != matrix);
        ATS_DEBUG_ASSERT(NULL != field_of_view);
        ATS_DEBUG_ASSERT(aspect_ratio > 0.0f);
        ATS_DEBUG_ASSERT(near_z_plane > 0.0f);
        ATS_DEBUG_ASSERT(far_z_plane > near_z_plane);

        ats_float cotangent = 1.0f / tanf(0.5f * ats_angle_radians(field_of_view));
        struct ats_mat4 projection_matrix = {
                {
                        1.0f * cotangent / aspect_ratio, 0, 0, 0,
                        0, cotangent, 0, 0,
                        0, 0, (near_z_plane + far_z_plane) / (near_z_plane - far_z_plane), -1,
                        0, 0, 2 * near_z_plane * far_z_plane / (near_z_plane - far_z_plane), 0
                }
        };

        ats_mat4_copy(matrix, &projection_matrix);
}

ATSAPI void ATSCALL ats_mat4_print(const struct ats_mat4 *matrix)
{
        ATS_DEBUG_ASSERT(NULL != matrix);

        for (int i = 0; i < 4; i++) {
                printf("[%c%4f  %c%4f  %c%4f  %c%4f ]\n",
                       matrix->m[0 + i]  < 0.0f ? '-' : ' ', fabsf(matrix->m[0 + i]),
                       matrix->m[4 + i]  < 0.0f ? '-' : ' ', fabsf(matrix->m[4 + i]),
                       matrix->m[8 + i]  < 0.0f ? '-' : ' ', fabsf(matrix->m[8 + i]),
                       matrix->m[12 + i] < 0.0f ? '-' : ' ', fabsf(matrix->m[12 + i])
                );
        }
}

ATSAPI void ATSCALL ats_mat4_make_rotX(struct ats_mat4 *output, const struct ats_angle *angle)
{
        ATS_DEBUG_ASSERT(NULL != output);
        ATS_DEBUG_ASSERT(NULL != angle);

        const float cosA = cosf(ats_angle_radians(angle));
        const float sinA = sinf(ats_angle_radians(angle));
        struct ats_mat4 rotation_matrix = {
                {
                        1.0f,  0.0f,  0.0f,  0.0f,
                        0.0f,  cosA,  sinA,  0.0f,
                        0.0f, -sinA,  cosA,  0.0f,
                        0.0f,  0.0f,  0.0f,  1.0f
                }
        };

        ats_mat4_copy(output, &rotation_matrix);
}

ATSAPI void ATSCALL ats_mat4_make_rotY(struct ats_mat4 *output, const struct ats_angle *angle)
{
        ATS_DEBUG_ASSERT(NULL != output);
        ATS_DEBUG_ASSERT(NULL != angle);

        const float cosA = cosf(ats_angle_radians(angle));
        const float sinA = sinf(ats_angle_radians(angle));
        struct ats_mat4 rotation_matrix = {
                {
                        cosA,  0.0f, -sinA,  0.0f,
                        0.0f,  1.0f,  0.0f,  0.0f,
                        sinA,  0.0f,  cosA,  0.0f,
                        0.0f,  0.0f,  0.0f,  1.0f
                }
        };

        ats_mat4_copy(output, &rotation_matrix);
}

ATSAPI void ATSCALL ats_mat4_make_rotZ(struct ats_mat4 *output, const struct ats_angle *angle)
{
        ATS_DEBUG_ASSERT(NULL != output);
        ATS_DEBUG_ASSERT(NULL != angle);

        const float cosA = cosf(ats_angle_radians(angle));
        const float sinA = sinf(ats_angle_radians(angle));
        struct ats_mat4 rotation_matrix = {
                {
                        cosA,  sinA,  0.0f,  0.0f,
                       -sinA,  cosA,  0.0f,  0.0f,
                        0.0f,  0.0f,  1.0f,  0.0f,
                        0.0f,  0.0f,  0.0f,  1.0f
                }
        };

        ats_mat4_copy(output, &rotation_matrix);
}

ATSAPI void ATSCALL ats_mat4_make_scale(struct ats_mat4 *output, const struct ats_vec3f *v)
{
        ATS_DEBUG_ASSERT(NULL != output);

        struct ats_mat4 scale_matrix = {
                {
                        v->x, 0.0f, 0.0f, 0.0f,
                        0.0f, v->y, 0.0f, 0.0f,
                        0.0f, 0.0f, v->z, 0.0f,
                        0.0f, 0.0f, 0.0f, 1.0f
                }
        };

        ats_mat4_copy(output, &scale_matrix);
}

ATSAPI void ATSCALL ats_mat4_make_translation(struct ats_mat4 *output, const struct ats_vec3f *v)
{
        ATS_DEBUG_ASSERT(NULL != output);

        struct ats_mat4 translation_matrix = {
                {
                        1.0f,  0.0f,  0.0f,  0.0f,
                        0.0f,  1.0f,  0.0f,  0.0f,
                        0.0f,  0.0f,  1.0f,  0.0f,
                        v->x,  v->y,  v->z,  1.0f
                }
        };

        ats_mat4_copy(output, &translation_matrix);
}
