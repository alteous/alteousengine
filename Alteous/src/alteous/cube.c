/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file cube.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 01/01/16
 * @brief Implementation of cube mesh generation.
 */

#include "alteous/cube.h"

#include <stdlib.h>
#include <string.h>

ats_float positions[] = {
        -0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, 0.5f,
        0.5f, 0.5f, 0.5f,
        -0.5f, 0.5f, 0.5f,

        -0.5f, -0.5f, -0.5f,
        0.5f, -0.5f, -0.5f,
        0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, -0.5f,

        0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, -0.5f,
        0.5f, 0.5f, -0.5f,
        0.5f, 0.5f, 0.5f,

        -0.5f, -0.5f, 0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, 0.5f,

        -0.5f, 0.5f, 0.5f,
        0.5f, 0.5f, 0.5f,
        0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, -0.5f,

        -0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f
};

ats_float colours[] = {
        0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f,

        0.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 1.0f,

        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 1.0f,

        0.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f
};

ats_float textures[] = {
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,

        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,

        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,

        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,

        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,

        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f
};

ats_float normals[] = {
        -0.577350f, -0.577350f, 0.577350f,
        0.577350f, -0.577350f, 0.577350f,
        0.577350f, 0.577350f, 0.577350f,
        -0.577350f, 0.577350f, 0.577350f,

        -0.577350f, -0.577350f, -0.577350f,
        0.577350f, -0.577350f, -0.577350f,
        0.577350f, 0.577350f, -0.577350f,
        -0.577350f, 0.577350f, -0.577350f,

        0.577350f, -0.577350f, 0.577350f,
        0.577350f, -0.577350f, -0.577350f,
        0.577350f, 0.577350f, -0.577350f,
        0.577350f, 0.577350f, 0.577350f,

        -0.577350f, -0.577350f, 0.577350f,
        -0.577350f, -0.577350f, -0.577350f,
        -0.577350f, 0.577350f, -0.577350f,
        -0.577350f, 0.577350f, 0.577350f,

        -0.577350f, 0.577350f, 0.577350f,
        0.577350f, 0.577350f, 0.577350f,
        0.577350f, 0.577350f, -0.577350f,
        -0.577350f, 0.577350f, -0.577350f,

        -0.577350f, -0.577350f, 0.577350f,
        0.577350f, -0.577350f, 0.577350f,
        0.577350f, -0.577350f, -0.577350f,
        -0.577350f, -0.577350f, -0.577350f
};

ats_ushort indices[] = {
        0, 1, 2,
        0, 2, 3,

        4, 6, 5,
        4, 7, 6,

        8, 9, 10,
        8, 10, 11,

        12, 14, 13,
        12, 15, 14,

        16, 17, 18,
        16, 18, 19,

        20, 22, 21,
        20, 23, 22
};

ATSAPI struct ats_mesh* ATSCALL ats_cube_init()
{
        struct ats_mesh *mesh = malloc(sizeof(*mesh));
        if (NULL == mesh) {
                return NULL;
        }

        mesh->positions = malloc(sizeof(positions));
        if (NULL == mesh->positions) {
                goto free_mesh;
                return NULL;
        }
        memcpy(mesh->positions, positions, sizeof(positions));

        mesh->textures = malloc(sizeof(textures));
        if (NULL == mesh->textures) {
                goto free_positions;
        }
        memcpy(mesh->textures, textures, sizeof(textures));

        mesh->normals = malloc(sizeof(normals));
        if (NULL == mesh->normals) {
                goto free_textures;
        }
        memcpy(mesh->normals, normals, sizeof(normals));

        mesh->indices = malloc(sizeof(indices));
        if (NULL == mesh->indices) {
                goto free_normals;
        }
        memcpy(mesh->indices, indices, sizeof(indices));

        mesh->vertex_count = sizeof(positions) / sizeof(*positions);
        mesh->index_count = sizeof(indices) / sizeof(*indices);
        mesh->name = "cube";

        return mesh;

        free_normals:
                free(mesh->normals);
        free_textures:
                free(mesh->textures);
        free_positions:
                free(mesh->positions);
        free_mesh:
                free(mesh);

        return NULL;
}
