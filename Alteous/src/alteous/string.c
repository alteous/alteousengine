/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file string.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 11/12/15
 * @brief Implementation of an basic string API.
 */

#include "alteous/string.h"
#include "alteous/memory.h"

#include <assert.h>
#include <stdio.h>

#define NULL_CHAR '\0'

static void _ats_string_copy(const ats_char *input, ats_size input_length, ats_char *output)
{
        for (ats_size i = 0; i < input_length; i++) {
                output[i] = input[i];
        }
        output[input_length] = '\0';
}

static ats_size _ats_string_literal_length(const ats_char *literal)
{
        ats_size length = 0;
        while (NULL_CHAR != literal[length]) {
                length++;
        }

        return length;
}

ATSAPI struct ats_string * ATSCALL ats_string_init(const ats_char *format, ...)
{
#ifdef DEBUG
        assert(NULL != format);
#endif
        struct ats_string *string = ATS_ALLOC(struct ats_string);
        if (NULL == string) {
                return NULL;
        }

        va_list arg_list;
        va_start(arg_list, format);

        ats_size formatted_size = (ats_size) snprintf(NULL, 0, format, arg_list);
        ats_char *formatted_chars = ATS_MALLOC(1 + formatted_size, ats_char);
        if (NULL == formatted_chars) {
                va_end(arg_list);
                free(string);
                return NULL;
        }
        snprintf(formatted_chars, 1 + formatted_size, format, arg_list);
        formatted_chars[formatted_size] = NULL_CHAR;
        va_end(arg_list);

        string->chars = formatted_chars;
        string->length = formatted_size;

        return string;
}

ATSAPI struct ats_string * ATSCALL ats_string_init_empty()
{
        return ats_string_init("");
}

ATSAPI void ATSCALL ats_string_free(struct ats_string *string)
{
#ifdef DEBUG
        assert(NULL != string);
#endif
        free(string->chars);
        free(string);
}

ATSAPI ats_error ATSCALL ats_string_append(struct ats_string *target, const ats_char *right)
{
#ifdef DEBUG
        assert(NULL != target);
        assert(NULL != right);
#endif
        ats_size right_length = _ats_string_literal_length(right);
        ats_size old_length = target->length;
        ats_size new_length = old_length + right_length;

        target->chars = target->chars ? ATS_REALLOC(target->chars, 1 + new_length, ats_char)
                                      : ATS_MALLOC(1 + new_length, ats_char);
        if (NULL == target->chars) {
                return AtsAllocFailed;
        }

        _ats_string_copy(right, right_length, target->chars + old_length);
        target->length = new_length;

        return AtsNoError;
}

ATSAPI ats_error ATSCALL ats_string_concat(struct ats_string *output, const ats_char *left,
                                           const ats_char *right)
{
#ifdef DEBUG
        assert(NULL != output);
        assert(NULL != left);
        assert(NULL != right);
#endif
        size_t left_length = _ats_string_literal_length(left);
        size_t right_length = _ats_string_literal_length(right);
        size_t concat_length = left_length + right_length;

        output->chars = output->chars
                ? ATS_REALLOC(output->chars, 1 + concat_length, ats_char)
                : ATS_MALLOC(1 + concat_length, ats_char);
        if (NULL == output->chars) {
                return AtsAllocFailed;
        }

        _ats_string_copy(left, left_length, output->chars);
        _ats_string_copy(right, right_length, output->chars + left_length);
        output->length = concat_length;

        return AtsNoError;
}

ATSAPI ats_error ATSCALL ats_string_copy(const struct ats_string *src, struct ats_string *dest)
{
#ifdef DEBUG
        assert(NULL != src);
        assert(NULL != dest);
#endif
        if (dest->chars) {
                void *ptr = ATS_REALLOC(dest->chars, 1 + src->length, ats_char);
                if (NULL == ptr) {
                        return AtsAllocFailed;
                }
                dest->chars = ptr;
                dest->length = src->length;
        } else {
                dest->chars = ATS_MALLOC(1 + src->length, ats_char);
                if (NULL == dest->chars) {
                        return AtsAllocFailed;
                }
        }

        _ats_string_copy(src->chars, src->length, dest->chars);

        return AtsNoError;
}

ATSAPI ats_error ATSCALL ats_string_copy_lit(const ats_char *literal, struct ats_string *dest)
{
#ifdef DEBUG
        assert(NULL != literal);
#endif
        ats_size literal_length = _ats_string_literal_length(literal);
        if (dest->chars) {
                void *ptr = ATS_REALLOC(dest->chars, 1 + literal_length, ats_char);
                if (NULL == ptr) {
                        return AtsAllocFailed;
                }
                dest->chars = ptr;
                dest->length = literal_length;
        } else {
                dest->chars = ATS_MALLOC(1 + literal_length, ats_char);
                if (NULL == dest->chars) {
                        return AtsAllocFailed;
                }
        }

        _ats_string_copy(literal, literal_length, dest->chars);

        return AtsNoError;
}

ATSAPI const ats_char * ATSCALL ats_string_data(const struct ats_string *string)
{
        return string->chars;
}

ATSAPI ats_size ATSCALL ats_string_length(const struct ats_string *string)
{
        return string->length;
}

ATSAPI ats_error ATSCALL ats_string_prepend(struct ats_string *target, const ats_char *left)
{
        size_t left_length = _ats_string_literal_length(left);
        size_t old_length = target->length;
        size_t new_length = old_length + left_length;

        target->chars = target->chars
                        ? ATS_REALLOC(target->chars,  + new_length, ats_char)
                        : ATS_MALLOC(1 + new_length, ats_char);

        if (NULL == target->chars) {
                return AtsAllocFailed;
        }

        _ats_string_copy(target->chars, old_length, target->chars + left_length);
        _ats_string_copy(left, left_length, target->chars);
        target->length = new_length;

        return AtsNoError;
}
