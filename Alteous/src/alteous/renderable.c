/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file renderable.c
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 03/01/16
 */

#include "alteous/renderable.h"

#include <stdlib.h>
#include <setjmp.h>
#include <string.h>

#include "alteous/shader.h"
#include "alteous/macros.h"
#include "alteous/assertion.h"
#include "alteous/mesh.h"
#include "alteous/gl.h"
#include "alteous/glext.h"

static jmp_buf ogl_error;

typedef GLuint ogl_buf_id;

/**
 * @position_buffer An array of GL_ARRAY_BUFFER ids containing the position vectors of a renderable.
 * @normal_buffer An array of GL_ARRAY_BUFFER ids containing the vertex normals of a renderable.
 * @texture_buffer An array of GL_ARRAY_BUFFER ids containing the texture co-ordinates of a
 * renderable.
 * @index_buffer An array of GL_ELEMENT_ARRAY_BUFFER ids containing vertex draw sequence, if
 * defined, of a renderable.
 * @name The name of the renderable (must be unique).
 * @vertex_count The number of positions, normals, etc. for each renderable in the soa.
 * @index_count The number of draw sequence indices, if defined, for each renderable in the soa.
 * @count The number of renderables stored in the soa.
 * @capacity The current capacity of the soa, i.e. the number of renderables the soa can store
 * before resizing.
 */
struct ats_renderable_soa {
        ogl_buf_id *position_buffer;
        ogl_buf_id *normal_buffer;
        ogl_buf_id *texture_buffer;
        ogl_buf_id *index_buffer;
        ats_char  **name;
        ats_int    *vertex_count;
        ats_int    *index_count;
        ats_int     count;
        ats_int     capacity;
};

/** The initial capacity of an ats_renderable_soa. */
#define INIT_TABLE_CAPACITY 256

/** The engine's renderable SOA table. */
static struct ats_renderable_soa soa;

/** True if the engine renderable soa is currently initialised. */
static ats_bool table_initialised = ATS_FALSE;

/** The number of floats per position vector in a GL_ARRAY_BUFFER. */
static const ats_size comps_per_position = 3;

/** The number of floats per vertex normal in a GL_ARRAY_BUFFER. */
static const ats_size comps_per_normal = 3;

/** The number of floats per texture co-ordinate in a GL_ARRAY_BUFFER. */
static const ats_size comps_per_texture = 2;

/** Initialises the engine renderable soa. */
static ats_error init_soa()
{
        soa.position_buffer = calloc(INIT_TABLE_CAPACITY, sizeof(*soa.position_buffer));
        if (NULL == soa.position_buffer) {
                goto position_buffer_alloc_failed;
        }

        soa.normal_buffer = calloc(INIT_TABLE_CAPACITY, sizeof(*soa.normal_buffer));
        if (NULL == soa.normal_buffer) {
                goto normal_buffer_alloc_failed;
        }

        soa.texture_buffer = calloc(INIT_TABLE_CAPACITY, sizeof(*soa.texture_buffer));
        if (NULL == soa.texture_buffer) {
                goto texture_buffer_alloc_failed;
        }

        soa.index_buffer = calloc(INIT_TABLE_CAPACITY, sizeof(*soa.index_buffer));
        if (NULL == soa.index_buffer) {
                goto index_buffer_alloc_failed;
        }

        soa.name = calloc(INIT_TABLE_CAPACITY, sizeof(*soa.name));
        if (NULL == soa.name) {
                goto name_alloc_failed;
        }

        soa.index_count = calloc(INIT_TABLE_CAPACITY, sizeof(*soa.index_count));
        if (NULL == soa.index_count) {
                goto index_count_alloc_failed;
        }

        soa.vertex_count = calloc(INIT_TABLE_CAPACITY, sizeof(*soa.vertex_count));
        if (NULL == soa.vertex_count) {
                goto vertex_count_alloc_failed;
        }

        soa.count = 0;
        soa.capacity = INIT_TABLE_CAPACITY;

        return AtsNoError;

        vertex_count_alloc_failed:
        free(soa.vertex_count);

        index_count_alloc_failed:
        free(soa.name);

        name_alloc_failed:
        free(soa.index_buffer);

        index_buffer_alloc_failed:
        free(soa.texture_buffer);

        texture_buffer_alloc_failed:
        free(soa.normal_buffer);

        normal_buffer_alloc_failed:
        free(soa.position_buffer);

        position_buffer_alloc_failed:

        return AtsAllocFailed;
}

/** Doubles the size of the engine renderable soa. */
static ats_error resize_soa()
{
        ats_int new_limit = 2 * soa.capacity;
        void *new_ptr = NULL;

        new_ptr = realloc(soa.texture_buffer, new_limit * sizeof(*soa.texture_buffer));
        if (NULL == new_ptr) {
                return AtsAllocFailed;
        }
        memset(new_ptr + soa.count, 0, soa.capacity * sizeof(*soa.texture_buffer));
        soa.texture_buffer = new_ptr;

        new_ptr = realloc(soa.position_buffer, new_limit * sizeof(*soa.position_buffer));
        if (NULL == new_ptr) {
                return AtsAllocFailed;
        }
        memset(new_ptr + soa.count, 0, soa.capacity * sizeof(*soa.position_buffer));
        soa.position_buffer = new_ptr;

        new_ptr = realloc(soa.normal_buffer, new_limit * sizeof(*soa.normal_buffer));
        if (NULL == new_ptr) {
                return AtsAllocFailed;
        }
        memset(new_ptr + soa.count, 0, soa.capacity * sizeof(*soa.normal_buffer));
        soa.normal_buffer = new_ptr;

        new_ptr = realloc(soa.index_buffer, new_limit * sizeof(*soa.index_buffer));
        if (NULL == new_ptr) {
                return AtsAllocFailed;
        }
        memset(new_ptr + soa.count, 0, soa.capacity * sizeof(*soa.index_buffer));
        soa.index_buffer = new_ptr;

        void **new_names = realloc(soa.name, new_limit * sizeof(*soa.name));
        if (NULL == new_names) {
                return AtsAllocFailed;
        }
        memset(new_names + soa.count, 0, soa.capacity * sizeof(*soa.name));
        soa.name = new_ptr;

        new_ptr = realloc(soa.index_count, new_limit * sizeof(*soa.index_count));
        if (NULL == new_ptr) {
                return AtsAllocFailed;
        }
        memset(new_ptr + soa.count, 0, soa.capacity * sizeof(*soa.index_count));
        soa.index_count = new_ptr;

        new_ptr = realloc(soa.vertex_count, new_limit * sizeof(*soa.vertex_count));
        if (NULL == new_ptr) {
                return AtsAllocFailed;
        }
        memset(new_ptr + soa.count, 0, soa.capacity * sizeof(*soa.vertex_count));
        soa.vertex_count = new_ptr;

        soa.capacity = new_limit;

        return AtsNoError;
}

/** Creates a GL_ARRAY_BUFFER containing position vectors. */
static void make_position_buffer(ogl_buf_id *id, const ats_float *position_data, ats_int count)
{
        glGenBuffers(1, id);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }

        glBindBuffer(GL_ARRAY_BUFFER, *id);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }

        ats_size buffer_length = count * sizeof(ats_float);
        glBufferData(GL_ARRAY_BUFFER, buffer_length, position_data, GL_STATIC_DRAW);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }
}

/** Creates a GL_ARRAY_BUFFER containing vertex normals. */
static void make_normal_buffer(ogl_buf_id *id, const ats_float *normal_data, ats_int count)
{
        glGenBuffers(1, id);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }

        glBindBuffer(GL_ARRAY_BUFFER, *id);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }

        ats_size buffer_length = count * sizeof(ats_float);
        glBufferData(GL_ARRAY_BUFFER, buffer_length, normal_data, GL_STATIC_DRAW);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }
}

/** Creates a GL_ARRAY_BUFFER containing texture co-ordinates. */
static void make_texture_buffer(ogl_buf_id *id, const ats_float *texture_data, ats_int count)
{
        glGenBuffers(1, id);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }

        glBindBuffer(GL_ARRAY_BUFFER, *id);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }

        ats_size buffer_length = count * sizeof(ats_float);
        glBufferData(GL_ARRAY_BUFFER, buffer_length, texture_data, GL_STATIC_DRAW);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }
}

/** Creates an GL_ELEMENT_ARRAY_BUFFER containing draw sequence indices. */
static void make_index_buffer(ogl_buf_id *id, const ats_ushort *index_data, ats_int count)
{
        glGenBuffers(1, id);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *id);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }

        ats_size buffer_length = count * sizeof(ats_ushort);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer_length, index_data, GL_STATIC_DRAW);
        if (GL_NO_ERROR != glGetError()) {
                longjmp(ogl_error, 1);
        }
}

/** Adds a mesh into the engine renderable soa. */
static ats_error put_mesh(ats_renderable_uid *id, const struct ats_mesh *mesh)
{
        /* If resize required. */
        if (soa.count == soa.capacity) {
                ats_error error = resize_soa();
                if (AtsNoError != error) {
                        return error;
                }
        }

        ats_char **name_ptr = soa.name + soa.count;
        ats_size mesh_name_length = strlen(mesh->name);
        *name_ptr = malloc((1 + mesh_name_length) * sizeof(ats_char));
        if (NULL == *name_ptr) {
                return AtsAllocFailed;
        }
        strncpy(*name_ptr, mesh->name, mesh_name_length);
        (*name_ptr)[mesh_name_length] = '\0';

        /* Retrieve pointers to the buffer ids for the new renderable. */
        ogl_buf_id *position_buf_id = soa.position_buffer + soa.count;
        ogl_buf_id *normal_buf_id = soa.normal_buffer + soa.count;
        ogl_buf_id *texture_buf_id = soa.texture_buffer + soa.count;
        ogl_buf_id *index_buf_id = soa.index_buffer + soa.count;

        if (!setjmp(ogl_error)) {

                if (NULL != mesh->positions) {
                        make_position_buffer(position_buf_id, mesh->positions, mesh->vertex_count);
                }

                if (NULL != mesh->normals) {
                        make_normal_buffer(normal_buf_id, mesh->normals, mesh->vertex_count);
                }

                if (NULL != mesh->textures) {
                        make_texture_buffer(texture_buf_id, mesh->textures, mesh->vertex_count);
                }

                if (NULL != mesh->indices) {
                        make_index_buffer(index_buf_id, mesh->indices, mesh->index_count);
                }

                *id = soa.count;
                soa.vertex_count[*id] = mesh->vertex_count;
                soa.index_count[*id] = mesh->index_count;
                soa.count++;

                return AtsNoError;
        } else {
                /* Delete any buffers we've created. N.B. values of 0 will be ignored. */
                glDeleteBuffers(1, position_buf_id);
                glDeleteBuffers(1, normal_buf_id);
                glDeleteBuffers(1, texture_buf_id);
                glDeleteBuffers(1, index_buf_id);

                return AtsOGLError;
        }
}

ATSAPI ats_error ATSCALL ats_renderable_init(ats_renderable_uid *id, const struct ats_mesh *mesh)
{
        ATS_DEBUG_ASSERT(NULL != id);

        if (ATS_FALSE == table_initialised) {
                ats_error error = init_soa();
                if (AtsNoError != error) {
                        *id = -1;
                        return error;
                }
                table_initialised = ATS_TRUE;
        }

        return put_mesh(id, mesh);
}

ATSAPI void ATSCALL ats_renderable_free(ats_renderable_uid id)
{
        /*
         * TODO: Remove id from soa.
         */

        /* Retrieve pointers to the buffer ids for this renderable. */
        ogl_buf_id *position_buffer_id = soa.position_buffer + id;
        ogl_buf_id *normal_buffer_id = soa.normal_buffer + id;
        ogl_buf_id *texture_buffer_id = soa.texture_buffer + id;
        ogl_buf_id *index_buffer_id = soa.index_buffer + id;

        /* Release the buffer data from OpenGL. */
        glDeleteBuffers(1, position_buffer_id);
        glDeleteBuffers(1, normal_buffer_id);
        glDeleteBuffers(1, texture_buffer_id);
        glDeleteBuffers(1, index_buffer_id);

        /* Reset the buffer ids back to the reserved "unassigned" state of 0. */
        *position_buffer_id = *normal_buffer_id = *texture_buffer_id = *index_buffer_id = 0;
}

ATSAPI void ATSCALL ats_renderable_render(ats_renderable_uid id)
{
        if (0 != soa.position_buffer[id]) {
                glEnableVertexAttribArray(AtsPositionAttrib);
                glBindBuffer(GL_ARRAY_BUFFER, soa.position_buffer[id]);
                glVertexAttribPointer(AtsPositionAttrib, comps_per_position, GL_FLOAT, GL_FALSE, 0,
                                      NULL);
        }

        if (0 != soa.normal_buffer[id]) {
                glEnableVertexAttribArray(AtsNormalAttrib);
                glBindBuffer(GL_ARRAY_BUFFER, soa.normal_buffer[id]);
                glVertexAttribPointer(AtsNormalAttrib, comps_per_normal, GL_FLOAT, GL_FALSE, 0,
                                      NULL);
        }

        if (0 != soa.texture_buffer[id]) {
                glEnableVertexAttribArray(AtsTextureAttrib);
                glBindBuffer(GL_ARRAY_BUFFER, soa.texture_buffer[id]);
                glVertexAttribPointer(AtsTextureAttrib, comps_per_texture, GL_FLOAT, GL_FALSE, 0,
                                      NULL);
        }

        if (0 == soa.index_buffer[id]) {
                /* Draw the vertices in the order they are provided. */
                glDrawArrays(GL_TRIANGLES, 0, soa.vertex_count[id]);
        } else {
                /* Draw with a pre-defined draw sequence. */
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, soa.index_buffer[id]);
                glDrawElements(GL_TRIANGLES, soa.index_count[id], GL_UNSIGNED_SHORT, NULL);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        }
}

ATSAPI ats_renderable_uid ATSCALL ats_renderable_get_id(const ats_char *name)
{
        for (ats_renderable_uid id = 0; id < soa.count; id++) {
                if (ATS_STR_EQ(soa.name[id], name)) {
                        return id;
                }
        }

        return -1;
}
