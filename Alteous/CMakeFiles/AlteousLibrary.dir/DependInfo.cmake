# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/alteous/AlteousEngine/Alteous/src/alteous/alteous.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/alteous.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/angle.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/angle.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/camera.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/camera.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/comparator.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/comparator.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/component.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/component.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/context.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/context.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/cursor.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/cursor.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/detail/context.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/detail/context.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/detail/cursor.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/detail/cursor.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/detail/fp_camera.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/detail/fp_camera.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/detail/keyboard.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/detail/keyboard.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/entity.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/entity.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/entity_table.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/entity_table.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/file.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/file.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/fp_camera.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/fp_camera.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/front_shader.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/front_shader.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/keyboard.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/keyboard.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/map.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/map.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/mat3.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/mat3.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/mat4.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/mat4.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/normals.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/normals.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/shader.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/shader.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/stack.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/stack.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/string.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/string.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/texture.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/texture.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/transform.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/transform.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/vec2.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/vec2.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/vec3.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/vec3.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/vector.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/vector.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/alteous/wavefront.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/alteous/wavefront.c.o"
  "/home/alteous/AlteousEngine/Alteous/src/third-party/stb/stb_image.c" "/home/alteous/AlteousEngine/Alteous/CMakeFiles/AlteousLibrary.dir/src/third-party/stb/stb_image.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "Alteous/include"
  "Alteous/include/third-party"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
