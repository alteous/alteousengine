/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file terrain.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 31/12/15
 * @brief Contains functions that render and manipulate engine height maps.
 */

#ifndef ALTEOUS_TERRAIN_H
#define ALTEOUS_TERRAIN_H

#include "alteous/header.h"
#include "alteous/types.h"

ATSBEGINDECL

ATSAPI void ATSCALL ats_terrain_render(struct ats_hgtmap *map);

ATSENDDECL

#endif /* ALTEOUS_TERRAIN_H */
