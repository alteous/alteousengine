/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file heightmap.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 31/12/15
 * @brief Contains an interface for loading and manipulating height map images.
 */

#ifndef ALTEOUS_HEIGHTMAP_H
#define ALTEOUS_HEIGHTMAP_H

#include "alteous/types.h"
#include "alteous/header.h"
#include "alteous/error.h"

ATSBEGINDECL

enum ats_img_fmt {
        AtsPng
};

struct ats_hgtmap;

/**
 * @brief Loads a height map from a .png file.
 * @param path The relative path to the height map file.
 * @param fmt The file format.
 * @return A pointer to the heightmap if successful, and an alternative error code otherwise.
 */
ATSAPI struct ats_hgtmap* ATSCALL ats_hgtmap_init(const ats_char *path, enum ats_img_fmt fmt);

/**
 * @brief Releases a height map from memory.
 * @param map The height map to be freed.
 */
ATSAPI void ATSCALL ats_hgtmap_free(struct ats_hgtmap *map);

ATSENDDECL

#endif /* ALTEOUS_HEIGHTMAP_H */
