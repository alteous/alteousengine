/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file camera.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 13/12/15
 * @brief Contains the definitions for an analogical camera that "views" scenes in the engine.
 */

#ifndef ALTEOUS_CAMERA_H
#define ALTEOUS_CAMERA_H

#include "alteous/error.h"
#include "alteous/mat4.h"
#include "alteous/vec3.h"
#include "alteous/header.h"

ATSBEGINDECL

struct ats_cam {
        struct ats_vec3f position;
        struct ats_angle pitch;
        struct ats_angle yaw;
        struct ats_angle roll;
};

/**
 * @brief Makes the default camera.
 *
 * The default camera is centred at the world origin, i.e. (0, 0, 0), and has 0 yaw, pitch, and roll
 * angle magnitudes. A default camera "views" the scene in the same way as OpenGL views them scene
 * by default.
 */
ATSAPI struct ats_cam ATSCALL ats_cam_make_default();

/**
 * @brief Creates a world-to-view transformation matrix from a camera.
 * @param output A 4x4 matrix to store the result.
 * @param camera The target camera.
 */
ATSAPI ats_error ATSCALL ats_cam_compute_view_matrix(struct ats_mat4 *output,
                                                     const struct ats_cam *camera);

ATSENDDECL

#endif /* ALTEOUS_CAMERA_H */
