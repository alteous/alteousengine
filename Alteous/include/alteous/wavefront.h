/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file wavefront.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 13/12/15
 * @brief Contains an interface for loading Wavefront .obj files into objects able to be rendered.
 */

#ifndef ALTEOUS_WAVEFRONT_OBJ_LOADER_H
#define ALTEOUS_WAVEFRONT_OBJ_LOADER_H

#include <stdlib.h>

#include "alteous/header.h"
#include "alteous/types.h"
#include "alteous/error.h"
#include "alteous/renderable.h"

ATSBEGINDECL

/**
 * @brief Defines a triangle in a mesh.
 *
 * Each element index, i, is represents v[i - 1] in the coordinate, normal, and texture co-ordinate
 * arrays respectively. i == 0 means no definition since .obj indices start at 1.
 */
struct ats_wft_face {
        ats_ushort position_index[3];
        ats_ushort texture_index[3];
        ats_ushort normal_index[3];
};

/**
 * @brief Defines a structure to hold raw data from an .obj file.
 *
 * "position_vectors" is the array of coordinate vectors.
 * "normal_vectors" is the array of normal vectors.
 * "texture_coordinates" is the array of texture co-ordinates.
 * "faces" are an array of index triads that represent a triangle.
 *
 * The maximum number of indices (and hence coordinate vectors, normal vectors, and texture
 * co-ordinates for a single mesh is defined to be the 65536, or 1 plus the maximum value of an
 * unsigned short. This is because GLES2 supports only unsigned byte and unsigned short type in a
 * call to glDrawElements().
 *
 * Note that although the number of indices has a fixed capacity, there is little restriction on the
 * number of faces that can be drawn using these indices.
 */
struct ats_wft {
        ats_float *position_vectors;     /* size == maximum_index_count */
        ats_size position_count;         /* do not resize */

        ats_float *normal_vectors;       /* size == maximum_index_count */
        ats_size normal_count;           /* do not resize */

        ats_float *texture_coordinates;  /* size == maximum_index_count */
        ats_size texture_count;          /* do not resize */

        struct ats_wft_face *faces;      /* struct members are zero-initialised. */
        ats_size face_count;             /* may be resized. */
        ats_size face_limit;             /* the number of faces currently in the array */
};

/** Forward declaration of the engine's mesh type. */
struct ats_mesh;

/**
 * @brief Loads a .obj file into a useful object.
 * @param file_path The relative path to the .obj file.
 * @param mesh A place to store the object data.
 * @return AtsNoError if the process succeeded and an alternative error code otherwise.
 */
ATSAPI struct ats_wft* ATSCALL ats_wft_init(const ats_char *file_path, ats_error *error);

/**
 * @brief Releases a set of parsed .obj data from memory.
 * @param wft The data to be freed.
 */
ATSAPI void ATSCALL ats_wft_free(struct ats_wft *wft);

/**
 * @brief Loads a .obj file into the engine rendering system.
 * @path The relative path to the .obj file.
 * @name The name of the model to be loaded.
 * @return A non-negative unique identifier to the loaded object is successful and -1 otherwise.
 */
ATSAPI ats_renderable_uid ATSCALL ats_wft_load(const ats_char *path, const ats_char *name);

/**
 * @brief Creates a renderable mesh object by copying the contents of a Wavefront object.
 * @param wft The wavefront object.
 * @return A new mesh object on the heap if successful and NULL otherwise.
 */
ATSAPI struct ats_mesh* ATSCALL ats_wft_to_mesh(const struct ats_wft *wft);

ATSENDDECL

#endif /* ALTEOUS_WAVEFRONT_OBJ_LOADER_H */
