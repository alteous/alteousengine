/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file map.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 15/12/15
 * @brief Contains an interface for a generic hash map.
 */

#ifndef ALTEOUS_MAP_H
#define ALTEOUS_MAP_H

#include "alteous/error.h"
#include "alteous/types.h"
#include "alteous/header.h"

ATSBEGINDECL

/**
 * @brief Defines a user-defined hashing function for a key value in a map.
 * @param key_ptr A pointer to the key value to be hashed.
 * @return The result of the hashing function.
 */
typedef ats_int (* ATSCALL ats_hash_fn)(void *key_ptr);

/**
 * @brief Defines a key-value pair.
 */
struct ats_map_pair {
        void *key_ptr;
        void *value_ptr;
};

/**
 * @brief Defines a linked entry in an ats_map bucket.
 */
struct ats_map_entry {
        struct ats_map_pair pair; /* The key-value pair contained in this entry. */
        struct ats_map_entry *next; /* The next entry in a bucket sub-list. */
};

/**
 * @brief Defines a map containing a set of generic key-value pairs.
 */
typedef ats_int (* ATSCALL ats_compare_fn)(void *left_ptr, void *right_ptr);
struct ats_map {
        struct ats_map_entry *buckets;
        ats_size key_size;
        ats_size value_size;
        ats_size capacity;
        ats_size size;
        ats_hash_fn hash_fn;
        ats_compare_fn key_compare_fn;
};

/**
 * @brief Convenience macro for creating an ats_map instance.
 * @code
 * struct ats_map *map = ats_map_make(char*, int);
 * @endcode
 */
#define ats_map_make(key_type, value_type, hash_fn, comparator_fn) ats_map_init((sizeof(key_type)), (sizeof(value_type)), (hash_fn), (comparator_fn))

/**
 * @brief Initialises a map.
 * @param key_size The size of each key in the map.
 * @param value_size The size of each value in the map.
 * @param hash_fn A function pointer to a key hash function.
 * @return A pointer to the map on the heap.
 */
ATSAPI struct ats_map* ATSCALL ats_map_init(ats_size key_size, ats_size value_size,
                                            ats_hash_fn hash_fn, ats_compare_fn comparator_fn);

/**
 * @brief Map de-initialiser.
 * @param map The map to be freed.
 */
ATSAPI void ATSCALL ats_map_free(struct ats_map *map);

/**
 * @brief Returns true if a map contains a given key value.
 * @param key_ptr A pointer to a key value to be located.
 * @return ATS_TRUE if the map contains the key value.
 */
ATSAPI ats_bool ATSCALL ats_map_contains_key(struct ats_map *map, void *key_ptr);

/**
 * @brief Retrieves, but does not remove, a pointer to a key-value pair from a map.
 * @param map The map to be inspected.
 * @param key_ptr A pointer to a key value to be located.
 * @return A mapped pair. If the mapping exists then key_ptr will be non-null.
 */
ATSAPI struct ats_map_pair ATSCALL ats_map_get(struct ats_map *map, void *key_ptr);

/**
 * @brief Inserts a new key-value pair into a map if and only if it does not already exist.
 * @param map The map to insert the pair into.
 * @param pair The key-value pair to be inserted into the map.
 * @return AtsKeyAlreadyExists, AtsAllocFailed, or AtsNoError.
 */
ATSAPI ats_error ATSCALL ats_map_put(struct ats_map *map, struct ats_map_pair pair);

/**
 * @brief Replaces the value in a key-value pair contained in a map.
 * @param map The map to be updated.
 * @param pair The key-value pair to replace the current pair with.
 * @return A pointer to the previously mapped key-value pair if it existed and NULL otherwise.
 */
ATSAPI struct ats_map_pair ATSCALL ats_map_replace(struct ats_map *map, struct ats_map_pair pair);

/**
 * @brief Removes a key-value pair from a map.
 * @param map The map to remove from.
 * @param key_ptr A pointer to a value representing the key.
 * @return A pointer to the removed key-value value if it existed and NULL otherwise.
 */
ATSAPI struct ats_map_pair ATSCALL ats_map_rm(struct ats_map *map, void *key_ptr);

ATSENDDECL

#endif /* ALTEOUS_MAP_H */
