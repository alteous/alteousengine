/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file context.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 21/12/15
 * @brief Contains a high-level interface for interacting with the engine OpenGL context.
 */

#ifndef ALTEOUS_CONTEXT_H
#define ALTEOUS_CONTEXT_H

#include "alteous/header.h"
#include "alteous/types.h"

ATSBEGINDECL

ATSAPI ats_float ATSCALL ats_context_compute_asp();

ATSAPI void ATSCALL ats_context_get_size(ats_int *width, ats_int *height);

ATSAPI ats_bool ATSCALL ats_context_is_active();

ATSAPI void ATSCALL ats_context_swap_buffers();

ATSENDDECL

#endif /* ALTEOUS_CONTEXT_H */
