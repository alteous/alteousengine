/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file vec3.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 06/12/15
 * @brief Contains definitions for three component mathematical vector types.
 */

#ifndef ALTEOUS_VEC3_H
#define ALTEOUS_VEC3_H

#include "alteous/types.h"

ATSBEGINDECL

struct ats_vec3f {
        ats_float x, y, z;
};

/**
 * @begin Adds the contents of one vector to another.
 * @param target The vector to add to.
 * @param arg The vector to be added.
 */
ATSAPI void ATSCALL ats_vec3f_add(struct ats_vec3f *target, const struct ats_vec3f *arg);

/**
 * @brief Copies the contents of one vector to another.
 * @param dest The vector to copy to.
 * @param src The vector to be copied.
 */
ATSAPI void ATSCALL ats_vec3f_copy(struct ats_vec3f *dest, const struct ats_vec3f *src);

/**
 * @brief Determines the direction vector between a start point and an end point.
 * If P is a vector representing a start point and Q is a vector representing an end point then the
 * direction of P and Q is given by PQ = Q - P.
 * @param A vector to store the direction.
 * @param start The start point.
 * @param end The end point.
 */
ATSAPI void ATSCALL ats_vec3f_direction(struct ats_vec3f *dir, const struct ats_vec3f *start,
                                        const struct ats_vec3f *end);

/**
 * @brief Determines whether two vectors are equal to each other.
 * Vector P is considered to be equal to vector Q where the x, y, and z components of P and Q are
 * within 1e-6 units of each other.
 * @param left The left hand argument.
 * @param right The right hand argument.
 * @return ATS_TRUE if the two vectors are equal.
 */
ATSAPI ats_bool ATSCALL ats_vec3f_equals(const struct ats_vec3f *left,
                                         const struct ats_vec3f *right);

/**
 * @brief Computes the vector cross product left x right.
 * @param product A vector to store the result.
 * @param left The left hand argument.
 * @param right The right hand argument.
 */
ATSAPI void ATSCALL ats_vec3f_cross(struct ats_vec3f *product, const struct ats_vec3f *left,
                                    const struct ats_vec3f *right);

/**
 * @brief Computes the length (magnitude) of a vector.
 * The length of a vector is defined the be the square root of the sum of the squares of all its
 * components. That is to say, length(v) == sqrt(v.x * v.x + v.y * v.y + v.z * v.z).
 */
ATSAPI ats_float ATSCALL ats_vec3f_length(const struct ats_vec3f *vector);

/**
 * @brief Reduces the length of a non-zero vector to 1 whilst keeping its direction unchanged.
 * @param vector The vector to be normalised.
 * @return ATS_TRUE if the vector was able to be normalised (i.e. it was non-zero).
 */
ATSAPI ats_bool ATSCALL ats_vec3f_normalise(struct ats_vec3f *vector);

/**
 * @brief Multiplies each component of a vector by a scalar.
 * @param vector The vector to be scaled.
 * @param scalar The scale factor.
 */
ATSAPI void ATSCALL ats_vec3f_scale(struct ats_vec3f *vector, ats_float scalar);

/**
 * @brief Computes the normal to the plane that passes through the vertices v0, v1, and v2.
 * The computation assumes a right-handed co-ordinate system.
 * @param normal A vector to store the surface normal.
 * @param v0 The first point on the plane.
 * @param v1 The second point on the plane.
 * @param v2 The third point on the plane.
 * @return ATS_TRUE if the surface normal was able to be normalised (i.e. it was non-zero).
 * @warn The normal argument will be modified regardless of the function return value.
 */
ATSAPI ats_bool ATSCALL ats_vec3f_surf_norm(struct ats_vec3f *normal, const struct ats_vec3f *v0,
                                            const struct ats_vec3f *v1, const struct ats_vec3f *v2);

ATSENDDECL

#endif /* ALTEOUS_VEC3_H */
