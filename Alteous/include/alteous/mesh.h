/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file mesh.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 31/12/15
 * @brief Defines an ordered set of vertex data that can be rendered by the engine.
 */

#ifndef ALTEOUS_MESH_H
#define ALTEOUS_MESH_H

#include "alteous/header.h"
#include "alteous/types.h"

ATSBEGINDECL

/**
 * @brief Contains raw mesh data.
 */
struct ats_mesh {
        const ats_char *name;  /* The name of the mesh. */
        ats_float *positions;  /* The vertex coordinate vectors (3 components per vertex). */
        ats_float *normals;    /* The vertex normal vectors (3 components per vertex). */
        ats_float *textures;   /* The vertex texture co-ordinates (2 components per vertex). */
        ats_int vertex_count;  /* The total number of vertices. */
        ats_ushort *indices;   /* The vertex draw sequence - may be NULL if unspecified. */
        ats_int index_count;   /* The total number of vertex draw sequence indices. */
};

/**
 * @brief Renders a mesh to the current OpenGL context.
 * @param mesh The mesh data to be rendered.
 */
ATSAPI void ATSCALL ats_mesh_render(const struct ats_mesh *mesh);

ATSENDDECL

#endif /* ALTEOUS_MESH_H */
