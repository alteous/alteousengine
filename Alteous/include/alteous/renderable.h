/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file renderable.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 03/01/16
 * @brief Contains definitions for data that is renderable by the engine.
 */

#ifndef ALTEOUS_RENDERABLE_H
#define ALTEOUS_RENDERABLE_H

#include "alteous/header.h"
#include "alteous/types.h"
#include "alteous/error.h"

ATSBEGINDECL

/** Defines an identifier for a renderable engine component. */
typedef ats_int ats_renderable_uid;

struct ats_mesh;

/**
 * @brief Loads a renderable into the engine from a set of mesh data.
 * @id A place to store the id.
 * @mesh The mesh data to be loaded.
 * @return AtsNoError if successful and an alternative error code otherwise.
 */
ATSAPI ats_error ATSCALL ats_renderable_init(ats_renderable_uid *id, const struct ats_mesh *mesh);

/**
 * @brief Releases a renderable from the engine and from memory.
 * @id The unique identifier of the renderable to be freed.
 */
ATSAPI void ATSCALL ats_renderable_free(ats_renderable_uid id);

/**
 * @brief Renders a renderable to the current OpenGL context.
 * @id The identifier of the renderable to be rendered.
 */
ATSAPI void ATSCALL ats_renderable_render(ats_renderable_uid id);

/**
 * @brief Returns the id of a renderable whose name matches the argument.
 * @name The name of the renderable to lookup.
 * @return The id of the renderable if found and -1 otherwise.
 */
ATSAPI ats_renderable_uid ATSCALL ats_renderable_get_id(const ats_char *name);

ATSENDDECL

#endif /* ALTEOUS_RENDERABLE_H */
