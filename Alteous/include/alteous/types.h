/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file types.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 02/12/15
 * @brief Defines the types used by the engine.
 */
#ifndef ALTEOUS_TYPES_H
#define ALTEOUS_TYPES_H

#include <SDL2/SDL_keycode.h>
#include <stdbool.h>
#include <stdint.h>

#include "alteous/gl.h"
#include "alteous/config.h"

ATSBEGINDECL

#define ATS_TRUE true
#define ATS_FALSE false

typedef _Bool       ats_bool;
typedef GLfloat     ats_float;
typedef double      ats_double;
typedef int8_t      ats_byte;
typedef GLchar      ats_char;
typedef int16_t     ats_short;
typedef int32_t     ats_int;

/* Do not use, if possible (see below). */
typedef int64_t     ats_long;
#ifdef EMSCRIPTEN
typedef uint32_t    ats_size;
#else
typedef size_t      ats_size;
#endif

typedef GLubyte     ats_ubyte;
#define             ATS_UBYTE_MAX (255)
typedef GLushort    ats_ushort;
#define             ATS_USHORT_MAX (65535)
typedef GLuint      ats_uint;
#define             ATS_UINT_MAX (4294967295)

typedef ats_int    *ats_int_ptr;
typedef ats_char   *ats_char_ptr;
typedef ats_float  *ats_float_ptr;
typedef ats_double *ats_double_ptr;
typedef void       *ats_void_ptr;

ATSENDDECL

#endif /* ALTEOUS_TYPES_H */
