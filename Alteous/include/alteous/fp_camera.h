/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file fp_camera.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 19/12/15
 * @brief Defines an interface for the engine's default first person camera.
 */

#ifndef ALTEOUS_FIRST_PERSON_CAMERA_H
#define ALTEOUS_FIRST_PERSON_CAMERA_H

#include "alteous/header.h"
#include "alteous/mat4.h"

ATSBEGINDECL

/**
 * @brief Copies the world coordinate of the camera.
 * @param coordinate A vector to store the coordinate.
 */
ATSAPI void ATSCALL ats_fp_cam_get_position(struct ats_vec3f *position);

/**
 * @brief Copies the world-to-view matrix representing the current transform of the camera.
 * @param matrix A matrix to store the matrix.
 */
ATSAPI void ATSCALL ats_fp_cam_get_view_matrix(struct ats_mat4 *matrix);

/**
 * @brief Updates the coordinate and look direction of the camera.
 */
ATSAPI void ATSCALL ats_fp_cam_update();

ATSENDDECL

#endif /* ALTEOUS_FIRST_PERSON_CAMERA_H */
