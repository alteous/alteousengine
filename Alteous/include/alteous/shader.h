/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file shader.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 29/11/15
 * @brief Defines an abstract GLSL program within OpenGL.
 */

#ifndef ALTEOUS_SHADER_H
#define ALTEOUS_SHADER_H

#include "alteous/gl.h"
#include "alteous/glext.h"
#include "alteous/types.h"
#include "alteous/header.h"

ATSBEGINDECL

/* The OpenGL unique identifier for a GLSL program stored in memory (valid when > 0). */
typedef GLuint ats_shader_uid;

/* The OpenGL unique identifier for a shader that may be attached to a program (valid when > 0). */
typedef GLuint ats_shader_comp_uid;

/** Data model for a GLSL program loaded into OpenGL. */
struct ats_shader {
        ats_shader_uid id;               /* the GLSL program id (valid when > 0) */
        ats_shader_comp_uid *components; /* attached shader components attached to this shader */
        ats_size component_count;        /* the number of shader components attached */
};

/* A location for an OpenGL shader attribute. */
enum ats_attrib_type {
        AtsPositionAttrib, AtsColourAttrib, AtsTextureAttrib, AtsNormalAttrib
};

/** Defines an attribute in a GLSL program. */
struct ats_attrib {
        const ats_char *name;
        const enum ats_attrib_type location;
};

/** Defines a function that handles any errors that arise when working with a ats_shader. */
typedef void (ATSCALL *ats_shader_err_fn)(const ats_char *error_message);

/**
 * @brief Creates a GLSL shader program from vertex shader and fragment shader source code.
 *
 * @param vertex_shader_source the source code of the vertex shader
 * @param fragment_shader_source the source code of the fragment shader
 * @param error_fn a function to handle any errors that may arise during the creation process
 *
 * @return a pointer to a valid GLSLShader if creation was successful, and 0 otherwise
 */
ATSAPI struct ats_shader* ATSCALL ats_shader_init(const ats_char *vertex_shader_source,
                                           const ats_char *fragment_shader_source,
                                           struct ats_attrib *attributes,
                                           ats_size attribute_count,
                                           ats_shader_err_fn error_fn);

/**
 * @brief Frees all memory associated with this shader program and releases all of its components
 * from OpenGL.
 *
 * @param program the program to be freed from memory
 */
ATSAPI void ATSCALL ats_shader_free(struct ats_shader *shader);

/**
 * @brief Binds a vertex attribute to a location index within a GLSL shader program.
 *
 * @param id the program where the attributes will be bound
 * @param attributes a pointer to an array of attributes
 * @param attribute_count the number of attributes in the array
 */
ATSAPI void ATSCALL ats_shader_bind_attribs(ats_shader_uid id, struct ats_attrib *attribs,
                                            ats_size attrib_count);

/**
 * @brief Compiles a shader program component in OpenGL from source.
 *
 * @param source the shader component source code to be compiled
 * @param type the shader type (must be either GL_VERTEX_SHADER or GL_FRAGMENT_SHADER)
 * @param error_fn a function to handle any errors that arise during the compilation process
 *
 * @return a valid shader component id (> 0) if the compilation was successful, 0 otherwise
 */
ATSAPI ats_shader_comp_uid ATSCALL ats_shader_compile_sources(const ats_char *source, GLenum type,
                                                              ats_shader_err_fn error_fn);
/**
 * @brief Clears all components in this program from OpenGL.
 *
 * @param program the program to be clears
 */
ATSAPI void ATSCALL ats_shader_clear(struct ats_shader *shader);

ATSENDDECL

#endif /* ALTEOUS_SHADER_H */
