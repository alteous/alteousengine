/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file keyboard.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 15/12/15
 * @brief Contains a set of functions to handle keyboard input.
 */

#ifndef ALTEOUS_KEYBOARD_H
#define ALTEOUS_KEYBOARD_H

#include "alteous/types.h"
#include "alteous/header.h"

ATSBEGINDECL

typedef SDL_Keycode ats_key_id;

/**
 * @brief Determines whether the if the specified key is currently pressed
 * @code
 * if (ats_keyboard_is_key_down('w')) {
 *      handle_key_input();
 * }
 * @endcode
 * @param key A character representing the key to be inspected.
 * @return ATS_TRUE, if the key is currently pressed
 */
ATSAPI ats_bool ATSCALL ats_keyboard_is_key_down(ats_key_id key);

ATSENDDECL

#endif /* ALTEOUS_KEYBOARD_H */
