/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file mat4.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 02/12/15
 * @brief Contains definitions for the OpenGL compatible 4x4 matrix type used by the API.
 */

#ifndef ALTEOUS_MAT4_H
#define ALTEOUS_MAT4_H

#include <stdlib.h>

#include "alteous/angle.h"
#include "alteous/types.h"
#include "alteous/vec3.h"

ATSBEGINDECL

/**
 * @brief A 4x4 single precision matrix type.
 *
 * The matrix is column-majored in order to be compatible with glUniformMatrix4fv. The matrix A :=
 * [ 1  2  3  4 ]
 * [ 4  5  6  7 ]
 * [ 7  8  9  0 ]
 * [ 1  2  3  4 ]
 * has the internal representation of
 * @code
 * struct ats_mat3 matrix = {{ 1, 4, 7, 1, 2, 5, 8, 3, 6, 9, 3, 4, 7, 0, 7 }};
 * @endcode
 */
struct ats_mat4 {
        ats_float m[16];
};

/**
 * @brief Copies the contents of one matrix to another.
 * @param target The matrix to store the copy.
 * @param arg The matrix to be copied.
 */
ATSAPI void ATSCALL ats_mat4_copy(struct ats_mat4 *target, const struct ats_mat4 *arg);

/**
 * @brief Sets a 4x4 to the multiplicative identity matrix.
 * The 4x4 multiplicative identity matrix is given by
 *       [ 1  0  0  0 ]
 * I_4 = [ 0  1  0  0 ]
 *       [ 0  0  1  0 ]
 *       [ 0  0  0  1 ].
 * @param target The target matrix.
 */
ATSAPI void ATSCALL ats_mat4_identity(struct ats_mat4 *target);

/**
 * @brief Determines whether two matrices are equal.
 * A == B implies each element [i, j] of A is within 1e-6 units of the corresponding element [i, j]
 * of B.
 * @param left The matrix A (see above).
 * @param right The matrix B (see above).
 * @return ATS_TRUE if A == B.
 */
ATSAPI ats_bool ATSCALL ats_mat4_eq(const struct ats_mat4 *left, const struct ats_mat4 *right);

/**
 * @brief Computes the product of two 4x4 matrices.
 * The result of the computation is a post multiplication onto [left] by [right], that is to say,
 * output = left * right.
 * @param output A matrix to store the result.
 * @param left The matrix argument on the left hand side.
 * @param right The matrix argument on the right hand side.
 */
ATSAPI void ATSCALL ats_mat4_mat_prod(struct ats_mat4 *output, const struct ats_mat4 *left,
                                      const struct ats_mat4 *right);

/**
 * @brief Computes the product of a 4x4 matrix with a scalar.
 * The computation multiplies every element in the matrix by the scalar argument.
 * @param target The matrix to be multiplied.
 * @param scalar The argument to multiply the matrix elements by.
 */
ATSAPI void ATSCALL ats_mat4_scl_prod(struct ats_mat4 *target, ats_float scalar);

/**
 * @brief Prints a 4x4 matrix to the standard output file.
 * The matrix is printed in row-major order as it would be in a standard mathematical texture_coordinates book -
 * this is precisely the transpose of the actual column-majored representation.
 */
ATSAPI void ATSCALL ats_mat4_print(const struct ats_mat4 *input);

/**
 * @brief Creates a perspective projection matrix.
 * @param output A matrix to store the result.
 * @param fov The field of view angle.
 * @param asp The pixel aspect ratio (width / height) of the context.
 * @param near_plane The z co-ordinate of the near_plane clipping plane.
 * @param far The z co-ordinate of the far clipping plane.
 */
ATSAPI void ATSCALL ats_mat4_make_projection(struct ats_mat4 *output, const struct ats_angle *fov,
                                             ats_float asp,
                                             ats_float near, ats_float far);

/**
 * @brief Creates a rotation transformation matrix about the x axis.
 * @param output A matrix to store the result.
 * @param angle The rotation angle about the x axis.
 */
ATSAPI void ATSCALL ats_mat4_make_rotX(struct ats_mat4 *output, const struct ats_angle *angle);

/**
 * @brief Creates a rotation transformation matrix about the y axis.
 * @param output A matrix to store the result.
 * @param angle The rotation angle about the y axis.
 */
ATSAPI void ATSCALL ats_mat4_make_rotY(struct ats_mat4 *output, const struct ats_angle *angle);

/**
 * @brief Creates a rotation transformation matrix about the z axis.
 * @param output A matrix to store the result.
 * @param angle The rotation angle about the z axis.
 */
ATSAPI void ATSCALL ats_mat4_make_rotZ(struct ats_mat4 *output, const struct ats_angle *angle);

/**
 * @brief Creates a scale transformation matrix in the x, y, and z axes.
 * @param output A matrix to store the result.
 * @param vector The number of units to scale in the x, y, and z axes.
 */
ATSAPI void ATSCALL ats_mat4_make_scale(struct ats_mat4 *output, const struct ats_vec3f *vector);

/**
 * @brief Creates a translation transformation matrix in the positive x, y, and z directions.
 * @param output A matrix to store the result.
 * @param vector The number of units to translate in the x, y, and z axes.
 */
ATSAPI void ATSCALL ats_mat4_make_translation(struct ats_mat4 *output,
                                              const struct ats_vec3f *vector);

ATSENDDECL

#endif /* ALTEOUS_MAT4_H */
