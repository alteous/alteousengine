/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file memory.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 21/12/15
 * @brief Contains convenience macros for allocating memory.
 */

#ifndef ALTEOUS_ALLOC_H
#define ALTEOUS_ALLOC_H

#include "alteous/header.h"

/* malloc() and realloc() */
#include <stdlib.h>

ATSBEGINDECL

/**
 * @brief Allocates memory for a single instance of a type.
 * @param type The type to allocate.
 * @return A pointer to the allocated memory if successful and NULL otherwise.
 */
#define ATS_ALLOC(type) malloc(sizeof(type))

/**
 * @brief Allocates memory for an array of instances of a type.
 * @param length The number of instances to allocate.
 * @param type The type to allocate.
 * @return A pointer to the allocated memory if successful and NULL otherwise.
 */
#define ATS_MALLOC(length, type) malloc((length) * sizeof(type))

/**
 * @brief Resizes an array, keeping the contents and their orders intact.
 * @param ptr A pointer to the beginning of the array.
 * @param length The desired length of the new array.
 * @param type The type to allocate.
 * @return A pointer to the reallocated memory if successful and NULL otherwise.
 */
#define ATS_REALLOC(ptr, length, type) realloc((ptr), (length) * sizeof(type))

ATSENDDECL

#endif /* ALTEOUS_ALLOC_H */
