/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file transform.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 06/12/15
 * @brief Contains an interface for a stack containing transformation instructions.
 */

#ifndef ALTEOUS_TRANSFORM_H
#define ALTEOUS_TRANSFORM_H

#include "alteous/angle.h"
#include "alteous/mat4.h"
#include "alteous/stack.h"
#include "alteous/header.h"

ATSBEGINDECL

/**
 * @brief A stack of transformations that can be composed into a transformation matrix.
 * @code
 * struct ats_transform *transform = ats_transform_init();
 * struct ats_mat4 transformation_matrix;
 * struct ats_angle angle = { 45.0f, AtsDegrees };
 * struct ats_vec3f vector = { 1.0f, -2.0f, 0.0f };
 * ats_transform_rotX(transform, &angle);
 * ats_transform_translate(transform, &vector);
 * ats_transform_compose(transform, &transformation_matrix);
 * ats_transform_free(transform);
 * @endcode
 */
struct ats_transform {
        struct ats_stk *stack;
};

/**
 * @brief Initialises a transformation.
 * @return A pointer to the transformation, if it was able to be allocated, and NULL otherwise.
 */
ATSAPI struct ats_transform* ATSCALL ats_transform_init();

/**
 * @brief Deinitialises a transformation.
 * @param transform The transformation to be freed.
 */
ATSAPI void ATSCALL ats_transform_free(struct ats_transform *transform);

/**
 * @brief Computes the transformation matrix that represents this transform.
 * @param transform The transform to be composed.
 * @param output A matrix to store the composition.
 */
ATSAPI void ATSCALL ats_transform_compose(struct ats_transform *transform, struct ats_mat4 *output);

/**
 * @brief Rotates the current transform anti-clockwise in the X axis.
 * @param transform The transform to be rotated.
 * @param angle The rotation angle.
 * @return AtsAllocFailed, if the transform could not be allocated, and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_transform_rotX(struct ats_transform *transform,
                                            const struct ats_angle *angle);

/**
 * @brief The reverse of AtsTransform_RotateX.
 * @see ats_transform_rotX
 */
ATSAPI ats_error ATSCALL ats_transform_unrotX(struct ats_transform *transform,
                                              const struct ats_angle *angle);

/**
 * @brief Rotates the current transform anti-clockwise in the Y axis.
 * @param transform The transform to be rotated.
 * @param angle The rotation angle.
 * @return AtsAllocFailed, if the transform could not be allocated, and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_transform_rotY(struct ats_transform *transform,
                                            const struct ats_angle *angle);

/**
 * @brief The reverse of AtsTransform_RotateY.
 * @see ats_transform_rotY
 */
ATSAPI ats_error ATSCALL ats_transform_unrotY(struct ats_transform *transform,
                                              const struct ats_angle *angle);

/**
 * @brief Rotates the current transform anti-clockwise in the Z axis.
 * @param transform The transform to be rotated.
 * @param angle The rotation angle.
 * @return AtsAllocFailed, if the transform could not be allocated, and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_transform_rotZ(struct ats_transform *transform,
                                            const struct ats_angle *angle);

/**
 * @brief The reverse of AtsTransform_RotateZ.
 * @see ats_transform_rotZ
 */
ATSAPI ats_error ATSCALL ats_transform_unrotZ(struct ats_transform *transform,
                                              const struct ats_angle *angle);

/**
 * @brief Scales the current transform in the x, y, and axes.
 * @param transform The transform to be scaled.
 * @param vector The number of units to scale in the x, y, and z axes respectively.
 * @return AtsAllocFailed, if the transform could not be allocated, and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_transform_scale(struct ats_transform *parent,
                                             const struct ats_vec3f *vector);
/**
 * @brief Translates the current transform in the x, y, and z axes.
 * @param transform The transform to be translated.
 * @param vector The number of units to translate in the x, y, and z axes respectively.
 * @return AtsAllocFailed, if the transform could not be allocated, and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_transform_translate(struct ats_transform *parent,
                                                 const struct ats_vec3f *vector);

/**
 * @brief The reverse of AtsTransform_Translate.
 * @see ats_transform_translate
 */
ATSAPI ats_error ATSCALL ats_transform_untranslate(struct ats_transform *parent,
                                                   const struct ats_vec3f *vector);

ATSENDDECL

#endif /* ALTEOUS_TRANSFORM_H */
