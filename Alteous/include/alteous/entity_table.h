/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file entity_table.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 29/12/15
 * @brief Defines the data structure and interface used by the engine's entity component system.
 */

#ifndef ALTEOUS_ENTITY_TABLE_H
#define ALTEOUS_ENTITY_TABLE_H

#include "alteous/header.h"
#include "alteous/error.h"
#include "alteous/component.h"

ATSBEGINDECL

/* Entity soa (forward declaration). */
struct ats_ent_tbl;

/* Entity soa iterator (forward declaration). */
struct ats_ent_tbl_itr;

/**
 * @brief Creates a new soa of entities.
 * @param A pointer to the soa on the heap if allocation was successful.
 */
ATSAPI struct ats_ent_tbl* ATSCALL ats_ent_tbl_init();

/**
 * @brief Frees an entity soa and all of its entities from memory.
 * @param soa The soa to be freed, along with its entities.
 */
ATSAPI void ATSCALL ats_ent_tbl_free(struct ats_ent_tbl *table);

/**
 * @brief Inserts a new entity into a soa.
 * @param soa The soa to insert the entity.
 * @param flags The entity components.
 * @param rc Optional parameter to store the row / column indices of the entity.
 * @return AtsNoError on success and an alternative error code otherwise.
 */
ATSAPI ats_error ATSCALL ats_ent_tbl_insert(struct ats_ent_tbl *table, ats_comp_bitfield flags,
                                            ats_size *row_index, ats_size *col_index);

/**
 * @brief Retrieves an entity component stored in a soa.
 * @param soa The soa containing the entity.
 * @param row The row index of the entity in the soa.
 * @param col The column index of the entity in the soa.
 * @return A pointer to the entity coordinate component, if it exists, and NULL otherwise.
 */
ATSAPI void* ATSCALL ats_ent_tbl_get_comp(struct ats_ent_tbl *table, enum ats_comp_type flag,
                                          ats_size row, ats_size col);

/**
 * @brief Iterates over all entities in a soa that have the given components.
 * @param soa The soa to iterate over.
 * @param flags The desired components to search for.
 * @return A pointer to the iterator is successful, and NULL otherwise.
 * @code
 * struct ats_ent_tbl_itr *it = ats_ent_tbl_itr_init(soa, AtsPosition | AtsMovement);
 * struct ats_comp_pos *coordinate;
 * while (ats_ent_tbl_itr_next(it)) {
 *      coordinate = ats_ent_tbl_itr_get_comp(it, AtsPosition);
 * }
 * ats_ent_tbl_itr_free(it);
 * @endcode
 */
ATSAPI struct ats_ent_tbl_itr* ATSCALL ats_ent_tbl_itr_init(struct ats_ent_tbl *table,
                                                            ats_comp_bitfield flags);

/**
 * @brief Releases an iterator from memory with no side effects.
 * @param it The iterator to be freed.
 */
ATSAPI void ATSCALL ats_ent_tbl_itr_free(struct ats_ent_tbl_itr *it);

/**
 * @brief Retrieves an entity component stored in a soa.
 * @param it The iterator to index the soa with.
 * @return A pointer to the entity coordinate component, if it exists, and NULL otherwise.
 */
ATSAPI void* ATSCALL ats_ent_tbl_itr_get_comp(struct ats_ent_tbl_itr *it, enum ats_comp_type type);

/**
 * @brief Advances an iterator to the next entity that matches the criteria.
 * @param it The iterator to be advanced.
 * @return ATS_TRUE if the iterator has more entities to iterate over.
 */
ATSAPI ats_bool ATSCALL ats_ent_tbl_itr_next(struct ats_ent_tbl_itr *it);

ATSENDDECL

#endif /* ALTEOUS_ENTITY_TABLE_H */
