/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file mat3.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 02/12/15
 * @brief Contains definitions for the OpenGL compatible 3x3 matrix type used by the API.
 */

#ifndef ALTEOUS_MAT3_H
#define ALTEOUS_MAT3_H

#include "alteous/mat4.h"
#include "alteous/header.h"

ATSBEGINDECL

/**
 * @brief A 3x3 single precision matrix type.
 *
 * The matrix is column-majored in order to be compatible with glUniformMatrix3fv. The matrix A :=
 * [ 1  2  3 ]
 * [ 4  5  6 ]
 * [ 7  8  9 ]
 * has the internal representation of
 * @code
 * struct ats_mat3 matrix = {{ 1, 4, 7, 2, 5, 8, 3, 6, 9 }};
 * @endcode
 */
struct ats_mat3 {
        ats_float m[9];
};

/**
 * @brief Computes the adjugate of a 3x3 matrix.
 * The adjugate of a matrix is the transposed matrix of its co-factors.
 * @param output A matrix to store the result.
 * @param input The target matrix.
 */
ATSAPI void ATSCALL ats_mat3_adj(struct ats_mat3 *output, const struct ats_mat3 *input);

/**
 * @brief Computes the determinant of a 3x3 matrix.
 * @param input The target matrix.
 * @return The determinant of the matrix.
 */
ATSAPI ats_float ATSCALL ats_mat3_det(const struct ats_mat3 *input);

/**
 * @brief Sets a 3x3 to the multiplicative identity matrix.
 * The 3x3 multiplicative identity matrix is given by
 *       [ 1  0  0 ]
 * I_3 = [ 0  1  0 ]
 *       [ 0  0  1 ].
 * @param target The target matrix.
 */
ATSAPI void ATSCALL ats_mat3_identity(struct ats_mat3 *target);

/**
 * @brief Computes the inverse of a 3x3 matrix, if it exists.
 * The inverse of a 3x3 matrix M is the 3x3 matrix M_inv such that M * M_inv = M_inv * M = I_3.
 * The inverse of any square matrix exists when its determinant is non-zero.
 * @param output A matrix to store the result.
 * @param input The matrix to be inverted.
 * @return ATS_TRUE, if the matrix was able to be inverted.
 */
ATSAPI ats_bool ATSCALL ats_mat3_inv(struct ats_mat3 *output, const struct ats_mat3 *input);

/**
 * @brief Computes the product of two 3x3 matrices.
 * The result of the computation is a post multiplication onto [left] by [right], that is to say,
 * output = left * right.
 * @param output A matrix to store the result.
 * @param left The matrix argument on the left hand side.
 * @param right The matrix argument on the right hand side.
 */
ATSAPI void ATSCALL ats_mat3_mat_prod(struct ats_mat3 *output, const struct ats_mat3 *left,
                                      const struct ats_mat3 *right);

/**
 * @brief Computes the 3x3 normal vector transformation matrix from a 4x4 modelview matrix.
 * The normal matrix, when multiplied with a normal vector, correctly aligns the vertex normals of
 * a 3D object such that they face the right way after the object is rotated. The normal matrix
 * exists if and only if the modelview matrix is invertible, since it is defined to be the transpose
 * of the inverse of the modelview matrix.
 * @param output A matrix to store the result.
 * @param modelview The 4x4 modelview matrix representing the transformation of the object.
 * @return ATS_TRUE, if the normal matrix could be determined.
 */
ATSAPI ats_bool ATSCALL ats_mat3_normal(struct ats_mat3 *output,
                                        const struct ats_mat4 *modelview);

/**
 * @brief Prints a 3x3 matrix to the standard output file.
 * The matrix is printed in row-major order as it would be in a standard mathematical texture_coordinates book -
 * this is precisely the transpose of the actual column-majored representation.
 */
ATSAPI void ATSCALL ats_mat3_print(const struct ats_mat3 *matrix);

/**
 * @brief Computes the product of a 3x3 matrix with a scalar.
 * The computation multiplies every element in the matrix by the scalar argument.
 * @param target The matrix to be multiplied.
 * @param scalar The argument to multiply the matrix elements by.
 */
ATSAPI void ATSCALL ats_mat3_scl_prod(struct ats_mat3 *matrix, ats_float scalar);

/**
 * @brief Transposes a 3x3 matrix by swapping its elements in place.
 * @param target The matrix to be transposed.
 */
ATSAPI void ATSCALL ats_mat3_transpose(struct ats_mat3 *matrix);

ATSENDDECL

#endif /* ALTEOUS_MAT3_H */
