/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file texture.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 29/12/15
 * @brief Contains an interface for loading textures into the engine.
 */

#ifndef ALTEOUS_TEXTURE_H
#define ALTEOUS_TEXTURE_H

#include "alteous/gl.h"
#include "alteous/header.h"
#include "alteous/types.h"

ATSBEGINDECL

typedef void (*ats_tex_err_fn)(const char *error_log);

typedef GLuint ats_tex_uid;

struct ats_tex {
        ats_tex_uid id;
};

ATSAPI struct ats_tex* ATSCALL ats_tex_init(const ats_char *file_name, ats_tex_err_fn error_fn);

ATSAPI void ATSCALL ats_tex_free(struct ats_tex *texture);

ATSENDDECL

#endif /* WEBGL_TEXTURE_H */
