/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file component.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 27/12/15
 * @brief Contains the component definitions used by the engine's entity component system.
 */

#ifndef ALTEOUS_COMPONENT_H
#define ALTEOUS_COMPONENT_H

#include "alteous/header.h"
#include "alteous/types.h"
#include "alteous/vec3.h"
#include "alteous/error.h"
#include "alteous/renderable.h"

ATSBEGINDECL

/**
 * @brief Defines the legal component types used by the engine.
 */
enum ats_comp_type {
         AtsCompPos = 0,                               /* Position. */
         AtsCompMvt,                                   /* Movement. */
         AtsCompRdr,                                   /* Render */
        _AtsCompCount                                  /* The number of valid component types. */
};

static const enum ats_comp_type ats_comp_types[] = {
        AtsCompPos,
        AtsCompMvt,
        AtsCompRdr
};

/**
 * @code
 * ats_comp_bitfield comps = AtsCompPosBit | AtsCompMvtBit;
 * @endcode
 */
typedef uint8_t ats_comp_bitfield;
enum ats_comp_bit {
        AtsCompPosBit = (1 << AtsCompPos),             /* Position bit. */
        AtsCompMvtBit = (1 << AtsCompMvt),             /* Movement bit. */
        AtsCompRdrBit = (1 << AtsCompRdr)              /* Render bit. */
};

/**
 * @define Defines the world coordinate of an object.
 */
struct ats_comp_pos {
        struct ats_vec3f coordinate;
};

/**
 * @brief Defines the movement characteristics of an object.
 */
struct ats_comp_mvt {
        struct ats_vec3f velocity;
        struct ats_vec3f acceleration;
};

/**
 * @brief Defines a renderable component of an object.
 */
struct ats_comp_rdr {
        ats_renderable_uid id;
};

/**
 * @brief Instantiates an array of components.
 * @param id The type of component to instantiate.
 * @param count The number of components in the array.
 * @return A pointer to the array, if allocated, and NULL otherwise.
 */
ATSAPI void* ATSCALL ats_comp_init(enum ats_comp_type type, ats_size count);

/**
 * @brief Releases an array of components from memory.
 * @param comp_ptr The array to be freed.
 */
ATSAPI void ATSCALL ats_comp_free(void *comp_ptr);

/**
 * @brief Returns the size in bytes of a named component type.
 * @param type The enum typename of the component.
 * @return The size of the named component in bytes, or 0 if unrecognised.
 */
ATSAPI ats_size ATSCALL ats_comp_sizeof(enum ats_comp_type type);

ATSENDDECL

#endif /* ALTEOUS_COMPONENT_H */
