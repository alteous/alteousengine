/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file vec2.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 16/12/15
 * @brief Contains definitions for two component mathematical vector types.
 */

#ifndef ALTEOUS_VEC2_H
#define ALTEOUS_VEC2_H

#include "alteous/types.h"
#include "alteous/header.h"

ATSBEGINDECL

/**
 * @brief A two dimensional vector of single precision floating point numbers.
 */
struct ats_vec2f {
        ats_float x;
        ats_float y;
};

/**
 * @brief A two dimensional vector of signed 32-bit integers.
 */
struct ats_vec2i {
        ats_int x;
        ats_int y;
};

/**
 * @brief Replaces the components of a vector with a copy of the componenets of another vector.
 * @param target A vector to store the copy.
 * @param arg The vector to be copied.
 */
ATSAPI void ATSCALL ats_vec2i_copy(struct ats_vec2i *target, const struct ats_vec2i *arg);

ATSENDDECL

#endif /* ALTEOUS_VEC2_H */
