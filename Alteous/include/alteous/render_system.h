/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file render_system.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 02/01/16
 * @brief Renders all entities in the ECS with a render component.
 */

#ifndef ALTEOUS_RENDER_SYSTEM_H
#define ALTEOUS_RENDER_SYSTEM_H

#include "alteous/header.h"

/**
 * TODO: Segregate entities by render flag.
 */

ATSBEGINDECL

/**
 * @brief Instructs the API to render all entities.
 * All entities in the engine's entity component system will be rendered to the current OpenGL
 * context.
 */
ATSAPI void ATSCALL ats_render_entities();

ATSENDDECL

#endif /* ALTEOUS_RENDER_SYSTEM_H */
