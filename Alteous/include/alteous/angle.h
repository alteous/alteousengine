/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file angle.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 06/12/15
 * @brief Contains a set of definitions for working with arbitrary angle types.
 */

#ifndef ALTEOUS_ANGLE_H
#define ALTEOUS_ANGLE_H

#include "alteous/types.h"
#include "alteous/header.h"

ATSBEGINDECL

/**
 * @brief A set of angle types.
 */
enum ats_angle_type {
        AtsDegrees, AtsRadians
};

/**
 * @brief Defines a generic angle type.
 *
 * @code
 * struct ats_angle angle = { 90.0f, AtsDegrees };
 * @endcode
 */
struct ats_angle {
        ats_float value;
        enum ats_angle_type type;
};

/**
 * @brief Copies the contents of one angle to another.
 * @param angle The angle to be copied.
 * @param destination An angle to store the copy.
 */
ATSAPI void ATSCALL ats_angle_copy(const struct ats_angle *angle, struct ats_angle *destination);

/**
 * @brief Retrieves the equivalent magnitude of an ats_angle in degrees.
 * @param angle The angle to query.
 * @return The magnitude of the angle in degrees.
 */
ATSAPI ats_float ATSCALL ats_angle_degrees(const struct ats_angle *angle);

/**
 * @brief Retrieves the equivalent magnitude of an ats_angle in radians.
 * @param angle The angle to query.
 * @return The magnitude of the angle in radians.
 */
ATSAPI ats_float ATSCALL ats_angle_radians(const struct ats_angle *angle);

/**
 * @brief Adds the magnitude of one angle to the magnitude of another.
 * @param angle The angle to be modified.
 * @param arg The angle to be added.
 * @code
 * struct ats_angle theta = { 90.0f, AtsDegrees };
 * struct ats_angle phi = { ATS_PI / 2.0f, AtsRadians };
 * ats_angle_add(theta, phi); // => theta == 180 degrees
 * @endcode
 */
ATSAPI void ATSCALL ats_angle_add(struct ats_angle *angle, const struct ats_angle *arg);

ATSENDDECL

#endif /* ALTEOUS_ANGLE_H */
