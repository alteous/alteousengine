/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file entity.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 27/12/15
 * @brief Contains an interface for interacting with the engine's entity component system.
 */

#ifndef ALTEOUS_ENTITY_H
#define ALTEOUS_ENTITY_H

#include "alteous/header.h"
#include "alteous/types.h"
#include "alteous/component.h"
#include "alteous/error.h"

ATSBEGINDECL

/* Forward declaration of iterator type. */
struct ats_ent_tbl_itr;

/** Defines the type for a unique entity identifier. */
typedef int64_t ats_ent_uid; /* N.B. We do not perform arithmetic on ids, so 64 bit is OK. */

/** Defines the engine entity iterator type. */
typedef struct ats_ent_tbl_itr ats_ent_itr;

/**
 * @brief Creates a new entity with the given component flags.
 * @code
 * ats_ent_uid id = ats_ent_init(AtsCompPosBit | AtsCompMvtBit);
 * @endcode
 * @return A unique identifier for the entity if successful, and -1 otherwise.
 */
ATSAPI ats_ent_uid ATSCALL ats_ent_init(ats_comp_bitfield component_flags);

/**
 * @brief Frees memory associated with an entity.
 * @param id The entity identifier to be freed.
 */
ATSAPI void ATSCALL ats_ent_free(ats_ent_uid id);

/**
 * @brief Returns a pointer to a component belonging to a specific entity.
 * @param id The unique identifier of the entity to lookup.
 * @param type The type of component to be returned.
 * @return A valid pointer to the component, if it exists, and NULL otherwise.
 */
ATSAPI void* ATSCALL ats_ent_get_comp(ats_ent_uid id, enum ats_comp_type type);

/**
 * @brief Creates an iterator that iterates over all entities with the given component flags.
 * N.B. A call to ats_entity_iterator_next() is required to begin iterating.
 * @param flags The desired component flags.
 * @return A pointer to the iterator if successful and NULL otherwise.
 * @code
 * ats_ent_itr *it = ats_ent_itr_init(AtsCompPosBit | AtsCompMvtBit);
 * while (ats_ent_itr_next(it)) {
 *      void *comp_ptr = ats_entity_iterator_get_comp(it, AtsCompPos);
 * }
 * ats_ent_itr_free(it);
 * @endcode
 */
ATSAPI ats_ent_itr * ATSCALL ats_ent_itr_init(ats_comp_bitfield flags);

/**
 * @brief Releases an iterator instance from memory.
 * @param it The iterator to be freed.
 */
ATSAPI void ATSCALL ats_ent_itr_free(ats_ent_itr *it);

/**
 * @brief Advances the iterator to the next element.
 * @param it The iterator to advance.
 * @return ATS_TRUE if the iterator is in a valid state.
 */
ATSAPI ats_bool ATSCALL ats_ent_itr_next(ats_ent_itr *it);

/**
 * @brief Returns a pointer to a component in the current iteration point.
 * @param it The iterator.
 * @param type The type of component to be returned.
 * @return A valid pointer to the component, if it exists, and NULL otherwise.
 */
ATSAPI void* ATSCALL ats_ent_itr_get_comp(ats_ent_itr *it, enum ats_comp_type type);

ATSENDDECL

#endif /* ALTEOUS_ENTITY_H */
