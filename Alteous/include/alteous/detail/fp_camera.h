/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file detail/fp_camera.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 19/12/15
 * @brief Contains an interface for interacting with the engine's first person camera.
 */

#ifndef ALTEOUS_FIRST_PERSON_CAMERA_DETAIL_H
#define ALTEOUS_FIRST_PERSON_CAMERA_DETAIL_H

#include "alteous/header.h"
#include "alteous/mat4.h"

ATSBEGINDECL

/**
 * @brief Initialises the first person camera.
 */
ATSAPI void ATSCALL _ats_fp_cam_init();

/**
 * @brief Deinitialises the first person camera.
 */
ATSAPI void ATSCALL _ats_fp_cam_free();

/**
 * @brief Updates the coordinate and look angle of the camera.
 */
ATSAPI void ATSCALL _ats_fp_cam_update();

/**
 * @brief Computes the world-to-view matrix for the camera.
 * @param matrix A matrix to store the result.
 */
ATSAPI void ATSCALL _ats_fp_cam_compute_view_matrix(struct ats_mat4 *matrix);

/**
 * @brief Retrieves the camera coordinate in the world co-ordinate system.
 * @param coordinate A vector to store the coordinate.
 */
ATSAPI void ATSCALL _ats_fp_cam_get_position(struct ats_vec3f *position);

ATSENDDECL

#endif /* ALTEOUS_FIRST_PERSON_CAMERA_DETAIL_H */
