/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file detail/keyboard.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 13/12/15
 * @brief Contains the functions that directly interface with the engine keyboard implementation.
 */

#ifndef ALTEOUS_KEYBOARD_DETAIL_H
#define ALTEOUS_KEYBOARD_DETAIL_H

#include "alteous/keyboard.h"
#include "alteous/types.h"
#include "alteous/header.h"

ATSBEGINDECL

/**
 * @brief Initialises the keyboard variables.
 */
ATSAPI void ATSCALL _ats_keyboard_init();

/**
 * @brief Sets a single key to its pressed state.
 * @param key The keycode of the key to be set.
 */
ATSAPI void ATSCALL _ats_keyboard_set_key_up(ats_key_id key);

/**
 * @brief Sets a single key to its unpressed state.
 * @param key The keycode of the key to be set.
 */
ATSAPI void ATSCALL _ats_keyboard_set_key_down(ats_key_id key);

/**
 * @brief Determines whether a key is in its pressed state.
 * @param key The keycode of the key to be inspected.
 * @return ATS_TRUE, if the key is currently in its pressed state.
 */
ATSAPI ats_bool ATSCALL _ats_keyboard_is_key_down(ats_key_id key);

ATSENDDECL

#endif /* ALTEOUS_KEYBOARD_DETAIL_H */
