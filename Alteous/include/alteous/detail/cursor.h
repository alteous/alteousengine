/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file detail/cursor.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 16/12/15
 * @brief Contains the functions that directly interface with the engine cursor implementation.
 */

#ifndef ALTEOUS_CURSOR_DETAIL_H
#define ALTEOUS_CURSOR_DETAIL_H

#include "alteous/types.h"
#include "alteous/header.h"
#include "alteous/vec2.h"

ATSBEGINDECL

/**
 * @brief Initialises the cursor variables.
 */
ATSAPI void ATSCALL _ats_cursor_init();

/**
 * @brief Copies the change in movement of the cursor.
 * @param delta A vector to store the cursor movement.
 */
ATSAPI void ATSCALL _ats_cursor_get_delta_pos(struct ats_vec2i *delta);

/**
 * @brief Copies the current coordinate of the cursor.
 * @param coordinate A vector to store the cursor coordinate.
 */
ATSAPI void ATSCALL _ats_cursor_get_pos(struct ats_vec2i *position);

/**
 * @brief Sets delta_position to the zero vector.
 * This is called at the end of every update function since SDL provides no "no movement"
 * cursor event.
 */
ATSAPI void ATSCALL _ats_cursor_reset_delta_pos();

/**
 * @brief Sets the current coordinate of the cursor.
 * @param new_position The new cursor coordinate.
 */
ATSAPI void ATSCALL _ats_cursor_set_pos(const struct ats_vec2i *new_position);

ATSENDDECL

#endif /* ALTEOUS_CURSOR_DETAIL_H */
