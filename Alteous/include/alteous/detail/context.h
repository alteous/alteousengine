/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file detail/context.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 21/12/15
 * @brief Contains an interface for interacting with the engine OpenGL context.
 */

#ifndef ALTEOUS_CONTEXT_DETAIL_H
#define ALTEOUS_CONTEXT_DETAIL_H

#include "alteous/types.h"
#include "alteous/header.h"

ATSBEGINDECL

/**
 * @brief Initialises and binds an OpenGL context.
 */
ATSAPI void ATSCALL _ats_context_init();

/**
 * @brief Terminates the currently bound context.
 */
ATSAPI void ATSCALL _ats_context_free();

/**
 * @brief Returns the size of the currently bound context.
 * @param width A place to store the context width (in pixels).
 * @param height A place to store the context height (in pixels).
 */
ATSAPI void ATSCALL _ats_context_get_size(ats_int *width, ats_int *height);

/**
 * @brief Sets the size of the currently bound context.
 * @param width The new width in pixels.
 * @param height The new height in pixels.
 */
ATSAPI void ATSCALL _ats_context_set_size(ats_int width, ats_int height);

/**
 * @brief Determines whether the current OpenGL process is terminating.
 * @return ATS_TRUE if the current OpenGL process is terminating.
 */
ATSAPI ats_bool ATSCALL _ats_context_is_active();

/**
 * @brief Instructs the engine to terminate the current OpenGL process.
 */
ATSAPI void ATSCALL _ats_context_close();

/**
 * @brief Swaps the frame buffers of the currently bound context.
 */
ATSAPI void ATSCALL _ats_context_swap_buffers();

ATSENDDECL

#endif /* ALTEOUS_CONTEXT_DETAIL_H */
