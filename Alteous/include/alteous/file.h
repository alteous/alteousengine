/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file file.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 15/12/15
 * @brief Contains a set of functions for working with native files.
 */

#ifndef ALTEOUS_FILE_H
#define ALTEOUS_FILE_H

#include <stdio.h>

#include "alteous/header.h"
#include "alteous/types.h"

ATSBEGINDECL

typedef FILE *ats_file;

typedef void (ATSCALL *file_err_fn)(const ats_char *error_log);

/**
 * @brief Copies the contents of a file into a UTF-8 string.
 * @param The file whose content is to be copied.
 * @return A pointer to the file contents on the heap.
 */
ATSAPI ats_char ATSCALL *ats_file_read_utf8(const ats_char *path, file_err_fn error_fn);

/**
 * @brief Determines the length of a file in bytes.
 * @param The file to be inspected.
 * @return The length of the file in bytes.
 */
ATSAPI ats_long ATSCALL ats_file_length(ats_file file);

ATSENDDECL

#endif /* FILE_H */
