/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file stack.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 06/12/15
 * @brief Contains an interface for a generic stack data structure.
 */

#ifndef ALTEOUS_STACK_H
#define ALTEOUS_STACK_H

#include <stddef.h>

#include "alteous/types.h"
#include "alteous/header.h"
#include "alteous/error.h"

ATSBEGINDECL

/**
 * @brief Defines a stack data structure.
 * The stack manages an array of generic pointers that are assumed to be on the heap. It is up to
 * the caller to dealloc the memory addressed by the stack elements.
 * @code
 * ats_int_ptr m0 = ATS_ALLOC(ats_int);
 * struct ats_stk *stack = ats_stk_init(10);
 * ats_stk_push(stack, m0);
 * ats_int_ptr ptr = ats_stk_pop(stack); // => m0
 * free(ptr); // equivalent to free(m0)
 * ats_stk_free(stack);
 * @endcode
 */
struct ats_stk {
        void **elements;
        void *top;
        ats_size height;
        ats_size capacity;
};

/**
 * @brief Initialises a stack container.
 * @param initial_size The initial stack capacity.
 */
ATSAPI struct ats_stk* ATSCALL ats_stk_init(ats_size initial_capacity);

/**
 * @brief De-initialises a stack container.
 * @param stack The stack to be freed.
 */
ATSAPI void ATSCALL ats_stk_free(struct ats_stk *stack);

/**
 * @brief Determines the current number of elements on the stack.
 * @param stack The stack to be inspected.
 * @return The number of elements currently on the stack.
 */
ATSAPI ats_size ATSCALL ats_stk_height(const struct ats_stk *stack);

/**
 * @brief Determines whether a stack contains any elements.
 * @param stack The stack to be inspected.
 * @return ATS_TRUE if the stack contains at least one element.
 */
ATSAPI ats_bool ATSCALL ats_stk_is_empty(const struct ats_stk *stack);

/**
 * @brief Retrieves the top element of the stack without removing it.
 * @param stack The stack to be inspected.
 * @return A constant pointer to the element at the top of the stack.
 */
ATSAPI const void* ATSCALL ats_stk_peek(const struct ats_stk *stack);

/**
 * @brief Removes and returns the top element of the stack.
 * @param stack The stack to remove from.
 * @return A pointer to the removed element.
 * @warn If the stack is empty, popping returns a NULL pointer.
 */
ATSAPI void* ATSCALL ats_stk_pop(struct ats_stk *stack);

/**
 * @brief Pushes an element onto the top of the stack.
 * @param stack The stack to add the element to.
 * @param element A pointer to the element to put on top of the stack.
 * @return AtsAllocFailed if the stack could not be resized and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_stk_push(struct ats_stk *stack, void *element);

ATSENDDECL

#endif /* ALTEOUS_STACK_H */
