/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file string.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 21/12/15
 * @brief A simple string API.
 */

#ifndef ALTEOUS_STRING_H
#define ALTEOUS_STRING_H

#include "alteous/error.h"
#include "alteous/types.h"
#include "alteous/header.h"

ATSBEGINDECL

struct ats_string {
        ats_char *chars;
        ats_size length;
};

/**
 * @brief Initialises a char* wrapper from a string literal.
 * @param string The pointer to be initialised.
 * @param format A format string.
 * @param va_list Arguments to match the format string.
 * @see printf
 * @return A pointer to the string if allocated and NULL otherwise.
 */
ATSAPI struct ats_string* ATSCALL ats_string_init(const ats_char *format, ...);

/**
 * @brief Initialises an char* wrapper containing the empty string.
 * @return A pointer to the string if allocated and NULL otherwise.
 */
ATSAPI struct ats_string* ATSCALL ats_string_init_empty();

/**
 * @brief Releases a char* wrapper from heap memory.
 * @param string the wrapper to be released from memory
 */
ATSAPI void ATSCALL ats_string_free(struct ats_string *string);

/**
 * @brief Appends a character array to an existing char* wrapper.
 * @param target The char* wrapper to append to
 * @param right The character array to be copied and appended
 * @return AtsAllocFailed if the string could not be allocated and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_string_append(struct ats_string *target, const ats_char *right);

/**
 * @brief Concatenates the char values of left and right and wraps the result in output.
 *
 * @param output The output string.
 * @param left The left-hand character array to be copied and concatenated.
 * @param right The right-hand character array to be copied and concatenated.
 * @return AtsAllocFailed if the string could not be allocated and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_string_concat(struct ats_string *output, const ats_char *left,
                                           const ats_char *right);

/**
 * @brief Copies the contents of one string wrapper to another (opposed to struct ats_string_init()).
 * @param src The string wrapper to be copied.
 * @param dest The destination string wrapper.
 * @return AtsAllocFailed if the string could not be allocated and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_string_copy(const struct ats_string *src, struct ats_string *dest);

/**
 * @brief Copies the contents of one string wrapper to another (opposed to struct ats_string_init()).
 * @param src The string wrapper to be copied.
 * @param dest The destination string wrapper.
 * @return AtsAllocFailed if the string could not be allocated and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_string_copy_lit(const ats_char *literal, struct ats_string *dest);

/**
 * @brief Returns the characters of a char* wrapper.
 * @param string The target char* wrapper.
 * @return A pointer to the beginning of the underlying character array.
 */
ATSAPI const ats_char* ATSCALL ats_string_data(const struct ats_string *string);

/**
 * @brief Returns the number of characters in a char* wrapper.
 * @param string The target char* wrapper.
 * @return The number of characters in the target char* wrapper.
 */
ATSAPI ats_size ATSCALL ats_string_length(const struct ats_string *string);

/**
 * @brief Prepend a character array to an existing char* wrapper.
 * @param target The char* wrapper to append to.
 * @param left The character array to be copied and prepended.
 * @return AtsAllocFailed if the string could not be allocated and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_string_prepend(struct ats_string *target, const char *left);

ATSENDDECL

#endif /* ALTEOUS_STRING_H */
