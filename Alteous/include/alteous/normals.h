/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file context.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 22/12/15
 * @brief Contains functions to compute the vertex normals of an object.
 */

#ifndef ALTEOUS_NORMALS_H
#define ALTEOUS_NORMALS_H

#include "alteous/header.h"
#include "alteous/vec3.h"

ATSBEGINDECL

/**
 * @brief Computes the vertex normals for a set of faces defined by a list of position_vectors and indices.
 * @param vertices An array containing the position_vectors of each vertex.
 * @param vertex_count The number of vertex coordinate in the array.
 * @param indices An array containing the triangulated vertex draw sequence.
 * @param index_count The number of indices in the array.
 * @param normals An array long enough to store the computed normals.
 */
ATSAPI void ATSCALL ats_normals_compute(const struct ats_vec3f *vertices, ats_size vertex_count,
                                        const ats_ushort *indices, ats_size index_count,
                                        struct ats_vec3f *normals);

ATSENDDECL

#endif /* ALTEOUS_NORMALS_H */
