/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file vector.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 15/12/15
 * @brief Contains an interface for a generic dynamically sized array.
 */

#ifndef ALTEOUS_VECTOR_H
#define ALTEOUS_VECTOR_H

#include "alteous/error.h"
#include "alteous/types.h"
#include "alteous/header.h"

ATSBEGINDECL

typedef void (*ats_vtr_itr_fn)(const void *value_ptr);

/**
 * @brief Defines the data structure for an enumerable dynamically sized block of memory.
 */
struct ats_vtr {
        void   **elements;
        ats_size capacity;
        ats_size length;
};

/**
 * @brief Initialises an ats_vtr with a given initial capacity.
 * @param initial_capacity The initial number of elements the vector can hold before resizing.
 * @return A pointer to the vector if allocated and NULL otherwise.
 */
ATSAPI struct ats_vtr* ATSCALL ats_vtr_init(ats_size initial_capacity);

/**
 * @brief Deinitialises an ats_vtr container.
 * @warn The pointers held in the container are not freed.
 * @param vector The container to be freed.
 */
ATSAPI void ATSCALL ats_vtr_free(struct ats_vtr *vector);

/**
 * @brief Retrieves a pointer to an indexed element in a vector.
 * @param vector The vector to retrieve from.
 * @param index The location of the item in the vector to be retrieved.
 * @return A pointer to the indexed element if in range and NULL otherwise.
 */
ATSAPI const void* ATSCALL ats_vtr_get(struct ats_vtr *vector, ats_size index);

/**
 * @brief Appends an element to the end of a vector.
 * @param vector The vector to append to.
 * @param value_ptr A non-null pointer to the value to be appended.
 * @return AtsAllocFailed if the container could not be resized and AtsNoError otherwise.
 */
ATSAPI ats_error ATSCALL ats_vtr_push(struct ats_vtr *vector, void *value_ptr);

/**
 * @brief Removes an indexed pointer from a vector.
 * @param vector The vector to remove from.
 * @param index The location to remove.
 * @return The indexed element.
 */
ATSAPI void* ATSCALL ats_vtr_rm(struct ats_vtr *vector, ats_size index);

/**
 * @brief Iterates over all elements of the array.
 * @param iterator_fn The function to be called once for each element in the array.
 */
ATSAPI void ATSCALL ats_vtr_for_each(struct ats_vtr *vector, ats_vtr_itr_fn iterator_fn);

ATSENDDECL

#endif /* ALTEOUS_VECTOR_H */
