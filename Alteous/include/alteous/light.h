/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file light.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 30/11/15
 * @brief Contains a set of definitions for light sources in a scene.
 */

#ifndef ALTEOUS_LIGHT_H
#define ALTEOUS_LIGHT_H

#include "alteous/vec3.h"
#include "colour.h"

ATSBEGINDECL

/**
 * @brief Defines a directional light source.
 *
 * A directional light source shines similarly to a bulb in a lamp shade.
 */
struct ats_dlight {
    ats_float intensity;  /* intensity constant */
    struct ats_vec3f position;  /* world coordinate */
    struct ats_vec3f direction; /* direction */
    struct ats_rgb colour;     /* colour */
};

/**
 * @brief Defines an omnidirectional light source.
 *
 * An omnidirectional light source shines similarly to an uncovered bulb.
 */
struct ats_olight {
    ats_float intensity; /* intensity constant */
    struct ats_vec3f position; /* world coordinate */
    struct ats_rgb colour;    /* colour */
};

ATSENDDECL

#endif /* ALTEOUS_LIGHT_H */
