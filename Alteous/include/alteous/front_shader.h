/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file front_shader.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 29/11/15
 * @brief Defines the shader for the default Alteous renderer.
 */

#ifndef ALTEOUS_FRONT_SHADER_H
#define ALTEOUS_FRONT_SHADER_H

#include "alteous/file.h"
#include "alteous/light.h"
#include "alteous/header.h"
#include "alteous/material.h"

ATSBEGINDECL

struct ats_shader;
typedef struct ats_shader ats_fshader;

/**
 * @brief Creates a front shader.
 */
ATSAPI ats_fshader* ATSCALL ats_fshader_init(file_err_fn file_error_fn);

/**
 * @brief Releases a front shader from OpenGL.
 */
ATSAPI void ATSCALL ats_fshader_free(ats_fshader *shader);

/**
 * @brief Sets the v_CameraPosition uniform variabel in a front shader.
 */
ATSAPI void ATSCALL ats_fshader_load_camera_position(ats_fshader *shader, ats_float x, ats_float y,
                                                     ats_float z);

/**
 * @brief Sets the v_ProjectionMatrix uniform variable in a front shader.
 */
ATSAPI void ATSCALL ats_fshader_load_projection_matrix(ats_fshader *shader, ats_float *matrix);

/**
 * @brief Sets the v_ModelViewMatrix uniform variable in a front shader.
 */
ATSAPI void ATSCALL ats_fshader_load_modelview_matrix(ats_fshader *shader, ats_float *matrix);

/**
 * @brief Sets the v_NormalMatrix uniform variable in a front shader.
 */
ATSAPI void ATSCALL ats_fshader_load_normal_matrix(ats_fshader *shader, ats_float *matrix);

/**
 * @brief Sets the v_GlobalLight uniform variable in a front shader.
 */
ATSAPI void ATSCALL ats_fshader_load_global_light(ats_fshader *shader, const struct ats_olight *light);

/**
 * @brief Sets the v_Material uniform variable in a front shader.
 */
ATSAPI void ATSCALL ats_fshader_load_mtl(ats_fshader *shader, const struct ats_mtl *material);

/**
 * @brief Sets the v_Gamma uniform variable in a front shader.
 */
ATSAPI void ATSCALL ats_fshader_load_gamma(ats_fshader *shader, ats_float gamma);

ATSENDDECL

#endif /* ALTEOUS_FRONT_SHADER_H */
