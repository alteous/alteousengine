/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

/**
 * @file comparator.h
 * @author David Harvey-Macaulay (alteous@outlook.com)
 * @date 19/12/15
 * @brief Contains definitions for comparison functions for common types.
*/

#ifndef ALTEOUS_COMPARATOR_H
#define ALTEOUS_COMPARATOR_H

#include "alteous/header.h"
#include "alteous/types.h"

ATSBEGINDECL

/**
 * @brief Defines a user-defined comparison function (>) for two values of the same type.
 * @param left A pointer to the key value on the left hand side of the comparison.
 * @param right A pointer to the key value on the right hand side of the comparison.
 * @return -1 if *left < *right, 0 if *left == *right, and 1 if *left > *right.
 */
typedef ats_int (ATSCALL *ats_compare_fn)(void *left_ptr, void *right_ptr);

/**
 * @brief Compares two values of type ats_int.
 * @see ats_compare_fn
 */
ATSAPI ats_int ATSCALL ats_compare_int(void *left_ptr, void *right_ptr);

/**
 * @brief Compares two values of type ats_float.
 * @see ats_compare_fn
 */
ATSAPI ats_int ATSCALL ats_compare_float(void *left_ptr, void *right_ptr);

/**
 * @brief Compares two values of type ats_char.
 * @see ats_compare_fn
 */
ATSAPI ats_int ATSCALL ats_compare_char(void *left_ptr, void *right_ptr);

/**
 * @brief Compares two values of type void*.
 * @see ats_compare_fn
 */
ATSAPI ats_int ATSCALL ats_compare_ptr(void *left_ptr, void *right_ptr);

/**
 * @brief Compares two values of type ats_size.
 * @see ats_compare_fn
 */
ATSAPI ats_int ATSCALL ats_compare_size(void *left_ptr, void *right_ptr);

/**
 * @brief Compares two values of type ats_bool.
 * @see ats_compare_fn
 */
ATSAPI ats_int ATSCALL ats_compare_bool(void *left_ptr, void *right_ptr);

ATSENDDECL

#endif /* ALTEOUS_COMPARATOR_H */
