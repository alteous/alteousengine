/**
 * Copyright (c) 2015 David Harvey-Macaulay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * The software is provided "as is", without warranty of any kind, express or implied, including
 * but not limited to the warranties of merchantability, fitness for a particular purpose and
 * non-infringement. In no event shall the authors or copyright holders be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or otherwise, arising from,
 * out of or in connection with the software or the use or other dealings in the software.
 */

#include <stdio.h>

#include <alteous/alteous.h>
#include <alteous/fp_camera.h>
#include <alteous/texture.h>
#include <alteous/camera.h>
#include <alteous/render_system.h>
#include <alteous/entity_loader.h>
#include <alteous/context.h>
#include <alteous/wavefront.h>
#include <alteous/cube.h>
#include <alteous/assertion.h>

struct ats_tex *texture = NULL;
struct ats_mesh *cube = NULL;

void gl_fatal_error_fn(const char *error_log)
{
        if (error_log) {
                fprintf(stderr, "Fatal OpenGL error: %s\n", error_log);
        }
        ats_exit();
}

void file_fatal_error_fn(const char *error_log)
{
        if (error_log) {
                fprintf(stderr, "Fatal file error: %s\n", error_log);
        }
        ats_exit();
}

void entity_load_error_fn(ats_error error_code, const ats_char *description)
{
        fprintf(stderr, "Error (%d): %s.\n", error_code, description);
        ats_exit();
}

void main_loop()
{
#ifndef EMSCRIPTEN
        while (ats_context_is_active()) {
#endif
                ats_poll_events();
                ats_render_entities();
#ifndef EMSCRIPTEN
        }
#endif
}

int main(int argc, char *argv[])
{
        ats_init(ATS_INIT_KEYBOARD | ATS_INIT_CURSOR | ATS_INIT_FP_CAMERA);

        cube = ats_cube_init();
        ATS_DEBUG_ASSERT(NULL != cube);
        ats_renderable_uid id;
        ats_renderable_init(&id, cube);
        ATS_DEBUG_ASSERT(id > -1);

        ats_ent_loader_load("res/scenes/crates.json", entity_load_error_fn);
        texture = ats_tex_init("res/textures/crate.png", gl_fatal_error_fn);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture->id);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glClearColor(0.78f, 0.94f, 1.0f, 1.0f);

#ifndef EMSCRIPTEN
        main_loop();
        ats_tex_free(texture);

        return ats_terminate();
#else
        emscripten_set_main_loop(main_loop, 0, 1);

        /* If we are porting to WebGL then let the garbage collector cleanup after us. */

        return EXIT_SUCCESS;
#endif
}
