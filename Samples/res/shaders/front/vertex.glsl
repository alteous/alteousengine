precision mediump float;

/* Defines the material properties for an arbitrary model. */
struct material_t {
        vec3 Ka;         /* ambient reflectivity */
        vec3 Kd;         /* diffuse reflectivity */
        vec3 Ks;         /* specular reflectivity */
        float shininess; /* specular shininess constant */
};

/* Defines a directional light source. */
struct dlight_t {
        float intensity;
        vec3 position;
        vec3 direction;
        vec3 colour;
};

/* Defines an omnidirectional light source. */
struct olight_t {
        float intensity;
        vec3 position;
        vec3 colour;
};

attribute vec3 v_Position;        /* the vertex local position */
attribute vec3 v_Colour;          /* the vertex color */
attribute vec2 v_TextureCoord;    /* the vertex texture co-ordinate */
attribute vec3 v_Normal;          /* the vertex normal vector */

uniform mat4 v_ModelViewMatrix;   /* local-to-world-to-view */
uniform mat4 v_ProjectionMatrix;  /* view-to-normalised */
uniform mat3 v_NormalMatrix;      /* transforms normal vectors */

uniform vec3 v_CameraPosition;    /* world co-ordinate for the camera position */

uniform olight_t v_GlobalLight;   /* the global light source */
uniform material_t v_Material;    /* material characteristics */
uniform float v_Gamma;            /* gamma correction constant */

/* Per-fragment attributes. */
varying vec2  f_TextureCoord;     /* fragment texture co-ordinate */
varying vec3  f_Position;         /* fragment interpolated position */
varying vec3  f_Normal;           /* fragment normal vector */

/* Lighting attributes. */
varying vec3  f_LightPosition;    /* fragment light position co-ordinate */
varying vec3  f_LightColour;      /* fragment light colour */

/* Material attributes. */
varying float f_Shininess;        /* fragment shininess */
varying vec3  f_Ka;               /* fragment ambient reflectivity */
varying vec3  f_Kd;               /* fragment diffuse reflectivity */
varying vec3  f_Ks;               /* fragment specular reflectivity */

/* Global attributes. */
varying float f_Gamma;            /* fragment gamma correction constant */

void main(void)
{
        vec4 WorldPosition = v_ModelViewMatrix * vec4(v_Position.xyz, 1.0);

        f_TextureCoord  = v_TextureCoord;
        f_Position      = WorldPosition.xyz;
        f_Normal        = normalize(v_NormalMatrix * v_Normal);

        f_LightPosition = v_GlobalLight.position;
        f_LightColour   = v_GlobalLight.colour;

        f_Shininess     = v_Material.shininess;
        f_Ka            = v_Material.Ka;
        f_Kd            = v_Material.Kd;
        f_Ks            = v_Material.Ks;

        f_Gamma         = v_Gamma;

        gl_Position = v_ProjectionMatrix * WorldPosition;
}
