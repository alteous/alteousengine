precision mediump float;

uniform sampler2D sampler;

/* Per-fragment attributes. */
varying vec2  f_TextureCoord;
varying vec3  f_Position;
varying vec3  f_Normal;

/* Lighting attributes. */
varying vec3  f_LightPosition;
varying vec3  f_LightColour;

/* Material attributes. */
varying float f_Shininess;
varying vec3  f_Ka;                  /* ambient reflectivity */
varying vec3  f_Kd;                  /* diffuse reflectivity */
varying vec3  f_Ks;                  /* specular reflectivity */

/* Global attributes. */
varying float f_Gamma;               /* gamma correction constant */

void main(void)
{
        /* Determine fragment texture pixel. */
        vec4 texel = texture2D(sampler, f_TextureCoord);

        /* Compute per-fragment attributes. */
        vec3 n = f_Normal;                                  /* adjusted normal */
        vec3 s = normalize(f_LightPosition - f_Position);   /* light direction */
        vec3 v = normalize(-1.0 * f_Position);              /* look direction  */
        vec3 r = -s + 2.0 * dot(s, n) * n;                  /* reflection      */

        /* Compute Phong lighting model. */
        vec3 ambient  = f_Ka;
        vec3 diffuse  = f_Kd * max( dot( s, n ), 0.0 );
        vec3 specular = f_Ks * pow( max( dot( r, v ), 0.0 ), f_Shininess );
        vec3 phong    = f_LightColour * (ambient + diffuse + specular);

        /* Gamma-correct the Phong light colour. */
        vec4 colour  = pow( vec4(phong, 1.0), vec4(1.0 / f_Gamma) );

        /* Apply and output the Phong lighting to the texture. */
        gl_FragColor = texel * colour;
}
