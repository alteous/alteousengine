precision mediump float;

/* Defines the material properties for an arbitrary model. */

struct material_t
{
  vec3 Ka;         /* ambient reflectivity */
  vec3 Kd;         /* diffuse reflectivity */
  vec3 Ks;         /* specular reflectivity */
  float shininess; /* specular shininess constant */
};

/* Defines a directional light source. */

struct dlight_t
{
  float intensity; /* intensity constant */
  vec3 position;   /* world position */
  vec3 direction;  /* direction */
  vec3 color;      /* color */
};

/* Defines an omnidirectional light source. */

struct olight_t
{
  float intensity; /* intensity constant */
  vec3 position;   /* world position */
  vec3 color;      /* color */
};

attribute vec3 v_Position;        /* the vertex local position      (layout = 0) */
attribute vec2 v_TextureCoord;    /* the vertex texture co-ordinate (layout = 1) */
attribute vec3 v_Normal;          /* the vertex normal vector       (layout = 2) */

uniform mat4 v_ModelViewMatrix;   /* local-to-world-to-view */
uniform mat4 v_ProjectionMatrix;  /* view-to-normalised */
uniform mat3 v_NormalMatrix;      /* transforms normal vectors */

uniform olight_t v_GlobalLight;   /* the global light source */
uniform material_t v_Material;    /* material characteristics */

varying vec2 f_TextureCoord;      /* fragment texture co-ordinate */
varying vec3 f_Light;             /* fragment diffuse light color */

void main(void)
{
  vec3 Normal = normalize(v_NormalMatrix * v_Normal);
  vec4 GlobalLightPosition = v_ModelViewMatrix * vec4(v_GlobalLight.position, 1.0);
  vec3 GlobalLightDirection = normalize(GlobalLightPosition.xyz - v_Position);

  float DiffuseLightIntensity = max( dot(GlobalLightDirection, Normal), 0.0 );
  vec3 Light = v_Material.Ka + DiffuseLightIntensity * v_Material.Kd * v_GlobalLight.color;

  f_TextureCoord = v_TextureCoord;
  f_Light = Light;

  gl_Position = v_ProjectionMatrix * v_ModelViewMatrix * vec4(v_Position.xyz, 1.0);
}
