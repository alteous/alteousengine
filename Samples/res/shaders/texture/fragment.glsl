precision mediump float;

uniform sampler2D sampler;

varying vec2 f_TextureCoord;
varying vec3 f_Light;

void main(void)
{
  vec4 texel = texture2D(sampler, f_TextureCoord);

  gl_FragColor = texel * vec4(f_Light.xyz, 1.0);
}
