ROOT_DIR            = .

ALTEOUS_LIBRARY_DIR = $(ROOT_DIR)/Alteous
SAMPLES_DIR         = $(ROOT_DIR)/Samples

build:
	$(MAKE) -C $(ALTEOUS_LIBRARY_DIR)
	$(MAKE) -C $(SAMPLES_DIR)

